<?php


// TIRAR A BARRA CASO TENHA
$APP_PATH_ROOT = (substr($_SERVER["DOCUMENT_ROOT"], -1) === "/" ? substr($_SERVER["DOCUMENT_ROOT"], 0, strlen($_SERVER["DOCUMENT_ROOT"]) - 1) : $_SERVER["DOCUMENT_ROOT"]);

require_once $APP_PATH_ROOT."/api/jogo/v000001/apimodels/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// LoginJogoAPIModel
// Classe para realização do login.
//
// Gerado em: 2018-03-09 07:03:24
// --------------------------------------------------------------------------------
class LoginJogoAPIModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe e criada
    function __construct() {
        parent::__construct();
    }

    private $CodigoAcesso;  // Código de acesso
    private $Senha;         // Senha
    private $IdJogo;        // Identificador do jogo
    private $Perfil;        // Perfil da pessoa que fez o login
    private $IdPessoa;      // Identificadof da pessoa que fez o login
    private $IdInstituicao; // Identificador da instituição que a pessoa está vincualda, se for usuário GOEDUCA será NULL
    private $Nome;          // Nome da pessoa
    private $Apelido;       // Apelido da Pessoa, se for NULL retorna o nome da pessoa
    private $Instituicao;   // Nome da instituição

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "CodigoAcesso") { return $this->CodigoAcesso; }
        if ($name === "Senha") { return $this->Senha; }
        if ($name === "IdJogo") { return $this->IdJogo; }
        if ($name === "Perfil") { return $this->Perfil; }
        if ($name === "IdPessoa") { return $this->IdPessoa; }
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "Apelido") { return $this->Apelido; }
        if ($name === "Instituicao") { return $this->Instituicao; }
        throw new Exception( $name . ' => Propriedade inválida.');
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "CodigoAcesso") { $this->CodigoAcesso = $value; return $value; }
        if ($name === "Senha") { $this->Senha = $value; return $value; }
        if ($name === "IdJogo") { $this->IdJogo = $value; return $value; }
        if ($name === "Perfil") { $this->Perfil = $value; return $value; }
        if ($name === "IdPessoa") { $this->IdPessoa = $value; return $value; }
        if ($name === "IdInstituicao") { $this->IdInstituicao = $value; return $value; }
        if ($name === "Nome") { $this->Nome = $value; return $value; }
        if ($name === "Apelido") { $this->Apelido = $value; return $value; }
        if ($name === "Instituicao") { $this->Instituicao = $value; return $value; }
        throw new Exception( $name . ' => Propriedade inválida.');
    }

    // --------------------------------------------------------------------------------
    // login
    //
    // Retorno
    //     IdPessoa         Identificador da pessoa
    //     IdInstituicao    Identificador da insituição
    //     Perfil           Tipo de perfil da pessoa juntoa a instituição
    //
    // Atenção:
    //     Usuário administrativo GOEDUCA apresenta o IdInstituicao com valor NULL.
    // --------------------------------------------------------------------------------
    public function login()
    {
        if (!isset($this->CodigoAcesso) || ($this->CodigoAcesso == '')) {
            $this->Perfil = null;
            $this->Id = null;
            return false;
        }
        // login para Pessoa
        $sql = "
                select  codigoacesso.idpessoa as IdPessoa,
                        codigoacesso.idinstituicao as IdInstituicao,
                        codigoacesso.perfil as Perfil,
                        pessoa.nome as Nome,
                        coalesce(pessoa.apelido, pessoa.nome) as Apelido,
                        instituicao.nome as Instituicao                        
                from    codigoacesso
                        join pessoa
                            on pessoa.idpessoa = codigoacesso.idpessoa
                            and pessoa.status = 'AT'
                        join instituicao
                            on instituicao.idinstituicao = codigoacesso.idinstituicao
                            and instituicao.status = 'AT'
                        join contapessoa
                            on contapessoa.idinstituicao = instituicao.idinstituicao
                            and contapessoa.idpessoa = codigoacesso.idpessoa
                            and contapessoa.status = 'AT'
                where   codigoacesso.codigoacesso = " . $this->o_db->quote($this->CodigoAcesso) . "
                        and
                        codigoacesso.senha = " . $this->o_db->quote($this->Senha) . "
                union all
                select  codigoacesso.idpessoa as IdPessoa,
                        codigoacesso.idinstituicao as IdInstituicao,
                        codigoacesso.perfil as Perfil,
                        pessoa.nome as Nome,
                        coalesce(pessoa.apelido, pessoa.nome) as Apelido,
                        instituicao.nome as Instituicao
                from    codigoacesso
                        join pessoa
                            on pessoa.idpessoa = codigoacesso.idpessoa
                            and pessoa.status = 'AT'
                        join instituicao
                            on instituicao.idinstituicao = codigoacesso.idinstituicao
                            and instituicao.status = 'AT'
                        join professor
                            on professor.idinstituicao = instituicao.idinstituicao
                            and professor.idprofessor = codigoacesso.idpessoa
                            and professor.status = 'AT'
                where  codigoacesso.codigoacesso = " . $this->o_db->quote($this->CodigoAcesso) . "
                        and
                        codigoacesso.senha = " . $this->o_db->quote($this->Senha) . "
                union all
                select  codigoacesso.idpessoa as IdPessoa,
                        codigoacesso.idinstituicao as IdInstituicao,
                        codigoacesso.perfil as Perfil,
                        pessoa.nome as Nome,
                        coalesce(pessoa.apelido, pessoa.nome) as Apelido,
                        instituicao.nome as Instituicao
                from    codigoacesso
                        join pessoa
                            on pessoa.idpessoa = codigoacesso.idpessoa
                            and pessoa.status = 'AT'
                        join instituicao
                            on instituicao.idinstituicao = codigoacesso.idinstituicao
                            and instituicao.status = 'AT'
                        join gestor
                            on gestor.idinstituicao = instituicao.idinstituicao
                            and gestor.idgestor = codigoacesso.idpessoa
                            and gestor.status = 'AT'
                where   codigoacesso.codigoacesso = " . $this->o_db->quote($this->CodigoAcesso) . "
                        and
                        codigoacesso.senha = " . $this->o_db->quote($this->Senha) . "
                union all
                select  codigoacesso.idpessoa as IdPessoa,
                        codigoacesso.idinstituicao as IdInstituicao,
                        codigoacesso.perfil as Perfil,
                        pessoa.nome as Nome,
                        coalesce(pessoa.apelido, pessoa.nome) as Apelido,
                        'GOEDUCA' as Instituicao
                from    codigoacesso
                        join pessoa
                            on pessoa.idpessoa = codigoacesso.idpessoa
                            and pessoa.status = 'AT'
                where   codigoacesso.codigoacesso = " . $this->o_db->quote($this->CodigoAcesso) . "
                        and
                        codigoacesso.senha = " . $this->o_db->quote($this->Senha) . "
                        and
                        codigoacesso.idinstituicao is null
                 ";

        // le o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj = $resultset->fetchObject()) {
                $this->Perfil = $obj->Perfil;
                $this->IdPessoa = $obj->IdPessoa;
                $this->IdInstituicao = $obj->IdInstituicao;
                $this->Nome = $obj->Nome;
                $this->Apelido = $obj->Apelido;
                $this->Instituicao = $obj->Instituicao;

                return true;
            }
        }

        $this->Perfil = null;
        $this->IdPessoa = null;
        $this->IdInstituicao = null;
        $this->Nome = null;
        $this->Apelido = null;
        $this->Instituicao = null;

        return false;
    }
}

?>
