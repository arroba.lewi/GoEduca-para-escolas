<?php


	// TIRAR A BARRA CASO TENHA
	$APP_PATH_ROOT = (substr($_SERVER["DOCUMENT_ROOT"], -1) === "/" ? substr($_SERVER["DOCUMENT_ROOT"], 0, strlen($_SERVER["DOCUMENT_ROOT"]) - 1) : $_SERVER["DOCUMENT_ROOT"]);

	require_once $APP_PATH_ROOT."/api/jogo/v000001/apimodels/RankingJogoAPI.php";

	header('Access-Control-Allow-Origin: *');

	function listarRankingJogo(){
		if(isset($_POST['idjogo']) && isset($_POST['idinstituicao']) && isset($_POST['iddisciplina'])){

			$o_ranking = new RankingJogoAPIModel;
			
			$o_ranking->IdJogo = $_POST['idjogo'];
			$o_ranking->IdInstituicao = $_POST['idinstituicao'];
			$o_ranking->IdDisciplina = $_POST['iddisciplina'];
			$o_ranking->TamanhoRanking = $_POST['tamanhoranking'];

			$result = $o_ranking->RankingInstituicaoDisciplina();

			if (is_array($result)) {
				$ranking = array();
				$pessoa = array();

				for($i = 0; $i < count($result); $i++) {
					array_push($pessoa, array("Nome" => $result[$i]->Nome, "DataPartida" => $result[$i]->DataPartida,"Pontos" => $result[$i]->Pontos));
				}	
	
				$ranking["status"] = "ok";
				$ranking["listapessoas"] = $pessoa;
	
				return $ranking;	
			}
			else {
				return array("status" => "Não foi possível recuperar os dados.");
			}
		}
		else {
			return array("status" => "Parâmetros inválidos.");
		}
	}

	$retornoRanking = listarRankingJogo();

	$retornoJson = json_encode($retornoRanking);

	echo $retornoJson;