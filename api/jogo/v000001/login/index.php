<?php

	$APP_PATH_ROOT = (substr($_SERVER["DOCUMENT_ROOT"], -1) === "/" ? substr($_SERVER["DOCUMENT_ROOT"], 0, strlen($_SERVER["DOCUMENT_ROOT"]) - 1) : $_SERVER["DOCUMENT_ROOT"]);

	require_once $APP_PATH_ROOT."/api/jogo/v000001/apimodels/LoginJogoAPI.php";

	header('Access-Control-Allow-Origin: *');

	function fazerLoginJogo(){
		if(	isset($_POST['codigoacesso'])
			&& isset($_POST['senha'])
			&& isset($_POST['idjogo']))
		{
			$o_loginjogoapi = new LoginJogoAPIModel;

			$o_loginjogoapi->CodigoAcesso = $_POST['codigoacesso'];
			$o_loginjogoapi->Senha = $_POST['senha'];
			$o_loginjogoapi->IdJogo = $_POST['idjogo'];

			if($o_loginjogoapi->login()) {
				$retorno = array();

				$retorno = array(
					"Nome" => $o_loginjogoapi->Nome,
					"Apelido" => $o_loginjogoapi->Apelido,
					"idpessoa" => $o_loginjogoapi->IdPessoa,
					"idinstituicao" => $o_loginjogoapi->IdInstituicao,
					"Instituicao" => $o_loginjogoapi->Instituicao,
					"status" => "OK"
				);
				return $retorno;
			}
			else {
				return array("status" => "Usuário ou senha inválidos.");
			}
		}
		else {
			return array("status" => "Parâmetros inválidos.");
		}
	}

	$retornoLogin = fazerLoginJogo();

	$retornoJson = json_encode($retornoLogin);

	echo $retornoJson;