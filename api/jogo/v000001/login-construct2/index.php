<?php

	// TIRAR A BARRA CASO TENHA
	$APP_PATH_ROOT = (substr($_SERVER["DOCUMENT_ROOT"], -1) === "/" ? substr($_SERVER["DOCUMENT_ROOT"], 0, strlen($_SERVER["DOCUMENT_ROOT"]) - 1) : $_SERVER["DOCUMENT_ROOT"]);

	require_once $APP_PATH_ROOT."/api/jogo/v000001/apimodels/LoginJogoAPI.php";

	header('Access-Control-Allow-Origin: *');

	function fazerLoginJogo(){
		if(	isset($_POST['codigoacesso'])
			&& isset($_POST['senha'])
			&& isset($_POST['idjogo']))
		{
			$o_loginjogoapi = new LoginJogoAPIModel;

			$o_loginjogoapi->CodigoAcesso = $_POST['codigoacesso'];
			$o_loginjogoapi->Senha = $_POST['senha'];
			$o_loginjogoapi->IdJogo = $_POST['idjogo'];

			if($o_loginjogoapi->login()) {
				$retorno = "{
					\"c2array\":true,
					\"size\":[1,6,1],
					\"data\":
					[
						[
							[\"" . $o_loginjogoapi->Nome . "\"],
							[\"" . $o_loginjogoapi->Apelido . "\"],
							[\"" . $o_loginjogoapi->IdPessoa . "\"],
							[\"" . $o_loginjogoapi->IdInstituicao . "\"],
							[\"" . $o_loginjogoapi->Instituicao . "\"],
							[\"" . "OK" . "\"]
						]
					]
				}";
				return $retorno;
			}
			else {
				$retorno = "{
					\"c2array\":true,
					\"size\":[1,1,1],
					\"data\":
					[
						[
							[\"" . "Usuário ou senha inválidos." . "\"]
						]
					]
				}";				
				return $retorno;
			}
		}
		else {
			$retorno = "{
				\"c2array\":true,
				\"size\":[1,1,1],
				\"data\":
				[
					[
						[\"" . "Parâmetros inválidos." . "\"]
					]
				]
			}";
			return $retorno;
		}
	}	

	$retornoLogin = fazerLoginJogo();

	echo $retornoLogin;