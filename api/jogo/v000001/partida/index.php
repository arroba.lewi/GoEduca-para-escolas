<?php

	// TIRAR A BARRA CASO TENHA
	$APP_PATH_ROOT = (substr($_SERVER["DOCUMENT_ROOT"], -1) === "/" ? substr($_SERVER["DOCUMENT_ROOT"], 0, strlen($_SERVER["DOCUMENT_ROOT"]) - 1) : $_SERVER["DOCUMENT_ROOT"]);

	require_once $APP_PATH_ROOT."/api/jogo/v000001/apimodels/Partida.php";

	header('Access-Control-Allow-Origin: *');

	function atualizarDadosPartida() {
		if(	true
			&& isset($_POST['idpartida'])
			&& isset($_POST['idjogo'])			
			&& isset($_POST['idpessoa'])
			&& isset($_POST['idinstituicao'])
			&& isset($_POST['codigoacesso'])
			&& isset($_POST['iddisciplina'])
			&& isset($_POST['datahorapartida'])
			&& isset($_POST['duracaosegundos'])
			&& isset($_POST['pontos']) 
			&& isset($_POST['qtdquestoes'])
			&& isset($_POST['qtdacertos'])

			) {

			$o_partida = new PartidaModel;

			$o_partida->IdPartida 			= $_POST['idpartida'];
			$o_partida->IdJogo				= $_POST['idjogo'];			
			$o_partida->IdPessoa 			= $_POST['idpessoa'];
			$o_partida->IdInstituicao 		= $_POST['idinstituicao'];
			$o_partida->CodigoAcesso 		= $_POST['codigoacesso'];
			$o_partida->IdGoEdDisciplina	= $_POST['iddisciplina'];
			$o_partida->IdGoEdTema	        = isset($_POST['idtema']) && !empty($_POST['idtema']) ? $_POST['idtema'] : null;
			$o_partida->IdGoEdTopico	    = isset($_POST['idtopico']) && !empty($_POST['idtopico']) ? $_POST['idtopico'] : null;
			$o_partida->DataHoraPartida 	= DateTime::createFromFormat("Y-m-d H:i:s", $_POST['datahorapartida'])->format("d-m-Y H:i:s");
			$o_partida->DuracaoSegundos 	= (int) $_POST['duracaosegundos'];
			$o_partida->Pontos 			    = (int) $_POST['pontos'];
			$o_partida->QtdQuestoes 		= (int) $_POST['qtdquestoes'];
			$o_partida->QtdAcertos 			= (int) $_POST['qtdacertos'];
			$o_partida->Desempenho 			= (($o_partida->QtdAcertos? $o_partida->QtdAcertos : 0) / ($o_partida->QtdQuestoes ? $o_partida->QtdQuestoes : 1)) * 100.0;


			//json padrão com os dados da partida
			$dadospartida 		= array(
				"idpartida" 			=> $o_partida->IdPartida,
				"idjogo" 				=> $o_partida->IdJogo,
				"idpessoa" 				=> $o_partida->IdPessoa,
				"idinstituicao"			=> $o_partida->IdInstituicao,
				"codigoacesso" 			=> $o_partida->CodigoAcesso,
				"codigoacesso" 			=> $o_partida->CodigoAcesso,
				"iddisciplina" 			=> $o_partida->IdGoEdDisciplina,
				"idtema" 				=> $o_partida->IdGoEdTema,
				"idtopico" 				=> $o_partida->IdGoEdTopico,
				"datahorapartida" 		=> $o_partida->DataHoraPartida,
				"duracaosegundos" 		=> $o_partida->DuracaoSegundos,
				"pontos" 				=> $o_partida->Pontos,
				"qtdquestoes" 			=> $o_partida->QtdQuestoes,
				"qtdacertos" 			=> $o_partida->QtdAcertos,
				"desempenho" 			=> $o_partida->Desempenho
			);

			//caso o jogo envie 'dadospartida' será adicionado ao básico
			if (isset($_POST['dadospartida']) && !empty($_POST['dadospartida'])) {
				$dadospartidajson_extra = $_POST['dadospartida'];      // lê os dadospartida passado como parâmetro
				$dadospartidaarray_extra = json_decode($dadospartida); // converte o json para um array
				array_push($dadospartida, $dadospartidaarray_extra);   // faz um merge do json padrão com os dados extra
			}

			$o_partida->DadosPartida = json_encode($dadospartida);

			return ($o_partida->save() ? "OK" : "Não foi possível salvar os dados da partida.");
		}
		else {
			return "Não foi possível salvar os dados da partida.";
		}
	}

	$retornoPartida = atualizarDadosPartida();

	echo json_encode($retornoPartida);