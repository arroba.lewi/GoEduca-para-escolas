<?php

	// Incluindo arquivo de configuração
	require_once (substr($_SERVER["DOCUMENT_ROOT"], -1) === "/" ? substr($_SERVER["DOCUMENT_ROOT"], 0, strlen($_SERVER["DOCUMENT_ROOT"]) - 1) : $_SERVER["DOCUMENT_ROOT"])."/escolas/config.php";

	// Conferir se está logado
	if(!Logado()){
		header("location: /$APP_PATH_VERSION");
	}

	$meta_title = "Aluno";
	$meta_description = "Junte-se ao GoEduca!";

	$localCss 	= "dashboard";
	$hideNav = true;
	$hideFooter = true;


	include $APP_PATH_ROOT."/components/header.php";

	include $APP_PATH_ROOT."/components/nav.php";
	include $APP_PATH_ROOT."/view/body-aluno.php";

	include $APP_PATH_ROOT."/components/config/end.php";
?>