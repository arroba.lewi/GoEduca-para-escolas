<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// GoEdQuestaoItemModel
//
// Itens que formam as questões disponíveis na plataforma.
//
// Gerado em: 2018-03-26 05:03:35
// --------------------------------------------------------------------------------
class GoEdQuestaoItemModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdGoEdQuestaoItem;    // char(32), obrigatório - Identificação do Item da Questão GOEDUCA
    private $IdGoEdQuestao;        // char(32), PK, FK, obrigatório - Identificação da Questão GOEDUCA
    private $Item;                 // varchar(32), obrigatório - Identificação do item (A, B, C, D e E)
    private $Resposta;             // tinyint(1), obrigatório - Indica se o item é Certo ou Errado, cada questão só tem um item certo TRUE
    private $Descricao;            // text, opcional - Descrição do item
    private $Explicacao;           // text, opcional - Explicação do item, porque ele é certo ou errado
    private $TipoMidia = 'Texto';  // varchar(64), obrigatório - Indica o tipo de mídia da questão
    private $Dimensao;             // varchar(64), opcional - Dimensão em pixels (Larguara x Altura) para tipos de mídia imagem e vídeo
    private $Status = 'AT';        // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdGoEdQuestaoItem") { return $this->IdGoEdQuestaoItem; }
        if ($name === "IdGoEdQuestao") { return $this->IdGoEdQuestao; }
        if ($name === "Item") { return $this->Item; }
        if ($name === "Resposta") { return $this->Resposta; }
        if ($name === "Descricao") { return $this->Descricao; }
        if ($name === "Explicacao") { return $this->Explicacao; }
        if ($name === "TipoMidia") { return $this->TipoMidia; }
        if ($name === "Dimensao") { return $this->Dimensao; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdGoEdQuestaoItem") {
            if (is_null($value)) {
                $this->IdGoEdQuestaoItem = null;
            }
            else {
                $this->IdGoEdQuestaoItem = substr((string) $value, 0, 32);
            }
            return $this->IdGoEdQuestaoItem;
        }
        if ($name === "IdGoEdQuestao") {
            if (is_null($value)) {
                $this->IdGoEdQuestao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdQuestao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdQuestao;
        }
        if ($name === "Item") {
            if (is_null($value)) {
                $this->Item = null;
            }
            else {
                $this->Item = substr((string) $value, 0, 32);
            }
            return $this->Item;
        }
        if ($name === "Resposta") {
            if (is_null($value)) {
                $this->Resposta = null;
            }
            else {
                if (is_bool($value)) {
                    $this->Resposta = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo BOOL inválido.");
                }
            }
            return $this->Resposta;
        }
        if ($name === "Descricao") {
            if (is_null($value)) {
                $this->Descricao = null;
            }
            else {
                $this->Descricao = substr((string) $value, 0, 65535);
            }
            return $this->Descricao;
        }
        if ($name === "Explicacao") {
            if (is_null($value)) {
                $this->Explicacao = null;
            }
            else {
                $this->Explicacao = substr((string) $value, 0, 65535);
            }
            return $this->Explicacao;
        }
        if ($name === "TipoMidia") {
            if (is_null($value)) {
                $this->TipoMidia = null;
            }
            else {
                $this->TipoMidia = substr((string) $value, 0, 64);
            }
            return $this->TipoMidia;
        }
        if ($name === "Dimensao") {
            if (is_null($value)) {
                $this->Dimensao = null;
            }
            else {
                $this->Dimensao = substr((string) $value, 0, 64);
            }
            return $this->Dimensao;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        goedquestaoitem
                    set 
                        idgoedquestaoitem = " . ( isset($this->IdGoEdQuestaoItem) ? $this->o_db->quote($IdGoEdQuestaoItem) : "null" ) . ", 
                        idgoedquestao = " . ( isset($this->IdGoEdQuestao) ? $this->o_db->quote($IdGoEdQuestao) : "null" ) . ", 
                        item = " . ( isset($this->Item) ? $this->o_db->quote($Item) : "null" ) . ", 
                        resposta = " . ( isset($this->Resposta) ? $this->Resposta : "null" ) . ", 
                        descricao = " . ( isset($this->Descricao) ? $this->o_db->quote($Descricao) : "null" ) . ", 
                        explicacao = " . ( isset($this->Explicacao) ? $this->o_db->quote($Explicacao) : "null" ) . ", 
                        tipomidia = " . ( isset($this->TipoMidia) ? $this->o_db->quote($TipoMidia) : "null" ) . ", 
                        dimensao = " . ( isset($this->Dimensao) ? $this->o_db->quote($Dimensao) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idgoedquestao" . ( isset($this->IdGoEdQuestao) ? " = " . $this->o_db->quote($this->IdGoEdQuestao) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        goedquestaoitem (
                            idgoedquestaoitem, 
                            idgoedquestao, 
                            item, 
                            resposta, 
                            descricao, 
                            explicacao, 
                            tipomidia, 
                            dimensao, 
                            status
                        )
                        values (
                            " . ( isset($this->IdGoEdQuestaoItem) ? $this->o_db->quote($this->IdGoEdQuestaoItem) : "null" ) . ", 
                            " . ( isset($this->IdGoEdQuestao) ? $this->o_db->quote($this->IdGoEdQuestao) : "null" ) . ", 
                            " . ( isset($this->Item) ? $this->o_db->quote($this->Item) : "null" ) . ", 
                            " . ( isset($this->Resposta) ? $this->Resposta : "null" ) . ", 
                            " . ( isset($this->Descricao) ? $this->o_db->quote($this->Descricao) : "null" ) . ", 
                            " . ( isset($this->Explicacao) ? $this->o_db->quote($this->Explicacao) : "null" ) . ", 
                            " . ( isset($this->TipoMidia) ? $this->o_db->quote($this->TipoMidia) : "null" ) . ", 
                            " . ( isset($this->Dimensao) ? $this->o_db->quote($this->Dimensao) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdGoEdQuestao)) {
            $sql = "delete from 
                        goedquestaoitem
                     where 
                        idgoedquestao" . ( isset($this->IdGoEdQuestao) ? " = " . $this->o_db->quote($this->IdGoEdQuestao) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdQuestaoItem = null, 
        string $IdGoEdQuestao = null, 
        string $Item = null, 
        bool $Resposta = null, 
        string $Descricao = null, 
        string $Explicacao = null, 
        string $TipoMidia = null, 
        string $Dimensao = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idgoedquestaoitem as IdGoEdQuestaoItem, 
                    idgoedquestao as IdGoEdQuestao, 
                    item as Item, 
                    resposta as Resposta, 
                    descricao as Descricao, 
                    explicacao as Explicacao, 
                    tipomidia as TipoMidia, 
                    dimensao as Dimensao, 
                    status as Status
                from
                    goedquestaoitem
                where 1 = 1";

        if (isset($IdGoEdQuestaoItem)) { $sql = $sql . " and (idgoedquestaoitem like " . $this->o_db->quote("%" . $IdGoEdQuestaoItem. "%") . ")"; }
        if (isset($IdGoEdQuestao)) { $sql = $sql . " and (idgoedquestao = " . $this->o_db->quote($IdGoEdQuestao) . ")"; }
        if (isset($Item)) { $sql = $sql . " and (item like " . $this->o_db->quote("%" . $Item. "%") . ")"; }
        if (isset($Resposta)) { $sql = $sql . " and (resposta = " . $Resposta . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao like " . $this->o_db->quote("%" . $Descricao. "%") . ")"; }
        if (isset($Explicacao)) { $sql = $sql . " and (explicacao like " . $this->o_db->quote("%" . $Explicacao. "%") . ")"; }
        if (isset($TipoMidia)) { $sql = $sql . " and (tipomidia like " . $this->o_db->quote("%" . $TipoMidia. "%") . ")"; }
        if (isset($Dimensao)) { $sql = $sql . " and (dimensao like " . $this->o_db->quote("%" . $Dimensao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_goedquestaoitem = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdQuestaoItemModel();

                $obj_out->IdGoEdQuestaoItem = $obj_in->IdGoEdQuestaoItem;
                $obj_out->IdGoEdQuestao = $obj_in->IdGoEdQuestao;
                $obj_out->Item = $obj_in->Item;
                $obj_out->Resposta = $obj_in->Resposta;
                $obj_out->Descricao = $obj_in->Descricao;
                $obj_out->Explicacao = $obj_in->Explicacao;
                $obj_out->TipoMidia = $obj_in->TipoMidia;
                $obj_out->Dimensao = $obj_in->Dimensao;
                $obj_out->Status = $obj_in->Status;

                array_push($array_goedquestaoitem, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_goedquestaoitem;
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdGoEdQuestaoItem = null, 
        string $IdGoEdQuestao = null, 
        string $Item = null, 
        bool $Resposta = null, 
        string $Descricao = null, 
        string $Explicacao = null, 
        string $TipoMidia = null, 
        string $Dimensao = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdGoEdQuestaoItem) && is_null($IdGoEdQuestao) && is_null($Item)
             && is_null($Resposta) && is_null($Descricao) && is_null($Explicacao)
             && is_null($TipoMidia) && is_null($Dimensao) && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idgoedquestaoitem as IdGoEdQuestaoItem, 
                    idgoedquestao as IdGoEdQuestao, 
                    item as Item, 
                    resposta as Resposta, 
                    descricao as Descricao, 
                    explicacao as Explicacao, 
                    tipomidia as TipoMidia, 
                    dimensao as Dimensao, 
                    status as Status
                from
                    goedquestaoitem
                where 1 = 1";

        if (isset($IdGoEdQuestaoItem)) { $sql = $sql . " and (idgoedquestaoitem = " . $this->o_db->quote($IdGoEdQuestaoItem) . ")"; }
        if (isset($IdGoEdQuestao)) { $sql = $sql . " and (idgoedquestao = " . $this->o_db->quote($IdGoEdQuestao) . ")"; }
        if (isset($Item)) { $sql = $sql . " and (item = " . $this->o_db->quote($Item) . ")"; }
        if (isset($Resposta)) { $sql = $sql . " and (resposta = " . $Resposta . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao = " . $this->o_db->quote($Descricao) . ")"; }
        if (isset($Explicacao)) { $sql = $sql . " and (explicacao = " . $this->o_db->quote($Explicacao) . ")"; }
        if (isset($TipoMidia)) { $sql = $sql . " and (tipomidia = " . $this->o_db->quote($TipoMidia) . ")"; }
        if (isset($Dimensao)) { $sql = $sql . " and (dimensao = " . $this->o_db->quote($Dimensao) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdQuestaoItemModel();

                $obj_out->IdGoEdQuestaoItem = $obj_in->IdGoEdQuestaoItem;
                $obj_out->IdGoEdQuestao = $obj_in->IdGoEdQuestao;
                $obj_out->Item = $obj_in->Item;
                $obj_out->Resposta = $obj_in->Resposta;
                $obj_out->Descricao = $obj_in->Descricao;
                $obj_out->Explicacao = $obj_in->Explicacao;
                $obj_out->TipoMidia = $obj_in->TipoMidia;
                $obj_out->Dimensao = $obj_in->Dimensao;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdGoEdQuestao)
    {
        $obj = $this->objectByFields(null, $IdGoEdQuestao, null, null, null, null, null, null, null);
        if ($obj) {
            $this->IdGoEdQuestaoItem = $obj->IdGoEdQuestaoItem;
            $this->IdGoEdQuestao = $obj->IdGoEdQuestao;
            $this->Item = $obj->Item;
            $this->Resposta = $obj->Resposta;
            $this->Descricao = $obj->Descricao;
            $this->Explicacao = $obj->Explicacao;
            $this->TipoMidia = $obj->TipoMidia;
            $this->Dimensao = $obj->Dimensao;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields(null, $this->IdGoEdQuestao, null, null, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdGoEdQuestaoItem = null, 
        string $IdGoEdQuestao = null, 
        string $Item = null, 
        bool $Resposta = null, 
        string $Descricao = null, 
        string $Explicacao = null, 
        string $TipoMidia = null, 
        string $Dimensao = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    goedquestaoitem
                where 1 = 1";

        if (isset($IdGoEdQuestaoItem)) { $sql = $sql . " and (idgoedquestaoitem like " . $this->o_db->quote("%" . $IdGoEdQuestaoItem. "%") . ")"; }
        if (isset($IdGoEdQuestao)) { $sql = $sql . " and (idgoedquestao = " . $this->o_db->quote($IdGoEdQuestao) . ")"; }
        if (isset($Item)) { $sql = $sql . " and (item like " . $this->o_db->quote("%" . $Item. "%") . ")"; }
        if (isset($Resposta)) { $sql = $sql . " and (resposta = " . Resposta . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao like " . $this->o_db->quote("%" . $Descricao. "%") . ")"; }
        if (isset($Explicacao)) { $sql = $sql . " and (explicacao like " . $this->o_db->quote("%" . $Explicacao. "%") . ")"; }
        if (isset($TipoMidia)) { $sql = $sql . " and (tipomidia like " . $this->o_db->quote("%" . $TipoMidia. "%") . ")"; }
        if (isset($Dimensao)) { $sql = $sql . " and (dimensao like " . $this->o_db->quote("%" . $Dimensao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
}

?>
