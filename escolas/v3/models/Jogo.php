<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// JogoModel
//
// Jogos disponíveis na platatorma.
//
// Gerado em: 2018-03-26 05:03:43
// --------------------------------------------------------------------------------
class JogoModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdJogo = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdJogo;                   // char(32), PK, obrigatório - Identificação do Jogo
    private $IdJogoProdutor;           // char(32), FK, obrigatório - Identificação do Produtor de Jogos
    private $Titulo;                   // varchar(256), obrigatório - Título do Jogo
    private $Descricao;                // text, opcional - Descrição do Jogo
    private $Versao;                   // varchar(128), opcional - Versão do Jogo
    private $FaixaEtariaInicial;       // smallint(6), opcional - Faixa Etária Inicial sugerida para o jogo
    private $FaixaEtariaFinal;         // smallint(6), opcional - Faixa Etária Final sugerida para o jogo
    private $ModoOperacao = 'ONLINE';  // varchar(8), obrigatório - Modo de operação do jogo (online e/ou offline)
    private $Status = 'AT';            // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdJogo") { return $this->IdJogo; }
        if ($name === "IdJogoProdutor") { return $this->IdJogoProdutor; }
        if ($name === "Titulo") { return $this->Titulo; }
        if ($name === "Descricao") { return $this->Descricao; }
        if ($name === "Versao") { return $this->Versao; }
        if ($name === "FaixaEtariaInicial") { return $this->FaixaEtariaInicial; }
        if ($name === "FaixaEtariaFinal") { return $this->FaixaEtariaFinal; }
        if ($name === "ModoOperacao") { return $this->ModoOperacao; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdJogo") {
            if (is_null($value)) {
                $this->IdJogo = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdJogo = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdJogo;
        }
        if ($name === "IdJogoProdutor") {
            if (is_null($value)) {
                $this->IdJogoProdutor = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdJogoProdutor = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdJogoProdutor;
        }
        if ($name === "Titulo") {
            if (is_null($value)) {
                $this->Titulo = null;
            }
            else {
                $this->Titulo = substr((string) $value, 0, 256);
            }
            return $this->Titulo;
        }
        if ($name === "Descricao") {
            if (is_null($value)) {
                $this->Descricao = null;
            }
            else {
                $this->Descricao = substr((string) $value, 0, 65535);
            }
            return $this->Descricao;
        }
        if ($name === "Versao") {
            if (is_null($value)) {
                $this->Versao = null;
            }
            else {
                $this->Versao = substr((string) $value, 0, 128);
            }
            return $this->Versao;
        }
        if ($name === "FaixaEtariaInicial") {
            if (is_null($value)) {
                $this->FaixaEtariaInicial = null;
            }
            else {
                if (is_int($value)) {
                    $this->FaixaEtariaInicial = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->FaixaEtariaInicial;
        }
        if ($name === "FaixaEtariaFinal") {
            if (is_null($value)) {
                $this->FaixaEtariaFinal = null;
            }
            else {
                if (is_int($value)) {
                    $this->FaixaEtariaFinal = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->FaixaEtariaFinal;
        }
        if ($name === "ModoOperacao") {
            if (is_null($value)) {
                $this->ModoOperacao = null;
            }
            else {
                $this->ModoOperacao = substr((string) $value, 0, 8);
            }
            return $this->ModoOperacao;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        jogo
                    set 
                        idjogo = " . ( isset($this->IdJogo) ? $this->o_db->quote($IdJogo) : "null" ) . ", 
                        idjogoprodutor = " . ( isset($this->IdJogoProdutor) ? $this->o_db->quote($IdJogoProdutor) : "null" ) . ", 
                        titulo = " . ( isset($this->Titulo) ? $this->o_db->quote($Titulo) : "null" ) . ", 
                        descricao = " . ( isset($this->Descricao) ? $this->o_db->quote($Descricao) : "null" ) . ", 
                        versao = " . ( isset($this->Versao) ? $this->o_db->quote($Versao) : "null" ) . ", 
                        faixaetariainicial = " . ( isset($this->FaixaEtariaInicial) ? $this->FaixaEtariaInicial : "null" ) . ", 
                        faixaetariafinal = " . ( isset($this->FaixaEtariaFinal) ? $this->FaixaEtariaFinal : "null" ) . ", 
                        modooperacao = " . ( isset($this->ModoOperacao) ? $this->o_db->quote($ModoOperacao) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idjogo" . ( isset($this->IdJogo) ? " = " . $this->o_db->quote($this->IdJogo) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        jogo (
                            idjogo, 
                            idjogoprodutor, 
                            titulo, 
                            descricao, 
                            versao, 
                            faixaetariainicial, 
                            faixaetariafinal, 
                            modooperacao, 
                            status
                        )
                        values (
                            " . ( isset($this->IdJogo) ? $this->o_db->quote($this->IdJogo) : "null" ) . ", 
                            " . ( isset($this->IdJogoProdutor) ? $this->o_db->quote($this->IdJogoProdutor) : "null" ) . ", 
                            " . ( isset($this->Titulo) ? $this->o_db->quote($this->Titulo) : "null" ) . ", 
                            " . ( isset($this->Descricao) ? $this->o_db->quote($this->Descricao) : "null" ) . ", 
                            " . ( isset($this->Versao) ? $this->o_db->quote($this->Versao) : "null" ) . ", 
                            " . ( isset($this->FaixaEtariaInicial) ? $this->FaixaEtariaInicial : "null" ) . ", 
                            " . ( isset($this->FaixaEtariaFinal) ? $this->FaixaEtariaFinal : "null" ) . ", 
                            " . ( isset($this->ModoOperacao) ? $this->o_db->quote($this->ModoOperacao) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdJogo)) {
            $sql = "delete from 
                        jogo
                     where 
                        idjogo" . ( isset($this->IdJogo) ? " = " . $this->o_db->quote($this->IdJogo) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdJogo = null, 
        string $IdJogoProdutor = null, 
        string $Titulo = null, 
        string $Descricao = null, 
        string $Versao = null, 
        int $FaixaEtariaInicial = null, 
        int $FaixaEtariaFinal = null, 
        string $ModoOperacao = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idjogo as IdJogo, 
                    idjogoprodutor as IdJogoProdutor, 
                    titulo as Titulo, 
                    descricao as Descricao, 
                    versao as Versao, 
                    faixaetariainicial as FaixaEtariaInicial, 
                    faixaetariafinal as FaixaEtariaFinal, 
                    modooperacao as ModoOperacao, 
                    status as Status
                from
                    jogo
                where 1 = 1";

        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdJogoProdutor)) { $sql = $sql . " and (idjogoprodutor = " . $this->o_db->quote($IdJogoProdutor) . ")"; }
        if (isset($Titulo)) { $sql = $sql . " and (titulo like " . $this->o_db->quote("%" . $Titulo. "%") . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao like " . $this->o_db->quote("%" . $Descricao. "%") . ")"; }
        if (isset($Versao)) { $sql = $sql . " and (versao like " . $this->o_db->quote("%" . $Versao. "%") . ")"; }
        if (isset($FaixaEtariaInicial)) { $sql = $sql . " and (faixaetariainicial = " . $FaixaEtariaInicial . ")"; }
        if (isset($FaixaEtariaFinal)) { $sql = $sql . " and (faixaetariafinal = " . $FaixaEtariaFinal . ")"; }
        if (isset($ModoOperacao)) { $sql = $sql . " and (modooperacao like " . $this->o_db->quote("%" . $ModoOperacao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_jogo = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new JogoModel();

                $obj_out->IdJogo = $obj_in->IdJogo;
                $obj_out->IdJogoProdutor = $obj_in->IdJogoProdutor;
                $obj_out->Titulo = $obj_in->Titulo;
                $obj_out->Descricao = $obj_in->Descricao;
                $obj_out->Versao = $obj_in->Versao;
                $obj_out->FaixaEtariaInicial = $obj_in->FaixaEtariaInicial;
                $obj_out->FaixaEtariaFinal = $obj_in->FaixaEtariaFinal;
                $obj_out->ModoOperacao = $obj_in->ModoOperacao;
                $obj_out->Status = $obj_in->Status;

                array_push($array_jogo, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_jogo;
    }

    // --------------------------------------------------------------------------------
    // listByIdJogoProdutorTitulo
    // Lista os registros com base em IdJogoProdutor, Titulo
    // --------------------------------------------------------------------------------
    public function listByIdJogoProdutorTitulo(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdJogoProdutor = null, 
        string $Titulo = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdJogoProdutor, $Titulo, null, null, null, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdJogo = null, 
        string $IdJogoProdutor = null, 
        string $Titulo = null, 
        string $Descricao = null, 
        string $Versao = null, 
        int $FaixaEtariaInicial = null, 
        int $FaixaEtariaFinal = null, 
        string $ModoOperacao = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdJogo) && is_null($IdJogoProdutor) && is_null($Titulo)
             && is_null($Descricao) && is_null($Versao) && is_null($FaixaEtariaInicial)
             && is_null($FaixaEtariaFinal) && is_null($ModoOperacao) && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idjogo as IdJogo, 
                    idjogoprodutor as IdJogoProdutor, 
                    titulo as Titulo, 
                    descricao as Descricao, 
                    versao as Versao, 
                    faixaetariainicial as FaixaEtariaInicial, 
                    faixaetariafinal as FaixaEtariaFinal, 
                    modooperacao as ModoOperacao, 
                    status as Status
                from
                    jogo
                where 1 = 1";

        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdJogoProdutor)) { $sql = $sql . " and (idjogoprodutor = " . $this->o_db->quote($IdJogoProdutor) . ")"; }
        if (isset($Titulo)) { $sql = $sql . " and (titulo = " . $this->o_db->quote($Titulo) . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao = " . $this->o_db->quote($Descricao) . ")"; }
        if (isset($Versao)) { $sql = $sql . " and (versao = " . $this->o_db->quote($Versao) . ")"; }
        if (isset($FaixaEtariaInicial)) { $sql = $sql . " and (faixaetariainicial = " . $FaixaEtariaInicial . ")"; }
        if (isset($FaixaEtariaFinal)) { $sql = $sql . " and (faixaetariafinal = " . $FaixaEtariaFinal . ")"; }
        if (isset($ModoOperacao)) { $sql = $sql . " and (modooperacao = " . $this->o_db->quote($ModoOperacao) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new JogoModel();

                $obj_out->IdJogo = $obj_in->IdJogo;
                $obj_out->IdJogoProdutor = $obj_in->IdJogoProdutor;
                $obj_out->Titulo = $obj_in->Titulo;
                $obj_out->Descricao = $obj_in->Descricao;
                $obj_out->Versao = $obj_in->Versao;
                $obj_out->FaixaEtariaInicial = $obj_in->FaixaEtariaInicial;
                $obj_out->FaixaEtariaFinal = $obj_in->FaixaEtariaFinal;
                $obj_out->ModoOperacao = $obj_in->ModoOperacao;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdJogo)
    {
        $obj = $this->objectByFields($IdJogo, null, null, null, null, null, null, null, null);
        if ($obj) {
            $this->IdJogo = $obj->IdJogo;
            $this->IdJogoProdutor = $obj->IdJogoProdutor;
            $this->Titulo = $obj->Titulo;
            $this->Descricao = $obj->Descricao;
            $this->Versao = $obj->Versao;
            $this->FaixaEtariaInicial = $obj->FaixaEtariaInicial;
            $this->FaixaEtariaFinal = $obj->FaixaEtariaFinal;
            $this->ModoOperacao = $obj->ModoOperacao;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdJogo, null, null, null, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdJogoProdutorTitulo
    // Verifica se existe um registro com IdJogoProdutor, Titulo
    // --------------------------------------------------------------------------------
    public function existsIdJogoProdutorTitulo()
    {
        $obj = $this->objectByFields(null, $this->IdJogoProdutor, $this->Titulo, null, null, null, null, null, null);
        return !($obj && ($obj->IdJogo === $this->IdJogo));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdJogo = null, 
        string $IdJogoProdutor = null, 
        string $Titulo = null, 
        string $Descricao = null, 
        string $Versao = null, 
        int $FaixaEtariaInicial = null, 
        int $FaixaEtariaFinal = null, 
        string $ModoOperacao = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    jogo
                where 1 = 1";

        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdJogoProdutor)) { $sql = $sql . " and (idjogoprodutor = " . $this->o_db->quote($IdJogoProdutor) . ")"; }
        if (isset($Titulo)) { $sql = $sql . " and (titulo like " . $this->o_db->quote("%" . $Titulo. "%") . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao like " . $this->o_db->quote("%" . $Descricao. "%") . ")"; }
        if (isset($Versao)) { $sql = $sql . " and (versao like " . $this->o_db->quote("%" . $Versao. "%") . ")"; }
        if (isset($FaixaEtariaInicial)) { $sql = $sql . " and (faixaetariainicial = " . FaixaEtariaInicial . ")"; }
        if (isset($FaixaEtariaFinal)) { $sql = $sql . " and (faixaetariafinal = " . FaixaEtariaFinal . ")"; }
        if (isset($ModoOperacao)) { $sql = $sql . " and (modooperacao like " . $this->o_db->quote("%" . $ModoOperacao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdJogoProdutorTitulo
    // Conta os registros com base em IdJogoProdutor, Titulo
    // --------------------------------------------------------------------------------
    public function countByIdJogoProdutorTitulo(
        string $IdJogoProdutor = null, 
        string $Titulo = null)
    {
        return $this->countBy(null, $IdJogoProdutor, $Titulo, null, null, null, null, null, null);
    }

}

?>
