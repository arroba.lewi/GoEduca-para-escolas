<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// GoEdCursoModel
//
// Cursos disponibilizados pela plataforma, serão usados como sugestões para as instituições.
//
// Gerado em: 2018-03-26 05:03:22
// --------------------------------------------------------------------------------
class GoEdCursoModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdGoEdCurso = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdGoEdCurso;        // char(32), PK, obrigatório - Identificação do Curso GOEDUCA
    private $Nome;               // varchar(256), obrigatório - Nome do Curso
    private $Sigla;              // varchar(32), obrigatório - Sigla do Curso
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdGoEdCurso") { return $this->IdGoEdCurso; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "Sigla") { return $this->Sigla; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdGoEdCurso") {
            if (is_null($value)) {
                $this->IdGoEdCurso = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdCurso = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdCurso;
        }
        if ($name === "Nome") {
            if (is_null($value)) {
                $this->Nome = null;
            }
            else {
                $this->Nome = substr((string) $value, 0, 256);
            }
            return $this->Nome;
        }
        if ($name === "Sigla") {
            if (is_null($value)) {
                $this->Sigla = null;
            }
            else {
                $this->Sigla = substr((string) $value, 0, 32);
            }
            return $this->Sigla;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        goedcurso
                    set 
                        idgoedcurso = " . ( isset($this->IdGoEdCurso) ? $this->o_db->quote($IdGoEdCurso) : "null" ) . ", 
                        nome = " . ( isset($this->Nome) ? $this->o_db->quote($Nome) : "null" ) . ", 
                        sigla = " . ( isset($this->Sigla) ? $this->o_db->quote($Sigla) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idgoedcurso" . ( isset($this->IdGoEdCurso) ? " = " . $this->o_db->quote($this->IdGoEdCurso) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        goedcurso (
                            idgoedcurso, 
                            nome, 
                            sigla, 
                            status
                        )
                        values (
                            " . ( isset($this->IdGoEdCurso) ? $this->o_db->quote($this->IdGoEdCurso) : "null" ) . ", 
                            " . ( isset($this->Nome) ? $this->o_db->quote($this->Nome) : "null" ) . ", 
                            " . ( isset($this->Sigla) ? $this->o_db->quote($this->Sigla) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdGoEdCurso)) {
            $sql = "delete from 
                        goedcurso
                     where 
                        idgoedcurso" . ( isset($this->IdGoEdCurso) ? " = " . $this->o_db->quote($this->IdGoEdCurso) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdCurso = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idgoedcurso as IdGoEdCurso, 
                    nome as Nome, 
                    sigla as Sigla, 
                    status as Status
                from
                    goedcurso
                where 1 = 1";

        if (isset($IdGoEdCurso)) { $sql = $sql . " and (idgoedcurso = " . $this->o_db->quote($IdGoEdCurso) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_goedcurso = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdCursoModel();

                $obj_out->IdGoEdCurso = $obj_in->IdGoEdCurso;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Status = $obj_in->Status;

                array_push($array_goedcurso, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_goedcurso;
    }

    // --------------------------------------------------------------------------------
    // listByIdGoEdCurso
    // Lista os registros com base em IdGoEdCurso
    // --------------------------------------------------------------------------------
    public function listByIdGoEdCurso(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdCurso = null)
    {
        return $this->listBy($pagenumber, $pagesize, $IdGoEdCurso, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // listByNome
    // Lista os registros com base em Nome
    // --------------------------------------------------------------------------------
    public function listByNome(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $Nome = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $Nome, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdGoEdCurso = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdGoEdCurso) && is_null($Nome) && is_null($Sigla)
             && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idgoedcurso as IdGoEdCurso, 
                    nome as Nome, 
                    sigla as Sigla, 
                    status as Status
                from
                    goedcurso
                where 1 = 1";

        if (isset($IdGoEdCurso)) { $sql = $sql . " and (idgoedcurso = " . $this->o_db->quote($IdGoEdCurso) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome = " . $this->o_db->quote($Nome) . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla = " . $this->o_db->quote($Sigla) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdCursoModel();

                $obj_out->IdGoEdCurso = $obj_in->IdGoEdCurso;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdGoEdCurso)
    {
        $obj = $this->objectByFields($IdGoEdCurso, null, null, null);
        if ($obj) {
            $this->IdGoEdCurso = $obj->IdGoEdCurso;
            $this->Nome = $obj->Nome;
            $this->Sigla = $obj->Sigla;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdGoEdCurso, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdGoEdCurso
    // Verifica se existe um registro com IdGoEdCurso
    // --------------------------------------------------------------------------------
    public function existsIdGoEdCurso()
    {
        $obj = $this->objectByFields($this->IdGoEdCurso, null, null, null);
        return !($obj && ($obj->IdGoEdCurso === $this->IdGoEdCurso));
    }

    // --------------------------------------------------------------------------------
    // existsNome
    // Verifica se existe um registro com Nome
    // --------------------------------------------------------------------------------
    public function existsNome()
    {
        $obj = $this->objectByFields(null, $this->Nome, null, null);
        return !($obj && ($obj->IdGoEdCurso === $this->IdGoEdCurso));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdGoEdCurso = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    goedcurso
                where 1 = 1";

        if (isset($IdGoEdCurso)) { $sql = $sql . " and (idgoedcurso = " . $this->o_db->quote($IdGoEdCurso) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdGoEdCurso
    // Conta os registros com base em IdGoEdCurso
    // --------------------------------------------------------------------------------
    public function countByIdGoEdCurso(
        string $IdGoEdCurso = null)
    {
        return $this->countBy($IdGoEdCurso, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // countByNome
    // Conta os registros com base em Nome
    // --------------------------------------------------------------------------------
    public function countByNome(
        string $Nome = null)
    {
        return $this->countBy(null, $Nome, null, null);
    }

}

?>
