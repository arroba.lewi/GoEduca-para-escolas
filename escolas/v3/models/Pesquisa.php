<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// PesquisaModel
//
// Registra os termos que foram pesquisados.
//
// Gerado em: 2018-03-26 05:03:57
// --------------------------------------------------------------------------------
class PesquisaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->DataHoraPesquisa = DateTime::createFromFormat("d-m-Y H:i:s", date("d-m-Y H:i:s"));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdPesquisa;         // bigint(20), PK, AUTO-INCREMENTO, obrigatório - Identificação da pesquisa
    private $IdPessoa;           // char(32), obrigatório - Identificador da Pessoa
    private $IdInstituicao;      // char(32), opcional - Identificador da Instituição
    private $CodigoAcesso;       // varchar(32), opcional - Representação reduzida alfanumérica do Código de Acesso
    private $DataHoraPesquisa;   // timestamp, obrigatório - Data e hora que ocorreu a pesquisa
    private $Texto;              // varchar(256), obrigatório - Texto pesquisado
    private $Pagina;             // int(11), opcional - Página apresentada no resultado da pesquisa
    private $TamanhoPagina;      // int(11), opcional - Tamanho da página apresentada no resultado da pesquisa
    private $QtdResultados;      // int(11), opcional - Quantidade de resultados da pesquisa

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdPesquisa") { return $this->IdPesquisa; }
        if ($name === "IdPessoa") { return $this->IdPessoa; }
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "CodigoAcesso") { return $this->CodigoAcesso; }
        if ($name === "DataHoraPesquisa") { return ($this->DataHoraPesquisa ? $this->DataHoraPesquisa->format("d-m-Y H:i:s") : null); }
        if ($name === "Texto") { return $this->Texto; }
        if ($name === "Pagina") { return $this->Pagina; }
        if ($name === "TamanhoPagina") { return $this->TamanhoPagina; }
        if ($name === "QtdResultados") { return $this->QtdResultados; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdPesquisa") {
            if (is_null($value)) {
                $this->IdPesquisa = null;
            }
            else {
                if (is_int($value)) {
                    $this->IdPesquisa = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->IdPesquisa;
        }
        if ($name === "IdPessoa") {
            if (is_null($value)) {
                $this->IdPessoa = null;
            }
            else {
                $this->IdPessoa = substr((string) $value, 0, 32);
            }
            return $this->IdPessoa;
        }
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                $this->IdInstituicao = substr((string) $value, 0, 32);
            }
            return $this->IdInstituicao;
        }
        if ($name === "CodigoAcesso") {
            if (is_null($value)) {
                $this->CodigoAcesso = null;
            }
            else {
                $this->CodigoAcesso = substr((string) $value, 0, 32);
            }
            return $this->CodigoAcesso;
        }
        if ($name === "DataHoraPesquisa") {
            if (is_null($value)) {
                $this->DataHoraPesquisa = null;
            }
            else {
                if (isset($value) && DateTime::createFromFormat("d-m-Y H:i:s", $value)) {
                    $this->DataHoraPesquisa = DateTime::createFromFormat("d-m-Y H:i:s", $value);
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo TIMESTAMP(dd-mm-aaaa hh:mi:ss) inválido.");
                }
            }
            return $this->DataHoraPesquisa->format("d-m-Y H:i:s");
        }
        if ($name === "Texto") {
            if (is_null($value)) {
                $this->Texto = null;
            }
            else {
                $this->Texto = substr((string) $value, 0, 256);
            }
            return $this->Texto;
        }
        if ($name === "Pagina") {
            if (is_null($value)) {
                $this->Pagina = null;
            }
            else {
                if (is_int($value)) {
                    $this->Pagina = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->Pagina;
        }
        if ($name === "TamanhoPagina") {
            if (is_null($value)) {
                $this->TamanhoPagina = null;
            }
            else {
                if (is_int($value)) {
                    $this->TamanhoPagina = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->TamanhoPagina;
        }
        if ($name === "QtdResultados") {
            if (is_null($value)) {
                $this->QtdResultados = null;
            }
            else {
                if (is_int($value)) {
                    $this->QtdResultados = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->QtdResultados;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        pesquisa
                    set 
                        idpesquisa = " . ( isset($this->IdPesquisa) ? $this->IdPesquisa : "null" ) . ", 
                        idpessoa = " . ( isset($this->IdPessoa) ? $this->o_db->quote($IdPessoa) : "null" ) . ", 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        codigoacesso = " . ( isset($this->CodigoAcesso) ? $this->o_db->quote($CodigoAcesso) : "null" ) . ", 
                        datahorapesquisa = " . ( isset($this->DataHoraPesquisa) ? $this->DataHoraPesquisa->format("Y-m-d H:i:s") : "null" ) . ", 
                        texto = " . ( isset($this->Texto) ? $this->o_db->quote($Texto) : "null" ) . ", 
                        pagina = " . ( isset($this->Pagina) ? $this->Pagina : "null" ) . ", 
                        tamanhopagina = " . ( isset($this->TamanhoPagina) ? $this->TamanhoPagina : "null" ) . ", 
                        qtdresultados = " . ( isset($this->QtdResultados) ? $this->QtdResultados : "null" ) . "
                    where 
                        idpesquisa" . ( isset($this->IdPesquisa) ? " = " . $this->IdPesquisa : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        pesquisa (
                            idpesquisa, 
                            idpessoa, 
                            idinstituicao, 
                            codigoacesso, 
                            datahorapesquisa, 
                            texto, 
                            pagina, 
                            tamanhopagina, 
                            qtdresultados
                        )
                        values (
                            null, 
                            " . ( isset($this->IdPessoa) ? $this->o_db->quote($this->IdPessoa) : "null" ) . ", 
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->CodigoAcesso) ? $this->o_db->quote($this->CodigoAcesso) : "null" ) . ", 
                            " . ( isset($this->DataHoraPesquisa) ? $this->o_db->quote($this->DataHoraPesquisa->format("Y-m-d H:i:s")) : "null" ) . ", 
                            " . ( isset($this->Texto) ? $this->o_db->quote($this->Texto) : "null" ) . ", 
                            " . ( isset($this->Pagina) ? $this->Pagina : "null" ) . ", 
                            " . ( isset($this->TamanhoPagina) ? $this->TamanhoPagina : "null" ) . ", 
                            " . ( isset($this->QtdResultados) ? $this->QtdResultados : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            if (!$regexists) {
                    $this->IdPesquisa = $this->o_db->lastInsertId();
            }
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdPesquisa)) {
            $sql = "delete from 
                        pesquisa
                     where 
                        idpesquisa" . ( isset($this->IdPesquisa) ? " = " . $this->o_db->quote($this->IdPesquisa) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        int $IdPesquisa = null, 
        string $IdPessoa = null, 
        string $IdInstituicao = null, 
        string $CodigoAcesso = null, 
        string $DataHoraPesquisa = null, 
        string $Texto = null, 
        int $Pagina = null, 
        int $TamanhoPagina = null, 
        int $QtdResultados = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idpesquisa as IdPesquisa, 
                    idpessoa as IdPessoa, 
                    idinstituicao as IdInstituicao, 
                    codigoacesso as CodigoAcesso, 
                    datahorapesquisa as DataHoraPesquisa, 
                    texto as Texto, 
                    pagina as Pagina, 
                    tamanhopagina as TamanhoPagina, 
                    qtdresultados as QtdResultados
                from
                    pesquisa
                where 1 = 1";

        if (isset($IdPesquisa)) { $sql = $sql . " and (idpesquisa = " . $IdPesquisa . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa like " . $this->o_db->quote("%" . $IdPessoa. "%") . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao like " . $this->o_db->quote("%" . $IdInstituicao. "%") . ")"; }
        if (isset($CodigoAcesso)) { $sql = $sql . " and (codigoacesso like " . $this->o_db->quote("%" . $CodigoAcesso. "%") . ")"; }
        if (isset($DataHoraPesquisa)) { $sql = $sql . " and (datahorapesquisa = " . $this->o_db->quote(isset($DataHoraPesquisa) ? DateTime::createFromFormat("Y-m-d H:i:s", $DataHoraPesquisa)->format("Y-m-d H:i:s") : "") . ")"; }
        if (isset($Texto)) { $sql = $sql . " and (texto like " . $this->o_db->quote("%" . $Texto. "%") . ")"; }
        if (isset($Pagina)) { $sql = $sql . " and (pagina = " . $Pagina . ")"; }
        if (isset($TamanhoPagina)) { $sql = $sql . " and (tamanhopagina = " . $TamanhoPagina . ")"; }
        if (isset($QtdResultados)) { $sql = $sql . " and (qtdresultados = " . $QtdResultados . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_pesquisa = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new PesquisaModel();

                $obj_out->IdPesquisa = $obj_in->IdPesquisa;
                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->CodigoAcesso = $obj_in->CodigoAcesso;
                $obj_out->DataHoraPesquisa = $obj_in->DataHoraPesquisa;
                $obj_out->Texto = $obj_in->Texto;
                $obj_out->Pagina = $obj_in->Pagina;
                $obj_out->TamanhoPagina = $obj_in->TamanhoPagina;
                $obj_out->QtdResultados = $obj_in->QtdResultados;

                array_push($array_pesquisa, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_pesquisa;
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        int $IdPesquisa = null, 
        string $IdPessoa = null, 
        string $IdInstituicao = null, 
        string $CodigoAcesso = null, 
        string $DataHoraPesquisa = null, 
        string $Texto = null, 
        int $Pagina = null, 
        int $TamanhoPagina = null, 
        int $QtdResultados = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdPesquisa) && is_null($IdPessoa) && is_null($IdInstituicao)
             && is_null($CodigoAcesso) && is_null($DataHoraPesquisa) && is_null($Texto)
             && is_null($Pagina) && is_null($TamanhoPagina) && is_null($QtdResultados)) {
            return null;
        }

        $sql = "select
                    idpesquisa as IdPesquisa, 
                    idpessoa as IdPessoa, 
                    idinstituicao as IdInstituicao, 
                    codigoacesso as CodigoAcesso, 
                    datahorapesquisa as DataHoraPesquisa, 
                    texto as Texto, 
                    pagina as Pagina, 
                    tamanhopagina as TamanhoPagina, 
                    qtdresultados as QtdResultados
                from
                    pesquisa
                where 1 = 1";

        if (isset($IdPesquisa)) { $sql = $sql . " and (idpesquisa = " . $IdPesquisa . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($CodigoAcesso)) { $sql = $sql . " and (codigoacesso = " . $this->o_db->quote($CodigoAcesso) . ")"; }
        if (isset($DataHoraPesquisa)) { $sql = $sql . " and (datahorapesquisa = " . $this->o_db->quote(isset($DataHoraPesquisa) ? DateTime::createFromFormat("Y-m-d H:i:s", $DataHoraPesquisa)->format("Y-m-d H:i:s") : "") . ")"; }
        if (isset($Texto)) { $sql = $sql . " and (texto = " . $this->o_db->quote($Texto) . ")"; }
        if (isset($Pagina)) { $sql = $sql . " and (pagina = " . $Pagina . ")"; }
        if (isset($TamanhoPagina)) { $sql = $sql . " and (tamanhopagina = " . $TamanhoPagina . ")"; }
        if (isset($QtdResultados)) { $sql = $sql . " and (qtdresultados = " . $QtdResultados . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new PesquisaModel();

                $obj_out->IdPesquisa = $obj_in->IdPesquisa;
                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->CodigoAcesso = $obj_in->CodigoAcesso;
                $obj_out->DataHoraPesquisa = $obj_in->DataHoraPesquisa;
                $obj_out->Texto = $obj_in->Texto;
                $obj_out->Pagina = $obj_in->Pagina;
                $obj_out->TamanhoPagina = $obj_in->TamanhoPagina;
                $obj_out->QtdResultados = $obj_in->QtdResultados;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(int $IdPesquisa)
    {
        $obj = $this->objectByFields($IdPesquisa, null, null, null, null, null, null, null, null);
        if ($obj) {
            $this->IdPesquisa = $obj->IdPesquisa;
            $this->IdPessoa = $obj->IdPessoa;
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->CodigoAcesso = $obj->CodigoAcesso;
            $this->DataHoraPesquisa = $obj->DataHoraPesquisa;
            $this->Texto = $obj->Texto;
            $this->Pagina = $obj->Pagina;
            $this->TamanhoPagina = $obj->TamanhoPagina;
            $this->QtdResultados = $obj->QtdResultados;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdPesquisa, null, null, null, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        int $IdPesquisa = null, 
        string $IdPessoa = null, 
        string $IdInstituicao = null, 
        string $CodigoAcesso = null, 
        string $DataHoraPesquisa = null, 
        string $Texto = null, 
        int $Pagina = null, 
        int $TamanhoPagina = null, 
        int $QtdResultados = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    pesquisa
                where 1 = 1";

        if (isset($IdPesquisa)) { $sql = $sql . " and (idpesquisa = " . IdPesquisa . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa like " . $this->o_db->quote("%" . $IdPessoa. "%") . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao like " . $this->o_db->quote("%" . $IdInstituicao. "%") . ")"; }
        if (isset($CodigoAcesso)) { $sql = $sql . " and (codigoacesso like " . $this->o_db->quote("%" . $CodigoAcesso. "%") . ")"; }
        if (isset($DataHoraPesquisa)) { $sql = $sql . " and (datahorapesquisa = " . $this->o_db->quote(DateTime::createFromFormat("Y-m-d H:i:s", $DataHoraPesquisa) ? DateTime::createFromFormat("Y-m-d H:i:s", $DataHoraPesquisa)->format("Y-m-d H:i:s") : "") . ")"; }
        if (isset($Texto)) { $sql = $sql . " and (texto like " . $this->o_db->quote("%" . $Texto. "%") . ")"; }
        if (isset($Pagina)) { $sql = $sql . " and (pagina = " . Pagina . ")"; }
        if (isset($TamanhoPagina)) { $sql = $sql . " and (tamanhopagina = " . TamanhoPagina . ")"; }
        if (isset($QtdResultados)) { $sql = $sql . " and (qtdresultados = " . QtdResultados . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
}

?>
