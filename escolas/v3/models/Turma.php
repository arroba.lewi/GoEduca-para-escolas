<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// TurmaModel
//
// Definição das turmas das instituições.
//
// Gerado em: 2018-03-26 05:04:07
// --------------------------------------------------------------------------------
class TurmaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdTurma = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdTurma;            // char(32), PK, obrigatório - Identificador da Turma
    private $IdInstituicao;      // char(32), PK, FK, obrigatório - Identificador da Instituição
    private $IdUnidade;          // char(32), PK, FK, obrigatório - Identificador da Unidade
    private $IdCurso;            // char(32), PK, FK, obrigatório - Identificador do Curso
    private $IdEtapa;            // char(32), FK, opcional - Identificador do Etapa de um Curso
    private $IdTurno;            // char(32), FK, opcional - Identificador do Turno
    private $PeriodoLetivo;      // varchar(128), obrigatório - Período Letivo que ocorreu a turma (Ano, Ano/Semestre, Ano/Mês, etc)
    private $Sigla;              // varchar(32), obrigatório - Sigla da Turma
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdTurma") { return $this->IdTurma; }
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "IdUnidade") { return $this->IdUnidade; }
        if ($name === "IdCurso") { return $this->IdCurso; }
        if ($name === "IdEtapa") { return $this->IdEtapa; }
        if ($name === "IdTurno") { return $this->IdTurno; }
        if ($name === "PeriodoLetivo") { return $this->PeriodoLetivo; }
        if ($name === "Sigla") { return $this->Sigla; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdTurma") {
            if (is_null($value)) {
                $this->IdTurma = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdTurma = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdTurma;
        }
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdInstituicao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdInstituicao;
        }
        if ($name === "IdUnidade") {
            if (is_null($value)) {
                $this->IdUnidade = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdUnidade = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdUnidade;
        }
        if ($name === "IdCurso") {
            if (is_null($value)) {
                $this->IdCurso = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdCurso = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdCurso;
        }
        if ($name === "IdEtapa") {
            if (is_null($value)) {
                $this->IdEtapa = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdEtapa = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdEtapa;
        }
        if ($name === "IdTurno") {
            if (is_null($value)) {
                $this->IdTurno = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdTurno = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdTurno;
        }
        if ($name === "PeriodoLetivo") {
            if (is_null($value)) {
                $this->PeriodoLetivo = null;
            }
            else {
                $this->PeriodoLetivo = substr((string) $value, 0, 128);
            }
            return $this->PeriodoLetivo;
        }
        if ($name === "Sigla") {
            if (is_null($value)) {
                $this->Sigla = null;
            }
            else {
                $this->Sigla = substr((string) $value, 0, 32);
            }
            return $this->Sigla;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        turma
                    set 
                        idturma = " . ( isset($this->IdTurma) ? $this->o_db->quote($IdTurma) : "null" ) . ", 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        idunidade = " . ( isset($this->IdUnidade) ? $this->o_db->quote($IdUnidade) : "null" ) . ", 
                        idcurso = " . ( isset($this->IdCurso) ? $this->o_db->quote($IdCurso) : "null" ) . ", 
                        idetapa = " . ( isset($this->IdEtapa) ? $this->o_db->quote($IdEtapa) : "null" ) . ", 
                        idturno = " . ( isset($this->IdTurno) ? $this->o_db->quote($IdTurno) : "null" ) . ", 
                        periodoletivo = " . ( isset($this->PeriodoLetivo) ? $this->o_db->quote($PeriodoLetivo) : "null" ) . ", 
                        sigla = " . ( isset($this->Sigla) ? $this->o_db->quote($Sigla) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idturma" . ( isset($this->IdTurma) ? " = " . $this->o_db->quote($this->IdTurma) : " is null" ) . "
                        and
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and
                        idunidade" . ( isset($this->IdUnidade) ? " = " . $this->o_db->quote($this->IdUnidade) : " is null" ) . "
                        and
                        idcurso" . ( isset($this->IdCurso) ? " = " . $this->o_db->quote($this->IdCurso) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        turma (
                            idturma, 
                            idinstituicao, 
                            idunidade, 
                            idcurso, 
                            idetapa, 
                            idturno, 
                            periodoletivo, 
                            sigla, 
                            status
                        )
                        values (
                            " . ( isset($this->IdTurma) ? $this->o_db->quote($this->IdTurma) : "null" ) . ", 
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->IdUnidade) ? $this->o_db->quote($this->IdUnidade) : "null" ) . ", 
                            " . ( isset($this->IdCurso) ? $this->o_db->quote($this->IdCurso) : "null" ) . ", 
                            " . ( isset($this->IdEtapa) ? $this->o_db->quote($this->IdEtapa) : "null" ) . ", 
                            " . ( isset($this->IdTurno) ? $this->o_db->quote($this->IdTurno) : "null" ) . ", 
                            " . ( isset($this->PeriodoLetivo) ? $this->o_db->quote($this->PeriodoLetivo) : "null" ) . ", 
                            " . ( isset($this->Sigla) ? $this->o_db->quote($this->Sigla) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdTurma) && isset($this->IdInstituicao) && isset($this->IdUnidade) && isset($this->IdCurso)) {
            $sql = "delete from 
                        turma
                     where 
                        idturma" . ( isset($this->IdTurma) ? " = " . $this->o_db->quote($this->IdTurma) : " is null" ) . "
                        and 
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and 
                        idunidade" . ( isset($this->IdUnidade) ? " = " . $this->o_db->quote($this->IdUnidade) : " is null" ) . "
                        and 
                        idcurso" . ( isset($this->IdCurso) ? " = " . $this->o_db->quote($this->IdCurso) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdTurma = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $IdCurso = null, 
        string $IdEtapa = null, 
        string $IdTurno = null, 
        string $PeriodoLetivo = null, 
        string $Sigla = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idturma as IdTurma, 
                    idinstituicao as IdInstituicao, 
                    idunidade as IdUnidade, 
                    idcurso as IdCurso, 
                    idetapa as IdEtapa, 
                    idturno as IdTurno, 
                    periodoletivo as PeriodoLetivo, 
                    sigla as Sigla, 
                    status as Status
                from
                    turma
                where 1 = 1";

        if (isset($IdTurma)) { $sql = $sql . " and (idturma = " . $this->o_db->quote($IdTurma) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($IdEtapa)) { $sql = $sql . " and (idetapa = " . $this->o_db->quote($IdEtapa) . ")"; }
        if (isset($IdTurno)) { $sql = $sql . " and (idturno = " . $this->o_db->quote($IdTurno) . ")"; }
        if (isset($PeriodoLetivo)) { $sql = $sql . " and (periodoletivo like " . $this->o_db->quote("%" . $PeriodoLetivo. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_turma = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new TurmaModel();

                $obj_out->IdTurma = $obj_in->IdTurma;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdUnidade = $obj_in->IdUnidade;
                $obj_out->IdCurso = $obj_in->IdCurso;
                $obj_out->IdEtapa = $obj_in->IdEtapa;
                $obj_out->IdTurno = $obj_in->IdTurno;
                $obj_out->PeriodoLetivo = $obj_in->PeriodoLetivo;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Status = $obj_in->Status;

                array_push($array_turma, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_turma;
    }

    // --------------------------------------------------------------------------------
    // listByIdInstituicaoIdUnidadeIdCursoIdEtapaIdTurnoPeriodoLetivoSigla
    // Lista os registros com base em IdInstituicao, IdUnidade, IdCurso, IdEtapa, IdTurno, PeriodoLetivo, Sigla
    // --------------------------------------------------------------------------------
    public function listByIdInstituicaoIdUnidadeIdCursoIdEtapaIdTurnoPeriodoLetivoSigla(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $IdCurso = null, 
        string $IdEtapa = null, 
        string $IdTurno = null, 
        string $PeriodoLetivo = null, 
        string $Sigla = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdInstituicao, $IdUnidade, $IdCurso, $IdEtapa, $IdTurno, $PeriodoLetivo, $Sigla, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdTurma = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $IdCurso = null, 
        string $IdEtapa = null, 
        string $IdTurno = null, 
        string $PeriodoLetivo = null, 
        string $Sigla = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdTurma) && is_null($IdInstituicao) && is_null($IdUnidade)
             && is_null($IdCurso) && is_null($IdEtapa) && is_null($IdTurno)
             && is_null($PeriodoLetivo) && is_null($Sigla) && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idturma as IdTurma, 
                    idinstituicao as IdInstituicao, 
                    idunidade as IdUnidade, 
                    idcurso as IdCurso, 
                    idetapa as IdEtapa, 
                    idturno as IdTurno, 
                    periodoletivo as PeriodoLetivo, 
                    sigla as Sigla, 
                    status as Status
                from
                    turma
                where 1 = 1";

        if (isset($IdTurma)) { $sql = $sql . " and (idturma = " . $this->o_db->quote($IdTurma) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($IdEtapa)) { $sql = $sql . " and (idetapa = " . $this->o_db->quote($IdEtapa) . ")"; }
        if (isset($IdTurno)) { $sql = $sql . " and (idturno = " . $this->o_db->quote($IdTurno) . ")"; }
        if (isset($PeriodoLetivo)) { $sql = $sql . " and (periodoletivo = " . $this->o_db->quote($PeriodoLetivo) . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla = " . $this->o_db->quote($Sigla) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new TurmaModel();

                $obj_out->IdTurma = $obj_in->IdTurma;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdUnidade = $obj_in->IdUnidade;
                $obj_out->IdCurso = $obj_in->IdCurso;
                $obj_out->IdEtapa = $obj_in->IdEtapa;
                $obj_out->IdTurno = $obj_in->IdTurno;
                $obj_out->PeriodoLetivo = $obj_in->PeriodoLetivo;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdTurma, string $IdInstituicao, string $IdUnidade, string $IdCurso)
    {
        $obj = $this->objectByFields($IdTurma, $IdInstituicao, $IdUnidade, $IdCurso, null, null, null, null, null);
        if ($obj) {
            $this->IdTurma = $obj->IdTurma;
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->IdUnidade = $obj->IdUnidade;
            $this->IdCurso = $obj->IdCurso;
            $this->IdEtapa = $obj->IdEtapa;
            $this->IdTurno = $obj->IdTurno;
            $this->PeriodoLetivo = $obj->PeriodoLetivo;
            $this->Sigla = $obj->Sigla;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdTurma, $this->IdInstituicao, $this->IdUnidade, $this->IdCurso, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdInstituicaoIdUnidadeIdCursoIdEtapaIdTurnoPeriodoLetivoSigla
    // Verifica se existe um registro com IdInstituicao, IdUnidade, IdCurso, IdEtapa, IdTurno, PeriodoLetivo, Sigla
    // --------------------------------------------------------------------------------
    public function existsIdInstituicaoIdUnidadeIdCursoIdEtapaIdTurnoPeriodoLetivoSigla()
    {
        $obj = $this->objectByFields(null, $this->IdInstituicao, $this->IdUnidade, $this->IdCurso, $this->IdEtapa, $this->IdTurno, $this->PeriodoLetivo, $this->Sigla, null);
        return !($obj && ($obj->IdTurma === $this->IdTurma) && ($obj->IdInstituicao === $this->IdInstituicao) && ($obj->IdUnidade === $this->IdUnidade) && ($obj->IdCurso === $this->IdCurso));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdTurma = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $IdCurso = null, 
        string $IdEtapa = null, 
        string $IdTurno = null, 
        string $PeriodoLetivo = null, 
        string $Sigla = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    turma
                where 1 = 1";

        if (isset($IdTurma)) { $sql = $sql . " and (idturma = " . $this->o_db->quote($IdTurma) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($IdEtapa)) { $sql = $sql . " and (idetapa = " . $this->o_db->quote($IdEtapa) . ")"; }
        if (isset($IdTurno)) { $sql = $sql . " and (idturno = " . $this->o_db->quote($IdTurno) . ")"; }
        if (isset($PeriodoLetivo)) { $sql = $sql . " and (periodoletivo like " . $this->o_db->quote("%" . $PeriodoLetivo. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdInstituicaoIdUnidadeIdCursoIdEtapaIdTurnoPeriodoLetivoSigla
    // Conta os registros com base em IdInstituicao, IdUnidade, IdCurso, IdEtapa, IdTurno, PeriodoLetivo, Sigla
    // --------------------------------------------------------------------------------
    public function countByIdInstituicaoIdUnidadeIdCursoIdEtapaIdTurnoPeriodoLetivoSigla(
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $IdCurso = null, 
        string $IdEtapa = null, 
        string $IdTurno = null, 
        string $PeriodoLetivo = null, 
        string $Sigla = null)
    {
        return $this->countBy(null, $IdInstituicao, $IdUnidade, $IdCurso, $IdEtapa, $IdTurno, $PeriodoLetivo, $Sigla, null);
    }

}

?>
