<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// PessoaModel
//
// Pessoas (alunos) que irão utilizar a plataforma como meio de aprendizado.
//
// Gerado em: 2018-03-26 05:03:59
// --------------------------------------------------------------------------------
class PessoaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdPessoa = md5(uniqid(rand(), true));
        $this->DataNasc = DateTime::createFromFormat("d-m-Y", "2000-01-01");
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdPessoa;           // char(32), PK, obrigatório - Identificador da Pessoa
    private $Nome;               // varchar(256), obrigatório - Nome da Pessoa
    private $DataNasc;           // date, opcional - Data de Nascimento da Pessoa
    private $Sexo = 'M';         // char(1), obrigatório - Sexo da Pessoa
    private $Endereco;           // varchar(256), opcional - Endereço
    private $Complemento;        // varchar(256), opcional - Complemento do Endereço
    private $Bairro;             // varchar(256), opcional - Bairro
    private $Cidade;             // varchar(256), opcional - Cidade
    private $UF;                 // char(2), opcional - UF
    private $Pais = 'Brasil';    // varchar(256), opcional - País
    private $Telefone;           // varchar(256), opcional - Número do telefone
    private $Email;              // varchar(256), opcional - Endereço e-mail
    private $Apelido;            // varchar(64), opcional - Como a pessoa deseja ser conhecida
    private $PNE = false;        // tinyint(1), obrigatório - Portadora de Necessidades Especiais
    private $Observacao;         // text, opcional - Observações sobre a pessoa
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdPessoa") { return $this->IdPessoa; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "DataNasc") { return ($this->DataNasc ? $this->DataNasc->format("d-m-Y") : null); }
        if ($name === "Sexo") { return $this->Sexo; }
        if ($name === "Endereco") { return $this->Endereco; }
        if ($name === "Complemento") { return $this->Complemento; }
        if ($name === "Bairro") { return $this->Bairro; }
        if ($name === "Cidade") { return $this->Cidade; }
        if ($name === "UF") { return $this->UF; }
        if ($name === "Pais") { return $this->Pais; }
        if ($name === "Telefone") { return $this->Telefone; }
        if ($name === "Email") { return $this->Email; }
        if ($name === "Apelido") { return $this->Apelido; }
        if ($name === "PNE") { return $this->PNE; }
        if ($name === "Observacao") { return $this->Observacao; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdPessoa") {
            if (is_null($value)) {
                $this->IdPessoa = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdPessoa = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdPessoa;
        }
        if ($name === "Nome") {
            if (is_null($value)) {
                $this->Nome = null;
            }
            else {
                $this->Nome = substr((string) $value, 0, 256);
            }
            return $this->Nome;
        }
        if ($name === "DataNasc") {
            if (is_null($value)) {
                $this->DataNasc = null;
            }
            else {
                if (isset($value) && DateTime::createFromFormat("d-m-Y", $value)) {
                    $this->DataNasc = DateTime::createFromFormat("d-m-Y", $value);
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo DATE(dd-mm-aaaa) inválido.");
                }
            }
            return $this->DataNasc->format("d-m-Y");
        }
        if ($name === "Sexo") {
            if (is_null($value)) {
                $this->Sexo = null;
            }
            else {
                $this->Sexo = substr((string) $value, 0, 1);
            }
            return $this->Sexo;
        }
        if ($name === "Endereco") {
            if (is_null($value)) {
                $this->Endereco = null;
            }
            else {
                $this->Endereco = substr((string) $value, 0, 256);
            }
            return $this->Endereco;
        }
        if ($name === "Complemento") {
            if (is_null($value)) {
                $this->Complemento = null;
            }
            else {
                $this->Complemento = substr((string) $value, 0, 256);
            }
            return $this->Complemento;
        }
        if ($name === "Bairro") {
            if (is_null($value)) {
                $this->Bairro = null;
            }
            else {
                $this->Bairro = substr((string) $value, 0, 256);
            }
            return $this->Bairro;
        }
        if ($name === "Cidade") {
            if (is_null($value)) {
                $this->Cidade = null;
            }
            else {
                $this->Cidade = substr((string) $value, 0, 256);
            }
            return $this->Cidade;
        }
        if ($name === "UF") {
            if (is_null($value)) {
                $this->UF = null;
            }
            else {
                $this->UF = substr((string) $value, 0, 2);
            }
            return $this->UF;
        }
        if ($name === "Pais") {
            if (is_null($value)) {
                $this->Pais = null;
            }
            else {
                $this->Pais = substr((string) $value, 0, 256);
            }
            return $this->Pais;
        }
        if ($name === "Telefone") {
            if (is_null($value)) {
                $this->Telefone = null;
            }
            else {
                $this->Telefone = substr((string) $value, 0, 256);
            }
            return $this->Telefone;
        }
        if ($name === "Email") {
            if (is_null($value)) {
                $this->Email = null;
            }
            else {
                $this->Email = substr((string) $value, 0, 256);
            }
            return $this->Email;
        }
        if ($name === "Apelido") {
            if (is_null($value)) {
                $this->Apelido = null;
            }
            else {
                $this->Apelido = substr((string) $value, 0, 64);
            }
            return $this->Apelido;
        }
        if ($name === "PNE") {
            if (is_null($value)) {
                $this->PNE = null;
            }
            else {
                if (is_bool($value)) {
                    $this->PNE = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo BOOL inválido.");
                }
            }
            return $this->PNE;
        }
        if ($name === "Observacao") {
            if (is_null($value)) {
                $this->Observacao = null;
            }
            else {
                $this->Observacao = substr((string) $value, 0, 65535);
            }
            return $this->Observacao;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        pessoa
                    set 
                        idpessoa = " . ( isset($this->IdPessoa) ? $this->o_db->quote($IdPessoa) : "null" ) . ", 
                        nome = " . ( isset($this->Nome) ? $this->o_db->quote($Nome) : "null" ) . ", 
                        datanasc = " . ( isset($this->DataNasc) ? $this->DataNasc->format("Y-m-d") : "null" ) . ", 
                        sexo = " . ( isset($this->Sexo) ? $this->o_db->quote($Sexo) : "null" ) . ", 
                        endereco = " . ( isset($this->Endereco) ? $this->o_db->quote($Endereco) : "null" ) . ", 
                        complemento = " . ( isset($this->Complemento) ? $this->o_db->quote($Complemento) : "null" ) . ", 
                        bairro = " . ( isset($this->Bairro) ? $this->o_db->quote($Bairro) : "null" ) . ", 
                        cidade = " . ( isset($this->Cidade) ? $this->o_db->quote($Cidade) : "null" ) . ", 
                        uf = " . ( isset($this->UF) ? $this->o_db->quote($UF) : "null" ) . ", 
                        pais = " . ( isset($this->Pais) ? $this->o_db->quote($Pais) : "null" ) . ", 
                        telefone = " . ( isset($this->Telefone) ? $this->o_db->quote($Telefone) : "null" ) . ", 
                        email = " . ( isset($this->Email) ? $this->o_db->quote($Email) : "null" ) . ", 
                        apelido = " . ( isset($this->Apelido) ? $this->o_db->quote($Apelido) : "null" ) . ", 
                        pne = " . ( isset($this->PNE) ? $this->PNE : "null" ) . ", 
                        observacao = " . ( isset($this->Observacao) ? $this->o_db->quote($Observacao) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idpessoa" . ( isset($this->IdPessoa) ? " = " . $this->o_db->quote($this->IdPessoa) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        pessoa (
                            idpessoa, 
                            nome, 
                            datanasc, 
                            sexo, 
                            endereco, 
                            complemento, 
                            bairro, 
                            cidade, 
                            uf, 
                            pais, 
                            telefone, 
                            email, 
                            apelido, 
                            pne, 
                            observacao, 
                            status
                        )
                        values (
                            " . ( isset($this->IdPessoa) ? $this->o_db->quote($this->IdPessoa) : "null" ) . ", 
                            " . ( isset($this->Nome) ? $this->o_db->quote($this->Nome) : "null" ) . ", 
                            " . ( isset($this->DataNasc) ? $this->o_db->quote($this->DataNasc->format("Y-m-d")) : "null" ) . ", 
                            " . ( isset($this->Sexo) ? $this->o_db->quote($this->Sexo) : "null" ) . ", 
                            " . ( isset($this->Endereco) ? $this->o_db->quote($this->Endereco) : "null" ) . ", 
                            " . ( isset($this->Complemento) ? $this->o_db->quote($this->Complemento) : "null" ) . ", 
                            " . ( isset($this->Bairro) ? $this->o_db->quote($this->Bairro) : "null" ) . ", 
                            " . ( isset($this->Cidade) ? $this->o_db->quote($this->Cidade) : "null" ) . ", 
                            " . ( isset($this->UF) ? $this->o_db->quote($this->UF) : "null" ) . ", 
                            " . ( isset($this->Pais) ? $this->o_db->quote($this->Pais) : "null" ) . ", 
                            " . ( isset($this->Telefone) ? $this->o_db->quote($this->Telefone) : "null" ) . ", 
                            " . ( isset($this->Email) ? $this->o_db->quote($this->Email) : "null" ) . ", 
                            " . ( isset($this->Apelido) ? $this->o_db->quote($this->Apelido) : "null" ) . ", 
                            " . ( isset($this->PNE) ? $this->PNE : "null" ) . ", 
                            " . ( isset($this->Observacao) ? $this->o_db->quote($this->Observacao) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdPessoa)) {
            $sql = "delete from 
                        pessoa
                     where 
                        idpessoa" . ( isset($this->IdPessoa) ? " = " . $this->o_db->quote($this->IdPessoa) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdPessoa = null, 
        string $Nome = null, 
        string $DataNasc = null, 
        string $Sexo = null, 
        string $Endereco = null, 
        string $Complemento = null, 
        string $Bairro = null, 
        string $Cidade = null, 
        string $UF = null, 
        string $Pais = null, 
        string $Telefone = null, 
        string $Email = null, 
        string $Apelido = null, 
        bool $PNE = null, 
        string $Observacao = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idpessoa as IdPessoa, 
                    nome as Nome, 
                    datanasc as DataNasc, 
                    sexo as Sexo, 
                    endereco as Endereco, 
                    complemento as Complemento, 
                    bairro as Bairro, 
                    cidade as Cidade, 
                    uf as UF, 
                    pais as Pais, 
                    telefone as Telefone, 
                    email as Email, 
                    apelido as Apelido, 
                    pne as PNE, 
                    observacao as Observacao, 
                    status as Status
                from
                    pessoa
                where 1 = 1";

        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($DataNasc)) { $sql = $sql . " and (datanasc = " . $this->o_db->quote(isset($DataNasc) ? DateTime::createFromFormat("d-m-Y", $DataNasc)->format("Y-m-d") : "") . ")"; }
        if (isset($Sexo)) { $sql = $sql . " and (sexo like " . $this->o_db->quote("%" . $Sexo. "%") . ")"; }
        if (isset($Endereco)) { $sql = $sql . " and (endereco like " . $this->o_db->quote("%" . $Endereco. "%") . ")"; }
        if (isset($Complemento)) { $sql = $sql . " and (complemento like " . $this->o_db->quote("%" . $Complemento. "%") . ")"; }
        if (isset($Bairro)) { $sql = $sql . " and (bairro like " . $this->o_db->quote("%" . $Bairro. "%") . ")"; }
        if (isset($Cidade)) { $sql = $sql . " and (cidade like " . $this->o_db->quote("%" . $Cidade. "%") . ")"; }
        if (isset($UF)) { $sql = $sql . " and (uf like " . $this->o_db->quote("%" . $UF. "%") . ")"; }
        if (isset($Pais)) { $sql = $sql . " and (pais like " . $this->o_db->quote("%" . $Pais. "%") . ")"; }
        if (isset($Telefone)) { $sql = $sql . " and (telefone like " . $this->o_db->quote("%" . $Telefone. "%") . ")"; }
        if (isset($Email)) { $sql = $sql . " and (email like " . $this->o_db->quote("%" . $Email. "%") . ")"; }
        if (isset($Apelido)) { $sql = $sql . " and (apelido like " . $this->o_db->quote("%" . $Apelido. "%") . ")"; }
        if (isset($PNE)) { $sql = $sql . " and (pne = " . $PNE . ")"; }
        if (isset($Observacao)) { $sql = $sql . " and (observacao like " . $this->o_db->quote("%" . $Observacao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_pessoa = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new PessoaModel();

                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->DataNasc = $obj_in->DataNasc;
                $obj_out->Sexo = $obj_in->Sexo;
                $obj_out->Endereco = $obj_in->Endereco;
                $obj_out->Complemento = $obj_in->Complemento;
                $obj_out->Bairro = $obj_in->Bairro;
                $obj_out->Cidade = $obj_in->Cidade;
                $obj_out->UF = $obj_in->UF;
                $obj_out->Pais = $obj_in->Pais;
                $obj_out->Telefone = $obj_in->Telefone;
                $obj_out->Email = $obj_in->Email;
                $obj_out->Apelido = $obj_in->Apelido;
                $obj_out->PNE = $obj_in->PNE;
                $obj_out->Observacao = $obj_in->Observacao;
                $obj_out->Status = $obj_in->Status;

                array_push($array_pessoa, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_pessoa;
    }

    // --------------------------------------------------------------------------------
    // listByNomeDataNasc
    // Lista os registros com base em Nome, DataNasc
    // --------------------------------------------------------------------------------
    public function listByNomeDataNasc(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $Nome = null, 
        string $DataNasc = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $Nome, $DataNasc, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // listByApelido
    // Lista os registros com base em Apelido
    // --------------------------------------------------------------------------------
    public function listByApelido(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $Apelido = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, null, null, null, null, null, null, null, null, null, null, null, $Apelido, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdPessoa = null, 
        string $Nome = null, 
        string $DataNasc = null, 
        string $Sexo = null, 
        string $Endereco = null, 
        string $Complemento = null, 
        string $Bairro = null, 
        string $Cidade = null, 
        string $UF = null, 
        string $Pais = null, 
        string $Telefone = null, 
        string $Email = null, 
        string $Apelido = null, 
        bool $PNE = null, 
        string $Observacao = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdPessoa) && is_null($Nome) && is_null($DataNasc)
             && is_null($Sexo) && is_null($Endereco) && is_null($Complemento)
             && is_null($Bairro) && is_null($Cidade) && is_null($UF)
             && is_null($Pais) && is_null($Telefone) && is_null($Email)
             && is_null($Apelido) && is_null($PNE) && is_null($Observacao)
             && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idpessoa as IdPessoa, 
                    nome as Nome, 
                    datanasc as DataNasc, 
                    sexo as Sexo, 
                    endereco as Endereco, 
                    complemento as Complemento, 
                    bairro as Bairro, 
                    cidade as Cidade, 
                    uf as UF, 
                    pais as Pais, 
                    telefone as Telefone, 
                    email as Email, 
                    apelido as Apelido, 
                    pne as PNE, 
                    observacao as Observacao, 
                    status as Status
                from
                    pessoa
                where 1 = 1";

        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome = " . $this->o_db->quote($Nome) . ")"; }
        if (isset($DataNasc)) { $sql = $sql . " and (datanasc = " . $this->o_db->quote(isset($DataNasc) ? DateTime::createFromFormat("d-m-Y", $DataNasc)->format("Y-m-d") : "") . ")"; }
        if (isset($Sexo)) { $sql = $sql . " and (sexo = " . $this->o_db->quote($Sexo) . ")"; }
        if (isset($Endereco)) { $sql = $sql . " and (endereco = " . $this->o_db->quote($Endereco) . ")"; }
        if (isset($Complemento)) { $sql = $sql . " and (complemento = " . $this->o_db->quote($Complemento) . ")"; }
        if (isset($Bairro)) { $sql = $sql . " and (bairro = " . $this->o_db->quote($Bairro) . ")"; }
        if (isset($Cidade)) { $sql = $sql . " and (cidade = " . $this->o_db->quote($Cidade) . ")"; }
        if (isset($UF)) { $sql = $sql . " and (uf = " . $this->o_db->quote($UF) . ")"; }
        if (isset($Pais)) { $sql = $sql . " and (pais = " . $this->o_db->quote($Pais) . ")"; }
        if (isset($Telefone)) { $sql = $sql . " and (telefone = " . $this->o_db->quote($Telefone) . ")"; }
        if (isset($Email)) { $sql = $sql . " and (email = " . $this->o_db->quote($Email) . ")"; }
        if (isset($Apelido)) { $sql = $sql . " and (apelido = " . $this->o_db->quote($Apelido) . ")"; }
        if (isset($PNE)) { $sql = $sql . " and (pne = " . $PNE . ")"; }
        if (isset($Observacao)) { $sql = $sql . " and (observacao = " . $this->o_db->quote($Observacao) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new PessoaModel();

                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->DataNasc = $obj_in->DataNasc;
                $obj_out->Sexo = $obj_in->Sexo;
                $obj_out->Endereco = $obj_in->Endereco;
                $obj_out->Complemento = $obj_in->Complemento;
                $obj_out->Bairro = $obj_in->Bairro;
                $obj_out->Cidade = $obj_in->Cidade;
                $obj_out->UF = $obj_in->UF;
                $obj_out->Pais = $obj_in->Pais;
                $obj_out->Telefone = $obj_in->Telefone;
                $obj_out->Email = $obj_in->Email;
                $obj_out->Apelido = $obj_in->Apelido;
                $obj_out->PNE = $obj_in->PNE;
                $obj_out->Observacao = $obj_in->Observacao;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdPessoa)
    {
        $obj = $this->objectByFields($IdPessoa, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        if ($obj) {
            $this->IdPessoa = $obj->IdPessoa;
            $this->Nome = $obj->Nome;
            $this->DataNasc = $obj->DataNasc;
            $this->Sexo = $obj->Sexo;
            $this->Endereco = $obj->Endereco;
            $this->Complemento = $obj->Complemento;
            $this->Bairro = $obj->Bairro;
            $this->Cidade = $obj->Cidade;
            $this->UF = $obj->UF;
            $this->Pais = $obj->Pais;
            $this->Telefone = $obj->Telefone;
            $this->Email = $obj->Email;
            $this->Apelido = $obj->Apelido;
            $this->PNE = $obj->PNE;
            $this->Observacao = $obj->Observacao;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdPessoa, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsNomeDataNasc
    // Verifica se existe um registro com Nome, DataNasc
    // --------------------------------------------------------------------------------
    public function existsNomeDataNasc()
    {
        $obj = $this->objectByFields(null, $this->Nome, $this->DataNasc, null, null, null, null, null, null, null, null, null, null, null, null, null);
        return !($obj && ($obj->IdPessoa === $this->IdPessoa));
    }

    // --------------------------------------------------------------------------------
    // existsApelido
    // Verifica se existe um registro com Apelido
    // --------------------------------------------------------------------------------
    public function existsApelido()
    {
        $obj = $this->objectByFields(null, null, null, null, null, null, null, null, null, null, null, null, $this->Apelido, null, null, null);
        return !($obj && ($obj->IdPessoa === $this->IdPessoa));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdPessoa = null, 
        string $Nome = null, 
        string $DataNasc = null, 
        string $Sexo = null, 
        string $Endereco = null, 
        string $Complemento = null, 
        string $Bairro = null, 
        string $Cidade = null, 
        string $UF = null, 
        string $Pais = null, 
        string $Telefone = null, 
        string $Email = null, 
        string $Apelido = null, 
        bool $PNE = null, 
        string $Observacao = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    pessoa
                where 1 = 1";

        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($DataNasc)) { $sql = $sql . " and (datanasc = " . $this->o_db->quote(DateTime::createFromFormat("d-m-Y", $DataNasc) ? DateTime::createFromFormat("d-m-Y", $DataNasc)->format("Y-m-d") : "") . ")"; }
        if (isset($Sexo)) { $sql = $sql . " and (sexo like " . $this->o_db->quote("%" . $Sexo. "%") . ")"; }
        if (isset($Endereco)) { $sql = $sql . " and (endereco like " . $this->o_db->quote("%" . $Endereco. "%") . ")"; }
        if (isset($Complemento)) { $sql = $sql . " and (complemento like " . $this->o_db->quote("%" . $Complemento. "%") . ")"; }
        if (isset($Bairro)) { $sql = $sql . " and (bairro like " . $this->o_db->quote("%" . $Bairro. "%") . ")"; }
        if (isset($Cidade)) { $sql = $sql . " and (cidade like " . $this->o_db->quote("%" . $Cidade. "%") . ")"; }
        if (isset($UF)) { $sql = $sql . " and (uf like " . $this->o_db->quote("%" . $UF. "%") . ")"; }
        if (isset($Pais)) { $sql = $sql . " and (pais like " . $this->o_db->quote("%" . $Pais. "%") . ")"; }
        if (isset($Telefone)) { $sql = $sql . " and (telefone like " . $this->o_db->quote("%" . $Telefone. "%") . ")"; }
        if (isset($Email)) { $sql = $sql . " and (email like " . $this->o_db->quote("%" . $Email. "%") . ")"; }
        if (isset($Apelido)) { $sql = $sql . " and (apelido like " . $this->o_db->quote("%" . $Apelido. "%") . ")"; }
        if (isset($PNE)) { $sql = $sql . " and (pne = " . PNE . ")"; }
        if (isset($Observacao)) { $sql = $sql . " and (observacao like " . $this->o_db->quote("%" . $Observacao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByNomeDataNasc
    // Conta os registros com base em Nome, DataNasc
    // --------------------------------------------------------------------------------
    public function countByNomeDataNasc(
        string $Nome = null, 
        string $DataNasc = null)
    {
        return $this->countBy(null, $Nome, $DataNasc, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // countByApelido
    // Conta os registros com base em Apelido
    // --------------------------------------------------------------------------------
    public function countByApelido(
        string $Apelido = null)
    {
        return $this->countBy(null, null, null, null, null, null, null, null, null, null, null, null, $Apelido, null, null, null);
    }

}

?>
