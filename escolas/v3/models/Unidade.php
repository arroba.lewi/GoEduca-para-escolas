<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// UnidadeModel
//
// Unidades das Instituições que contratam o serviço de auxílio a educação via jogos.
//
// Gerado em: 2018-03-26 05:04:15
// --------------------------------------------------------------------------------
class UnidadeModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdUnidade = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdInstituicao;      // char(32), PK, FK, obrigatório - Identificador da Instituição
    private $IdUnidade;          // char(32), PK, obrigatório - Identificador da Unidade
    private $Nome;               // varchar(256), obrigatório - Nome da Unidade
    private $RazaoSocial;        // varchar(256), obrigatório - Razão Social da Unidade
    private $Cnpj;               // varchar(32), obrigatório - CNPJ da Unidade
    private $InscEstadual;       // varchar(32), opcional - Inscrição Estadual da Unidade
    private $Endereco;           // varchar(256), opcional - Endereço
    private $Complemento;        // varchar(256), opcional - Complemento do Endereço
    private $Bairro;             // varchar(256), opcional - Bairro
    private $Cidade;             // varchar(256), opcional - Cidade
    private $UF;                 // char(2), opcional - UF
    private $Pais = 'Brasil';    // varchar(256), opcional - País
    private $Telefone;           // varchar(256), opcional - Número do telefone
    private $Email;              // varchar(256), opcional - Endereço e-mail
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "IdUnidade") { return $this->IdUnidade; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "RazaoSocial") { return $this->RazaoSocial; }
        if ($name === "Cnpj") { return $this->Cnpj; }
        if ($name === "InscEstadual") { return $this->InscEstadual; }
        if ($name === "Endereco") { return $this->Endereco; }
        if ($name === "Complemento") { return $this->Complemento; }
        if ($name === "Bairro") { return $this->Bairro; }
        if ($name === "Cidade") { return $this->Cidade; }
        if ($name === "UF") { return $this->UF; }
        if ($name === "Pais") { return $this->Pais; }
        if ($name === "Telefone") { return $this->Telefone; }
        if ($name === "Email") { return $this->Email; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdInstituicao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdInstituicao;
        }
        if ($name === "IdUnidade") {
            if (is_null($value)) {
                $this->IdUnidade = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdUnidade = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdUnidade;
        }
        if ($name === "Nome") {
            if (is_null($value)) {
                $this->Nome = null;
            }
            else {
                $this->Nome = substr((string) $value, 0, 256);
            }
            return $this->Nome;
        }
        if ($name === "RazaoSocial") {
            if (is_null($value)) {
                $this->RazaoSocial = null;
            }
            else {
                $this->RazaoSocial = substr((string) $value, 0, 256);
            }
            return $this->RazaoSocial;
        }
        if ($name === "Cnpj") {
            if (is_null($value)) {
                $this->Cnpj = null;
            }
            else {
                $this->Cnpj = substr((string) $value, 0, 32);
            }
            return $this->Cnpj;
        }
        if ($name === "InscEstadual") {
            if (is_null($value)) {
                $this->InscEstadual = null;
            }
            else {
                $this->InscEstadual = substr((string) $value, 0, 32);
            }
            return $this->InscEstadual;
        }
        if ($name === "Endereco") {
            if (is_null($value)) {
                $this->Endereco = null;
            }
            else {
                $this->Endereco = substr((string) $value, 0, 256);
            }
            return $this->Endereco;
        }
        if ($name === "Complemento") {
            if (is_null($value)) {
                $this->Complemento = null;
            }
            else {
                $this->Complemento = substr((string) $value, 0, 256);
            }
            return $this->Complemento;
        }
        if ($name === "Bairro") {
            if (is_null($value)) {
                $this->Bairro = null;
            }
            else {
                $this->Bairro = substr((string) $value, 0, 256);
            }
            return $this->Bairro;
        }
        if ($name === "Cidade") {
            if (is_null($value)) {
                $this->Cidade = null;
            }
            else {
                $this->Cidade = substr((string) $value, 0, 256);
            }
            return $this->Cidade;
        }
        if ($name === "UF") {
            if (is_null($value)) {
                $this->UF = null;
            }
            else {
                $this->UF = substr((string) $value, 0, 2);
            }
            return $this->UF;
        }
        if ($name === "Pais") {
            if (is_null($value)) {
                $this->Pais = null;
            }
            else {
                $this->Pais = substr((string) $value, 0, 256);
            }
            return $this->Pais;
        }
        if ($name === "Telefone") {
            if (is_null($value)) {
                $this->Telefone = null;
            }
            else {
                $this->Telefone = substr((string) $value, 0, 256);
            }
            return $this->Telefone;
        }
        if ($name === "Email") {
            if (is_null($value)) {
                $this->Email = null;
            }
            else {
                $this->Email = substr((string) $value, 0, 256);
            }
            return $this->Email;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        unidade
                    set 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        idunidade = " . ( isset($this->IdUnidade) ? $this->o_db->quote($IdUnidade) : "null" ) . ", 
                        nome = " . ( isset($this->Nome) ? $this->o_db->quote($Nome) : "null" ) . ", 
                        razaosocial = " . ( isset($this->RazaoSocial) ? $this->o_db->quote($RazaoSocial) : "null" ) . ", 
                        cnpj = " . ( isset($this->Cnpj) ? $this->o_db->quote($Cnpj) : "null" ) . ", 
                        inscestadual = " . ( isset($this->InscEstadual) ? $this->o_db->quote($InscEstadual) : "null" ) . ", 
                        endereco = " . ( isset($this->Endereco) ? $this->o_db->quote($Endereco) : "null" ) . ", 
                        complemento = " . ( isset($this->Complemento) ? $this->o_db->quote($Complemento) : "null" ) . ", 
                        bairro = " . ( isset($this->Bairro) ? $this->o_db->quote($Bairro) : "null" ) . ", 
                        cidade = " . ( isset($this->Cidade) ? $this->o_db->quote($Cidade) : "null" ) . ", 
                        uf = " . ( isset($this->UF) ? $this->o_db->quote($UF) : "null" ) . ", 
                        pais = " . ( isset($this->Pais) ? $this->o_db->quote($Pais) : "null" ) . ", 
                        telefone = " . ( isset($this->Telefone) ? $this->o_db->quote($Telefone) : "null" ) . ", 
                        email = " . ( isset($this->Email) ? $this->o_db->quote($Email) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and
                        idunidade" . ( isset($this->IdUnidade) ? " = " . $this->o_db->quote($this->IdUnidade) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        unidade (
                            idinstituicao, 
                            idunidade, 
                            nome, 
                            razaosocial, 
                            cnpj, 
                            inscestadual, 
                            endereco, 
                            complemento, 
                            bairro, 
                            cidade, 
                            uf, 
                            pais, 
                            telefone, 
                            email, 
                            status
                        )
                        values (
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->IdUnidade) ? $this->o_db->quote($this->IdUnidade) : "null" ) . ", 
                            " . ( isset($this->Nome) ? $this->o_db->quote($this->Nome) : "null" ) . ", 
                            " . ( isset($this->RazaoSocial) ? $this->o_db->quote($this->RazaoSocial) : "null" ) . ", 
                            " . ( isset($this->Cnpj) ? $this->o_db->quote($this->Cnpj) : "null" ) . ", 
                            " . ( isset($this->InscEstadual) ? $this->o_db->quote($this->InscEstadual) : "null" ) . ", 
                            " . ( isset($this->Endereco) ? $this->o_db->quote($this->Endereco) : "null" ) . ", 
                            " . ( isset($this->Complemento) ? $this->o_db->quote($this->Complemento) : "null" ) . ", 
                            " . ( isset($this->Bairro) ? $this->o_db->quote($this->Bairro) : "null" ) . ", 
                            " . ( isset($this->Cidade) ? $this->o_db->quote($this->Cidade) : "null" ) . ", 
                            " . ( isset($this->UF) ? $this->o_db->quote($this->UF) : "null" ) . ", 
                            " . ( isset($this->Pais) ? $this->o_db->quote($this->Pais) : "null" ) . ", 
                            " . ( isset($this->Telefone) ? $this->o_db->quote($this->Telefone) : "null" ) . ", 
                            " . ( isset($this->Email) ? $this->o_db->quote($this->Email) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdInstituicao) && isset($this->IdUnidade)) {
            $sql = "delete from 
                        unidade
                     where 
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and 
                        idunidade" . ( isset($this->IdUnidade) ? " = " . $this->o_db->quote($this->IdUnidade) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $Nome = null, 
        string $RazaoSocial = null, 
        string $Cnpj = null, 
        string $InscEstadual = null, 
        string $Endereco = null, 
        string $Complemento = null, 
        string $Bairro = null, 
        string $Cidade = null, 
        string $UF = null, 
        string $Pais = null, 
        string $Telefone = null, 
        string $Email = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idinstituicao as IdInstituicao, 
                    idunidade as IdUnidade, 
                    nome as Nome, 
                    razaosocial as RazaoSocial, 
                    cnpj as Cnpj, 
                    inscestadual as InscEstadual, 
                    endereco as Endereco, 
                    complemento as Complemento, 
                    bairro as Bairro, 
                    cidade as Cidade, 
                    uf as UF, 
                    pais as Pais, 
                    telefone as Telefone, 
                    email as Email, 
                    status as Status
                from
                    unidade
                where 1 = 1";

        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($RazaoSocial)) { $sql = $sql . " and (razaosocial like " . $this->o_db->quote("%" . $RazaoSocial. "%") . ")"; }
        if (isset($Cnpj)) { $sql = $sql . " and (cnpj like " . $this->o_db->quote("%" . $Cnpj. "%") . ")"; }
        if (isset($InscEstadual)) { $sql = $sql . " and (inscestadual like " . $this->o_db->quote("%" . $InscEstadual. "%") . ")"; }
        if (isset($Endereco)) { $sql = $sql . " and (endereco like " . $this->o_db->quote("%" . $Endereco. "%") . ")"; }
        if (isset($Complemento)) { $sql = $sql . " and (complemento like " . $this->o_db->quote("%" . $Complemento. "%") . ")"; }
        if (isset($Bairro)) { $sql = $sql . " and (bairro like " . $this->o_db->quote("%" . $Bairro. "%") . ")"; }
        if (isset($Cidade)) { $sql = $sql . " and (cidade like " . $this->o_db->quote("%" . $Cidade. "%") . ")"; }
        if (isset($UF)) { $sql = $sql . " and (uf like " . $this->o_db->quote("%" . $UF. "%") . ")"; }
        if (isset($Pais)) { $sql = $sql . " and (pais like " . $this->o_db->quote("%" . $Pais. "%") . ")"; }
        if (isset($Telefone)) { $sql = $sql . " and (telefone like " . $this->o_db->quote("%" . $Telefone. "%") . ")"; }
        if (isset($Email)) { $sql = $sql . " and (email like " . $this->o_db->quote("%" . $Email. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_unidade = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new UnidadeModel();

                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdUnidade = $obj_in->IdUnidade;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->RazaoSocial = $obj_in->RazaoSocial;
                $obj_out->Cnpj = $obj_in->Cnpj;
                $obj_out->InscEstadual = $obj_in->InscEstadual;
                $obj_out->Endereco = $obj_in->Endereco;
                $obj_out->Complemento = $obj_in->Complemento;
                $obj_out->Bairro = $obj_in->Bairro;
                $obj_out->Cidade = $obj_in->Cidade;
                $obj_out->UF = $obj_in->UF;
                $obj_out->Pais = $obj_in->Pais;
                $obj_out->Telefone = $obj_in->Telefone;
                $obj_out->Email = $obj_in->Email;
                $obj_out->Status = $obj_in->Status;

                array_push($array_unidade, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_unidade;
    }

    // --------------------------------------------------------------------------------
    // listByIdInstituicaoNome
    // Lista os registros com base em IdInstituicao, Nome
    // --------------------------------------------------------------------------------
    public function listByIdInstituicaoNome(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdInstituicao = null, 
        string $Nome = null)
    {
        return $this->listBy($pagenumber, $pagesize, $IdInstituicao, null, $Nome, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $Nome = null, 
        string $RazaoSocial = null, 
        string $Cnpj = null, 
        string $InscEstadual = null, 
        string $Endereco = null, 
        string $Complemento = null, 
        string $Bairro = null, 
        string $Cidade = null, 
        string $UF = null, 
        string $Pais = null, 
        string $Telefone = null, 
        string $Email = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdInstituicao) && is_null($IdUnidade) && is_null($Nome)
             && is_null($RazaoSocial) && is_null($Cnpj) && is_null($InscEstadual)
             && is_null($Endereco) && is_null($Complemento) && is_null($Bairro)
             && is_null($Cidade) && is_null($UF) && is_null($Pais)
             && is_null($Telefone) && is_null($Email) && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idinstituicao as IdInstituicao, 
                    idunidade as IdUnidade, 
                    nome as Nome, 
                    razaosocial as RazaoSocial, 
                    cnpj as Cnpj, 
                    inscestadual as InscEstadual, 
                    endereco as Endereco, 
                    complemento as Complemento, 
                    bairro as Bairro, 
                    cidade as Cidade, 
                    uf as UF, 
                    pais as Pais, 
                    telefone as Telefone, 
                    email as Email, 
                    status as Status
                from
                    unidade
                where 1 = 1";

        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome = " . $this->o_db->quote($Nome) . ")"; }
        if (isset($RazaoSocial)) { $sql = $sql . " and (razaosocial = " . $this->o_db->quote($RazaoSocial) . ")"; }
        if (isset($Cnpj)) { $sql = $sql . " and (cnpj = " . $this->o_db->quote($Cnpj) . ")"; }
        if (isset($InscEstadual)) { $sql = $sql . " and (inscestadual = " . $this->o_db->quote($InscEstadual) . ")"; }
        if (isset($Endereco)) { $sql = $sql . " and (endereco = " . $this->o_db->quote($Endereco) . ")"; }
        if (isset($Complemento)) { $sql = $sql . " and (complemento = " . $this->o_db->quote($Complemento) . ")"; }
        if (isset($Bairro)) { $sql = $sql . " and (bairro = " . $this->o_db->quote($Bairro) . ")"; }
        if (isset($Cidade)) { $sql = $sql . " and (cidade = " . $this->o_db->quote($Cidade) . ")"; }
        if (isset($UF)) { $sql = $sql . " and (uf = " . $this->o_db->quote($UF) . ")"; }
        if (isset($Pais)) { $sql = $sql . " and (pais = " . $this->o_db->quote($Pais) . ")"; }
        if (isset($Telefone)) { $sql = $sql . " and (telefone = " . $this->o_db->quote($Telefone) . ")"; }
        if (isset($Email)) { $sql = $sql . " and (email = " . $this->o_db->quote($Email) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new UnidadeModel();

                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdUnidade = $obj_in->IdUnidade;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->RazaoSocial = $obj_in->RazaoSocial;
                $obj_out->Cnpj = $obj_in->Cnpj;
                $obj_out->InscEstadual = $obj_in->InscEstadual;
                $obj_out->Endereco = $obj_in->Endereco;
                $obj_out->Complemento = $obj_in->Complemento;
                $obj_out->Bairro = $obj_in->Bairro;
                $obj_out->Cidade = $obj_in->Cidade;
                $obj_out->UF = $obj_in->UF;
                $obj_out->Pais = $obj_in->Pais;
                $obj_out->Telefone = $obj_in->Telefone;
                $obj_out->Email = $obj_in->Email;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdInstituicao, string $IdUnidade)
    {
        $obj = $this->objectByFields($IdInstituicao, $IdUnidade, null, null, null, null, null, null, null, null, null, null, null, null, null);
        if ($obj) {
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->IdUnidade = $obj->IdUnidade;
            $this->Nome = $obj->Nome;
            $this->RazaoSocial = $obj->RazaoSocial;
            $this->Cnpj = $obj->Cnpj;
            $this->InscEstadual = $obj->InscEstadual;
            $this->Endereco = $obj->Endereco;
            $this->Complemento = $obj->Complemento;
            $this->Bairro = $obj->Bairro;
            $this->Cidade = $obj->Cidade;
            $this->UF = $obj->UF;
            $this->Pais = $obj->Pais;
            $this->Telefone = $obj->Telefone;
            $this->Email = $obj->Email;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdInstituicao, $this->IdUnidade, null, null, null, null, null, null, null, null, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdInstituicaoNome
    // Verifica se existe um registro com IdInstituicao, Nome
    // --------------------------------------------------------------------------------
    public function existsIdInstituicaoNome()
    {
        $obj = $this->objectByFields($this->IdInstituicao, null, $this->Nome, null, null, null, null, null, null, null, null, null, null, null, null);
        return !($obj && ($obj->IdInstituicao === $this->IdInstituicao) && ($obj->IdUnidade === $this->IdUnidade));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $Nome = null, 
        string $RazaoSocial = null, 
        string $Cnpj = null, 
        string $InscEstadual = null, 
        string $Endereco = null, 
        string $Complemento = null, 
        string $Bairro = null, 
        string $Cidade = null, 
        string $UF = null, 
        string $Pais = null, 
        string $Telefone = null, 
        string $Email = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    unidade
                where 1 = 1";

        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($RazaoSocial)) { $sql = $sql . " and (razaosocial like " . $this->o_db->quote("%" . $RazaoSocial. "%") . ")"; }
        if (isset($Cnpj)) { $sql = $sql . " and (cnpj like " . $this->o_db->quote("%" . $Cnpj. "%") . ")"; }
        if (isset($InscEstadual)) { $sql = $sql . " and (inscestadual like " . $this->o_db->quote("%" . $InscEstadual. "%") . ")"; }
        if (isset($Endereco)) { $sql = $sql . " and (endereco like " . $this->o_db->quote("%" . $Endereco. "%") . ")"; }
        if (isset($Complemento)) { $sql = $sql . " and (complemento like " . $this->o_db->quote("%" . $Complemento. "%") . ")"; }
        if (isset($Bairro)) { $sql = $sql . " and (bairro like " . $this->o_db->quote("%" . $Bairro. "%") . ")"; }
        if (isset($Cidade)) { $sql = $sql . " and (cidade like " . $this->o_db->quote("%" . $Cidade. "%") . ")"; }
        if (isset($UF)) { $sql = $sql . " and (uf like " . $this->o_db->quote("%" . $UF. "%") . ")"; }
        if (isset($Pais)) { $sql = $sql . " and (pais like " . $this->o_db->quote("%" . $Pais. "%") . ")"; }
        if (isset($Telefone)) { $sql = $sql . " and (telefone like " . $this->o_db->quote("%" . $Telefone. "%") . ")"; }
        if (isset($Email)) { $sql = $sql . " and (email like " . $this->o_db->quote("%" . $Email. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdInstituicaoNome
    // Conta os registros com base em IdInstituicao, Nome
    // --------------------------------------------------------------------------------
    public function countByIdInstituicaoNome(
        string $IdInstituicao = null, 
        string $Nome = null)
    {
        return $this->countBy($IdInstituicao, null, $Nome, null, null, null, null, null, null, null, null, null, null, null, null);
    }

}

?>
