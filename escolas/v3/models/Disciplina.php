<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// DisciplinaModel
//
// Disciplinas disponibilizadas na instituição.
//
// Gerado em: 2018-03-26 05:03:15
// --------------------------------------------------------------------------------
class DisciplinaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdDisciplina = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdDisciplina;       // char(32), PK, obrigatório - Identificador da disciplina
    private $IdInstituicao;      // char(32), PK, FK, obrigatório - Identificador da Instituição
    private $Nome;               // varchar(256), obrigatório - Nome da disciplina
    private $Sigla;              // varchar(16), obrigatório - Sigla da disciplina
    private $Observacao;         // text, opcional - Observações sobre a pessoa
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdDisciplina") { return $this->IdDisciplina; }
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "Sigla") { return $this->Sigla; }
        if ($name === "Observacao") { return $this->Observacao; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdDisciplina") {
            if (is_null($value)) {
                $this->IdDisciplina = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdDisciplina = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdDisciplina;
        }
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdInstituicao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdInstituicao;
        }
        if ($name === "Nome") {
            if (is_null($value)) {
                $this->Nome = null;
            }
            else {
                $this->Nome = substr((string) $value, 0, 256);
            }
            return $this->Nome;
        }
        if ($name === "Sigla") {
            if (is_null($value)) {
                $this->Sigla = null;
            }
            else {
                $this->Sigla = substr((string) $value, 0, 16);
            }
            return $this->Sigla;
        }
        if ($name === "Observacao") {
            if (is_null($value)) {
                $this->Observacao = null;
            }
            else {
                $this->Observacao = substr((string) $value, 0, 65535);
            }
            return $this->Observacao;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        disciplina
                    set 
                        iddisciplina = " . ( isset($this->IdDisciplina) ? $this->o_db->quote($IdDisciplina) : "null" ) . ", 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        nome = " . ( isset($this->Nome) ? $this->o_db->quote($Nome) : "null" ) . ", 
                        sigla = " . ( isset($this->Sigla) ? $this->o_db->quote($Sigla) : "null" ) . ", 
                        observacao = " . ( isset($this->Observacao) ? $this->o_db->quote($Observacao) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        iddisciplina" . ( isset($this->IdDisciplina) ? " = " . $this->o_db->quote($this->IdDisciplina) : " is null" ) . "
                        and
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        disciplina (
                            iddisciplina, 
                            idinstituicao, 
                            nome, 
                            sigla, 
                            observacao, 
                            status
                        )
                        values (
                            " . ( isset($this->IdDisciplina) ? $this->o_db->quote($this->IdDisciplina) : "null" ) . ", 
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->Nome) ? $this->o_db->quote($this->Nome) : "null" ) . ", 
                            " . ( isset($this->Sigla) ? $this->o_db->quote($this->Sigla) : "null" ) . ", 
                            " . ( isset($this->Observacao) ? $this->o_db->quote($this->Observacao) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdDisciplina) && isset($this->IdInstituicao)) {
            $sql = "delete from 
                        disciplina
                     where 
                        iddisciplina" . ( isset($this->IdDisciplina) ? " = " . $this->o_db->quote($this->IdDisciplina) : " is null" ) . "
                        and 
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdDisciplina = null, 
        string $IdInstituicao = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Observacao = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    iddisciplina as IdDisciplina, 
                    idinstituicao as IdInstituicao, 
                    nome as Nome, 
                    sigla as Sigla, 
                    observacao as Observacao, 
                    status as Status
                from
                    disciplina
                where 1 = 1";

        if (isset($IdDisciplina)) { $sql = $sql . " and (iddisciplina = " . $this->o_db->quote($IdDisciplina) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Observacao)) { $sql = $sql . " and (observacao like " . $this->o_db->quote("%" . $Observacao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_disciplina = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new DisciplinaModel();

                $obj_out->IdDisciplina = $obj_in->IdDisciplina;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Observacao = $obj_in->Observacao;
                $obj_out->Status = $obj_in->Status;

                array_push($array_disciplina, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_disciplina;
    }

    // --------------------------------------------------------------------------------
    // listByIdInstituicaoNome
    // Lista os registros com base em IdInstituicao, Nome
    // --------------------------------------------------------------------------------
    public function listByIdInstituicaoNome(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdInstituicao = null, 
        string $Nome = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdInstituicao, $Nome, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // listByIdInstituicaoSigla
    // Lista os registros com base em IdInstituicao, Sigla
    // --------------------------------------------------------------------------------
    public function listByIdInstituicaoSigla(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdInstituicao = null, 
        string $Sigla = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdInstituicao, null, $Sigla, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdDisciplina = null, 
        string $IdInstituicao = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Observacao = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdDisciplina) && is_null($IdInstituicao) && is_null($Nome)
             && is_null($Sigla) && is_null($Observacao) && is_null($Status)) {
            return null;
        }

        $sql = "select
                    iddisciplina as IdDisciplina, 
                    idinstituicao as IdInstituicao, 
                    nome as Nome, 
                    sigla as Sigla, 
                    observacao as Observacao, 
                    status as Status
                from
                    disciplina
                where 1 = 1";

        if (isset($IdDisciplina)) { $sql = $sql . " and (iddisciplina = " . $this->o_db->quote($IdDisciplina) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome = " . $this->o_db->quote($Nome) . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla = " . $this->o_db->quote($Sigla) . ")"; }
        if (isset($Observacao)) { $sql = $sql . " and (observacao = " . $this->o_db->quote($Observacao) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new DisciplinaModel();

                $obj_out->IdDisciplina = $obj_in->IdDisciplina;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Observacao = $obj_in->Observacao;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdDisciplina, string $IdInstituicao)
    {
        $obj = $this->objectByFields($IdDisciplina, $IdInstituicao, null, null, null, null);
        if ($obj) {
            $this->IdDisciplina = $obj->IdDisciplina;
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->Nome = $obj->Nome;
            $this->Sigla = $obj->Sigla;
            $this->Observacao = $obj->Observacao;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdDisciplina, $this->IdInstituicao, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdInstituicaoNome
    // Verifica se existe um registro com IdInstituicao, Nome
    // --------------------------------------------------------------------------------
    public function existsIdInstituicaoNome()
    {
        $obj = $this->objectByFields(null, $this->IdInstituicao, $this->Nome, null, null, null);
        return !($obj && ($obj->IdDisciplina === $this->IdDisciplina) && ($obj->IdInstituicao === $this->IdInstituicao));
    }

    // --------------------------------------------------------------------------------
    // existsIdInstituicaoSigla
    // Verifica se existe um registro com IdInstituicao, Sigla
    // --------------------------------------------------------------------------------
    public function existsIdInstituicaoSigla()
    {
        $obj = $this->objectByFields(null, $this->IdInstituicao, null, $this->Sigla, null, null);
        return !($obj && ($obj->IdDisciplina === $this->IdDisciplina) && ($obj->IdInstituicao === $this->IdInstituicao));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdDisciplina = null, 
        string $IdInstituicao = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Observacao = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    disciplina
                where 1 = 1";

        if (isset($IdDisciplina)) { $sql = $sql . " and (iddisciplina = " . $this->o_db->quote($IdDisciplina) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Observacao)) { $sql = $sql . " and (observacao like " . $this->o_db->quote("%" . $Observacao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdInstituicaoNome
    // Conta os registros com base em IdInstituicao, Nome
    // --------------------------------------------------------------------------------
    public function countByIdInstituicaoNome(
        string $IdInstituicao = null, 
        string $Nome = null)
    {
        return $this->countBy(null, $IdInstituicao, $Nome, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // countByIdInstituicaoSigla
    // Conta os registros com base em IdInstituicao, Sigla
    // --------------------------------------------------------------------------------
    public function countByIdInstituicaoSigla(
        string $IdInstituicao = null, 
        string $Sigla = null)
    {
        return $this->countBy(null, $IdInstituicao, null, $Sigla, null, null);
    }

}

?>
