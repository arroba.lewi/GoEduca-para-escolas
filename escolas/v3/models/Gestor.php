<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// GestorModel
//
// Pessoas vinculadas à área administrativa da instituições e/ou unidades.
//
// Gerado em: 2018-03-26 05:03:21
// --------------------------------------------------------------------------------
class GestorModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdGestor;           // char(32), PK, FK, obrigatório - Identificador do Gestor, assoaciado a uma pessoa
    private $IdInstituicao;      // char(32), PK, FK, obrigatório - Identificador da Instituição
    private $IdUnidade;          // char(32), PK, FK, obrigatório - Identificador da Unidade
    private $Cargo;              // varchar(256), opcional - Cargo
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdGestor") { return $this->IdGestor; }
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "IdUnidade") { return $this->IdUnidade; }
        if ($name === "Cargo") { return $this->Cargo; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdGestor") {
            if (is_null($value)) {
                $this->IdGestor = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGestor = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGestor;
        }
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdInstituicao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdInstituicao;
        }
        if ($name === "IdUnidade") {
            if (is_null($value)) {
                $this->IdUnidade = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdUnidade = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdUnidade;
        }
        if ($name === "Cargo") {
            if (is_null($value)) {
                $this->Cargo = null;
            }
            else {
                $this->Cargo = substr((string) $value, 0, 256);
            }
            return $this->Cargo;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        gestor
                    set 
                        idgestor = " . ( isset($this->IdGestor) ? $this->o_db->quote($IdGestor) : "null" ) . ", 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        idunidade = " . ( isset($this->IdUnidade) ? $this->o_db->quote($IdUnidade) : "null" ) . ", 
                        cargo = " . ( isset($this->Cargo) ? $this->o_db->quote($Cargo) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idgestor" . ( isset($this->IdGestor) ? " = " . $this->o_db->quote($this->IdGestor) : " is null" ) . "
                        and
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and
                        idunidade" . ( isset($this->IdUnidade) ? " = " . $this->o_db->quote($this->IdUnidade) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        gestor (
                            idgestor, 
                            idinstituicao, 
                            idunidade, 
                            cargo, 
                            status
                        )
                        values (
                            " . ( isset($this->IdGestor) ? $this->o_db->quote($this->IdGestor) : "null" ) . ", 
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->IdUnidade) ? $this->o_db->quote($this->IdUnidade) : "null" ) . ", 
                            " . ( isset($this->Cargo) ? $this->o_db->quote($this->Cargo) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdGestor) && isset($this->IdInstituicao) && isset($this->IdUnidade)) {
            $sql = "delete from 
                        gestor
                     where 
                        idgestor" . ( isset($this->IdGestor) ? " = " . $this->o_db->quote($this->IdGestor) : " is null" ) . "
                        and 
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and 
                        idunidade" . ( isset($this->IdUnidade) ? " = " . $this->o_db->quote($this->IdUnidade) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGestor = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $Cargo = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idgestor as IdGestor, 
                    idinstituicao as IdInstituicao, 
                    idunidade as IdUnidade, 
                    cargo as Cargo, 
                    status as Status
                from
                    gestor
                where 1 = 1";

        if (isset($IdGestor)) { $sql = $sql . " and (idgestor = " . $this->o_db->quote($IdGestor) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($Cargo)) { $sql = $sql . " and (cargo like " . $this->o_db->quote("%" . $Cargo. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_gestor = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GestorModel();

                $obj_out->IdGestor = $obj_in->IdGestor;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdUnidade = $obj_in->IdUnidade;
                $obj_out->Cargo = $obj_in->Cargo;
                $obj_out->Status = $obj_in->Status;

                array_push($array_gestor, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_gestor;
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdGestor = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $Cargo = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdGestor) && is_null($IdInstituicao) && is_null($IdUnidade)
             && is_null($Cargo) && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idgestor as IdGestor, 
                    idinstituicao as IdInstituicao, 
                    idunidade as IdUnidade, 
                    cargo as Cargo, 
                    status as Status
                from
                    gestor
                where 1 = 1";

        if (isset($IdGestor)) { $sql = $sql . " and (idgestor = " . $this->o_db->quote($IdGestor) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($Cargo)) { $sql = $sql . " and (cargo = " . $this->o_db->quote($Cargo) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GestorModel();

                $obj_out->IdGestor = $obj_in->IdGestor;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdUnidade = $obj_in->IdUnidade;
                $obj_out->Cargo = $obj_in->Cargo;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdGestor, string $IdInstituicao, string $IdUnidade)
    {
        $obj = $this->objectByFields($IdGestor, $IdInstituicao, $IdUnidade, null, null);
        if ($obj) {
            $this->IdGestor = $obj->IdGestor;
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->IdUnidade = $obj->IdUnidade;
            $this->Cargo = $obj->Cargo;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdGestor, $this->IdInstituicao, $this->IdUnidade, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdGestor = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $Cargo = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    gestor
                where 1 = 1";

        if (isset($IdGestor)) { $sql = $sql . " and (idgestor = " . $this->o_db->quote($IdGestor) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($Cargo)) { $sql = $sql . " and (cargo like " . $this->o_db->quote("%" . $Cargo. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
}

?>
