<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// TurnoModel
//
// Turnos de aulas disponíveis nas instituições.
//
// Gerado em: 2018-03-26 05:04:12
// --------------------------------------------------------------------------------
class TurnoModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdTurno = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdInstituicao;      // char(32), PK, FK, obrigatório - Identificador da Instituição
    private $IdTurno;            // char(32), PK, obrigatório - Identificador do Turno
    private $Nome;               // varchar(256), obrigatório - Nome do Turno
    private $Sigla;              // varchar(32), obrigatório - Sigla do Turno
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "IdTurno") { return $this->IdTurno; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "Sigla") { return $this->Sigla; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdInstituicao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdInstituicao;
        }
        if ($name === "IdTurno") {
            if (is_null($value)) {
                $this->IdTurno = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdTurno = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdTurno;
        }
        if ($name === "Nome") {
            if (is_null($value)) {
                $this->Nome = null;
            }
            else {
                $this->Nome = substr((string) $value, 0, 256);
            }
            return $this->Nome;
        }
        if ($name === "Sigla") {
            if (is_null($value)) {
                $this->Sigla = null;
            }
            else {
                $this->Sigla = substr((string) $value, 0, 32);
            }
            return $this->Sigla;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        turno
                    set 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        idturno = " . ( isset($this->IdTurno) ? $this->o_db->quote($IdTurno) : "null" ) . ", 
                        nome = " . ( isset($this->Nome) ? $this->o_db->quote($Nome) : "null" ) . ", 
                        sigla = " . ( isset($this->Sigla) ? $this->o_db->quote($Sigla) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and
                        idturno" . ( isset($this->IdTurno) ? " = " . $this->o_db->quote($this->IdTurno) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        turno (
                            idinstituicao, 
                            idturno, 
                            nome, 
                            sigla, 
                            status
                        )
                        values (
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->IdTurno) ? $this->o_db->quote($this->IdTurno) : "null" ) . ", 
                            " . ( isset($this->Nome) ? $this->o_db->quote($this->Nome) : "null" ) . ", 
                            " . ( isset($this->Sigla) ? $this->o_db->quote($this->Sigla) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdInstituicao) && isset($this->IdTurno)) {
            $sql = "delete from 
                        turno
                     where 
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and 
                        idturno" . ( isset($this->IdTurno) ? " = " . $this->o_db->quote($this->IdTurno) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdInstituicao = null, 
        string $IdTurno = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idinstituicao as IdInstituicao, 
                    idturno as IdTurno, 
                    nome as Nome, 
                    sigla as Sigla, 
                    status as Status
                from
                    turno
                where 1 = 1";

        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdTurno)) { $sql = $sql . " and (idturno = " . $this->o_db->quote($IdTurno) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_turno = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new TurnoModel();

                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdTurno = $obj_in->IdTurno;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Status = $obj_in->Status;

                array_push($array_turno, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_turno;
    }

    // --------------------------------------------------------------------------------
    // listByIdInstituicaoNome
    // Lista os registros com base em IdInstituicao, Nome
    // --------------------------------------------------------------------------------
    public function listByIdInstituicaoNome(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdInstituicao = null, 
        string $Nome = null)
    {
        return $this->listBy($pagenumber, $pagesize, $IdInstituicao, null, $Nome, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdInstituicao = null, 
        string $IdTurno = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdInstituicao) && is_null($IdTurno) && is_null($Nome)
             && is_null($Sigla) && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idinstituicao as IdInstituicao, 
                    idturno as IdTurno, 
                    nome as Nome, 
                    sigla as Sigla, 
                    status as Status
                from
                    turno
                where 1 = 1";

        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdTurno)) { $sql = $sql . " and (idturno = " . $this->o_db->quote($IdTurno) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome = " . $this->o_db->quote($Nome) . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla = " . $this->o_db->quote($Sigla) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new TurnoModel();

                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdTurno = $obj_in->IdTurno;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdInstituicao, string $IdTurno)
    {
        $obj = $this->objectByFields($IdInstituicao, $IdTurno, null, null, null);
        if ($obj) {
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->IdTurno = $obj->IdTurno;
            $this->Nome = $obj->Nome;
            $this->Sigla = $obj->Sigla;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdInstituicao, $this->IdTurno, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdInstituicaoNome
    // Verifica se existe um registro com IdInstituicao, Nome
    // --------------------------------------------------------------------------------
    public function existsIdInstituicaoNome()
    {
        $obj = $this->objectByFields($this->IdInstituicao, null, $this->Nome, null, null);
        return !($obj && ($obj->IdInstituicao === $this->IdInstituicao) && ($obj->IdTurno === $this->IdTurno));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdInstituicao = null, 
        string $IdTurno = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    turno
                where 1 = 1";

        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdTurno)) { $sql = $sql . " and (idturno = " . $this->o_db->quote($IdTurno) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdInstituicaoNome
    // Conta os registros com base em IdInstituicao, Nome
    // --------------------------------------------------------------------------------
    public function countByIdInstituicaoNome(
        string $IdInstituicao = null, 
        string $Nome = null)
    {
        return $this->countBy($IdInstituicao, null, $Nome, null, null);
    }

}

?>
