<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// JogoJogoGeneroModel
//
// Estabelece os gêneros aos quais os jogos pertencem.
//
// Gerado em: 2018-03-26 05:03:53
// --------------------------------------------------------------------------------
class JogoJogoGeneroModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdJogo;             // char(32), PK, FK, obrigatório - Identificação do Jogo
    private $IdJogoGenero;       // char(32), PK, FK, obrigatório - Identificação do Gênero do Jogo

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdJogo") { return $this->IdJogo; }
        if ($name === "IdJogoGenero") { return $this->IdJogoGenero; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdJogo") {
            if (is_null($value)) {
                $this->IdJogo = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdJogo = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdJogo;
        }
        if ($name === "IdJogoGenero") {
            if (is_null($value)) {
                $this->IdJogoGenero = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdJogoGenero = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdJogoGenero;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        jogo_jogogenero
                    set 
                        idjogo = " . ( isset($this->IdJogo) ? $this->o_db->quote($IdJogo) : "null" ) . ", 
                        idjogogenero = " . ( isset($this->IdJogoGenero) ? $this->o_db->quote($IdJogoGenero) : "null" ) . "
                    where 
                        idjogo" . ( isset($this->IdJogo) ? " = " . $this->o_db->quote($this->IdJogo) : " is null" ) . "
                        and
                        idjogogenero" . ( isset($this->IdJogoGenero) ? " = " . $this->o_db->quote($this->IdJogoGenero) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        jogo_jogogenero (
                            idjogo, 
                            idjogogenero
                        )
                        values (
                            " . ( isset($this->IdJogo) ? $this->o_db->quote($this->IdJogo) : "null" ) . ", 
                            " . ( isset($this->IdJogoGenero) ? $this->o_db->quote($this->IdJogoGenero) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdJogo) && isset($this->IdJogoGenero)) {
            $sql = "delete from 
                        jogo_jogogenero
                     where 
                        idjogo" . ( isset($this->IdJogo) ? " = " . $this->o_db->quote($this->IdJogo) : " is null" ) . "
                        and 
                        idjogogenero" . ( isset($this->IdJogoGenero) ? " = " . $this->o_db->quote($this->IdJogoGenero) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdJogo = null, 
        string $IdJogoGenero = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idjogo as IdJogo, 
                    idjogogenero as IdJogoGenero
                from
                    jogo_jogogenero
                where 1 = 1";

        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdJogoGenero)) { $sql = $sql . " and (idjogogenero = " . $this->o_db->quote($IdJogoGenero) . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_jogo_jogogenero = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new JogoJogoGeneroModel();

                $obj_out->IdJogo = $obj_in->IdJogo;
                $obj_out->IdJogoGenero = $obj_in->IdJogoGenero;

                array_push($array_jogo_jogogenero, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_jogo_jogogenero;
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdJogo = null, 
        string $IdJogoGenero = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdJogo) && is_null($IdJogoGenero)) {
            return null;
        }

        $sql = "select
                    idjogo as IdJogo, 
                    idjogogenero as IdJogoGenero
                from
                    jogo_jogogenero
                where 1 = 1";

        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdJogoGenero)) { $sql = $sql . " and (idjogogenero = " . $this->o_db->quote($IdJogoGenero) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new JogoJogoGeneroModel();

                $obj_out->IdJogo = $obj_in->IdJogo;
                $obj_out->IdJogoGenero = $obj_in->IdJogoGenero;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdJogo, string $IdJogoGenero)
    {
        $obj = $this->objectByFields($IdJogo, $IdJogoGenero);
        if ($obj) {
            $this->IdJogo = $obj->IdJogo;
            $this->IdJogoGenero = $obj->IdJogoGenero;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdJogo, $this->IdJogoGenero);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdJogo = null, 
        string $IdJogoGenero = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    jogo_jogogenero
                where 1 = 1";

        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdJogoGenero)) { $sql = $sql . " and (idjogogenero = " . $this->o_db->quote($IdJogoGenero) . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
}

?>
