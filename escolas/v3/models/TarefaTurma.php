<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// TarefaTurmaModel
//
// Tarefas que os professores definem para as turmas.
//
// Gerado em: 2018-03-26 05:04:05
// --------------------------------------------------------------------------------
class TarefaTurmaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdTarefaTurma = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdTarefaTurma;      // char(32), PK, obrigatório - Identificador da Tarefa da Turma
    private $IdInstituicao;      // char(32), FK, obrigatório - Identificador da Instituição
    private $IdUnidade;          // char(32), FK, obrigatório - Identificador da Unidade
    private $IdCurso;            // char(32), FK, obrigatório - Identificador do Curso
    private $IdTurma;            // char(32), FK, obrigatório - Identificador da Turma
    private $IdProfessor;        // char(32), FK, obrigatório - Identificador do Professor
    private $IdDisciplina;       // char(32), FK, obrigatório - Identificador da disciplina
    private $IdJogo;             // char(32), FK, obrigatório - Identificação do Jogo
    private $Localizador;        // varchar(64), opcional - Localizador da tarefa definido pelo professor
    private $Titulo;             // varchar(256), obrigatório - Título da Tarefa
    private $Descricao;          // varchar(256), opcional - Título da Tarefa
    private $DataInicial;        // varchar(256), obrigatório - Data Inicial da Tarefa da Turma
    private $DataFinal;          // varchar(256), obrigatório - Data Final da Tarefa da Turma
    private $MetaPontuacao;      // int(11), opcional - Meta de pontuação a ser alcançada
    private $MetaQtdPartidas;    // int(11), opcional - Meta de quantidade de partidas a serem jogadas
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdTarefaTurma") { return $this->IdTarefaTurma; }
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "IdUnidade") { return $this->IdUnidade; }
        if ($name === "IdCurso") { return $this->IdCurso; }
        if ($name === "IdTurma") { return $this->IdTurma; }
        if ($name === "IdProfessor") { return $this->IdProfessor; }
        if ($name === "IdDisciplina") { return $this->IdDisciplina; }
        if ($name === "IdJogo") { return $this->IdJogo; }
        if ($name === "Localizador") { return $this->Localizador; }
        if ($name === "Titulo") { return $this->Titulo; }
        if ($name === "Descricao") { return $this->Descricao; }
        if ($name === "DataInicial") { return $this->DataInicial; }
        if ($name === "DataFinal") { return $this->DataFinal; }
        if ($name === "MetaPontuacao") { return $this->MetaPontuacao; }
        if ($name === "MetaQtdPartidas") { return $this->MetaQtdPartidas; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdTarefaTurma") {
            if (is_null($value)) {
                $this->IdTarefaTurma = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdTarefaTurma = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdTarefaTurma;
        }
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdInstituicao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdInstituicao;
        }
        if ($name === "IdUnidade") {
            if (is_null($value)) {
                $this->IdUnidade = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdUnidade = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdUnidade;
        }
        if ($name === "IdCurso") {
            if (is_null($value)) {
                $this->IdCurso = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdCurso = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdCurso;
        }
        if ($name === "IdTurma") {
            if (is_null($value)) {
                $this->IdTurma = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdTurma = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdTurma;
        }
        if ($name === "IdProfessor") {
            if (is_null($value)) {
                $this->IdProfessor = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdProfessor = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdProfessor;
        }
        if ($name === "IdDisciplina") {
            if (is_null($value)) {
                $this->IdDisciplina = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdDisciplina = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdDisciplina;
        }
        if ($name === "IdJogo") {
            if (is_null($value)) {
                $this->IdJogo = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdJogo = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdJogo;
        }
        if ($name === "Localizador") {
            if (is_null($value)) {
                $this->Localizador = null;
            }
            else {
                $this->Localizador = substr((string) $value, 0, 64);
            }
            return $this->Localizador;
        }
        if ($name === "Titulo") {
            if (is_null($value)) {
                $this->Titulo = null;
            }
            else {
                $this->Titulo = substr((string) $value, 0, 256);
            }
            return $this->Titulo;
        }
        if ($name === "Descricao") {
            if (is_null($value)) {
                $this->Descricao = null;
            }
            else {
                $this->Descricao = substr((string) $value, 0, 256);
            }
            return $this->Descricao;
        }
        if ($name === "DataInicial") {
            if (is_null($value)) {
                $this->DataInicial = null;
            }
            else {
                $this->DataInicial = substr((string) $value, 0, 256);
            }
            return $this->DataInicial;
        }
        if ($name === "DataFinal") {
            if (is_null($value)) {
                $this->DataFinal = null;
            }
            else {
                $this->DataFinal = substr((string) $value, 0, 256);
            }
            return $this->DataFinal;
        }
        if ($name === "MetaPontuacao") {
            if (is_null($value)) {
                $this->MetaPontuacao = null;
            }
            else {
                if (is_int($value)) {
                    $this->MetaPontuacao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->MetaPontuacao;
        }
        if ($name === "MetaQtdPartidas") {
            if (is_null($value)) {
                $this->MetaQtdPartidas = null;
            }
            else {
                if (is_int($value)) {
                    $this->MetaQtdPartidas = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->MetaQtdPartidas;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        tarefaturma
                    set 
                        idtarefaturma = " . ( isset($this->IdTarefaTurma) ? $this->o_db->quote($IdTarefaTurma) : "null" ) . ", 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        idunidade = " . ( isset($this->IdUnidade) ? $this->o_db->quote($IdUnidade) : "null" ) . ", 
                        idcurso = " . ( isset($this->IdCurso) ? $this->o_db->quote($IdCurso) : "null" ) . ", 
                        idturma = " . ( isset($this->IdTurma) ? $this->o_db->quote($IdTurma) : "null" ) . ", 
                        idprofessor = " . ( isset($this->IdProfessor) ? $this->o_db->quote($IdProfessor) : "null" ) . ", 
                        iddisciplina = " . ( isset($this->IdDisciplina) ? $this->o_db->quote($IdDisciplina) : "null" ) . ", 
                        idjogo = " . ( isset($this->IdJogo) ? $this->o_db->quote($IdJogo) : "null" ) . ", 
                        localizador = " . ( isset($this->Localizador) ? $this->o_db->quote($Localizador) : "null" ) . ", 
                        titulo = " . ( isset($this->Titulo) ? $this->o_db->quote($Titulo) : "null" ) . ", 
                        descricao = " . ( isset($this->Descricao) ? $this->o_db->quote($Descricao) : "null" ) . ", 
                        datainicial = " . ( isset($this->DataInicial) ? $this->o_db->quote($DataInicial) : "null" ) . ", 
                        datafinal = " . ( isset($this->DataFinal) ? $this->o_db->quote($DataFinal) : "null" ) . ", 
                        metapontuacao = " . ( isset($this->MetaPontuacao) ? $this->MetaPontuacao : "null" ) . ", 
                        metaqtdpartidas = " . ( isset($this->MetaQtdPartidas) ? $this->MetaQtdPartidas : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idtarefaturma" . ( isset($this->IdTarefaTurma) ? " = " . $this->o_db->quote($this->IdTarefaTurma) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        tarefaturma (
                            idtarefaturma, 
                            idinstituicao, 
                            idunidade, 
                            idcurso, 
                            idturma, 
                            idprofessor, 
                            iddisciplina, 
                            idjogo, 
                            localizador, 
                            titulo, 
                            descricao, 
                            datainicial, 
                            datafinal, 
                            metapontuacao, 
                            metaqtdpartidas, 
                            status
                        )
                        values (
                            " . ( isset($this->IdTarefaTurma) ? $this->o_db->quote($this->IdTarefaTurma) : "null" ) . ", 
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->IdUnidade) ? $this->o_db->quote($this->IdUnidade) : "null" ) . ", 
                            " . ( isset($this->IdCurso) ? $this->o_db->quote($this->IdCurso) : "null" ) . ", 
                            " . ( isset($this->IdTurma) ? $this->o_db->quote($this->IdTurma) : "null" ) . ", 
                            " . ( isset($this->IdProfessor) ? $this->o_db->quote($this->IdProfessor) : "null" ) . ", 
                            " . ( isset($this->IdDisciplina) ? $this->o_db->quote($this->IdDisciplina) : "null" ) . ", 
                            " . ( isset($this->IdJogo) ? $this->o_db->quote($this->IdJogo) : "null" ) . ", 
                            " . ( isset($this->Localizador) ? $this->o_db->quote($this->Localizador) : "null" ) . ", 
                            " . ( isset($this->Titulo) ? $this->o_db->quote($this->Titulo) : "null" ) . ", 
                            " . ( isset($this->Descricao) ? $this->o_db->quote($this->Descricao) : "null" ) . ", 
                            " . ( isset($this->DataInicial) ? $this->o_db->quote($this->DataInicial) : "null" ) . ", 
                            " . ( isset($this->DataFinal) ? $this->o_db->quote($this->DataFinal) : "null" ) . ", 
                            " . ( isset($this->MetaPontuacao) ? $this->MetaPontuacao : "null" ) . ", 
                            " . ( isset($this->MetaQtdPartidas) ? $this->MetaQtdPartidas : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdTarefaTurma)) {
            $sql = "delete from 
                        tarefaturma
                     where 
                        idtarefaturma" . ( isset($this->IdTarefaTurma) ? " = " . $this->o_db->quote($this->IdTarefaTurma) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdTarefaTurma = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $IdCurso = null, 
        string $IdTurma = null, 
        string $IdProfessor = null, 
        string $IdDisciplina = null, 
        string $IdJogo = null, 
        string $Localizador = null, 
        string $Titulo = null, 
        string $Descricao = null, 
        string $DataInicial = null, 
        string $DataFinal = null, 
        int $MetaPontuacao = null, 
        int $MetaQtdPartidas = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idtarefaturma as IdTarefaTurma, 
                    idinstituicao as IdInstituicao, 
                    idunidade as IdUnidade, 
                    idcurso as IdCurso, 
                    idturma as IdTurma, 
                    idprofessor as IdProfessor, 
                    iddisciplina as IdDisciplina, 
                    idjogo as IdJogo, 
                    localizador as Localizador, 
                    titulo as Titulo, 
                    descricao as Descricao, 
                    datainicial as DataInicial, 
                    datafinal as DataFinal, 
                    metapontuacao as MetaPontuacao, 
                    metaqtdpartidas as MetaQtdPartidas, 
                    status as Status
                from
                    tarefaturma
                where 1 = 1";

        if (isset($IdTarefaTurma)) { $sql = $sql . " and (idtarefaturma = " . $this->o_db->quote($IdTarefaTurma) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($IdTurma)) { $sql = $sql . " and (idturma = " . $this->o_db->quote($IdTurma) . ")"; }
        if (isset($IdProfessor)) { $sql = $sql . " and (idprofessor = " . $this->o_db->quote($IdProfessor) . ")"; }
        if (isset($IdDisciplina)) { $sql = $sql . " and (iddisciplina = " . $this->o_db->quote($IdDisciplina) . ")"; }
        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($Localizador)) { $sql = $sql . " and (localizador like " . $this->o_db->quote("%" . $Localizador. "%") . ")"; }
        if (isset($Titulo)) { $sql = $sql . " and (titulo like " . $this->o_db->quote("%" . $Titulo. "%") . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao like " . $this->o_db->quote("%" . $Descricao. "%") . ")"; }
        if (isset($DataInicial)) { $sql = $sql . " and (datainicial like " . $this->o_db->quote("%" . $DataInicial. "%") . ")"; }
        if (isset($DataFinal)) { $sql = $sql . " and (datafinal like " . $this->o_db->quote("%" . $DataFinal. "%") . ")"; }
        if (isset($MetaPontuacao)) { $sql = $sql . " and (metapontuacao = " . $MetaPontuacao . ")"; }
        if (isset($MetaQtdPartidas)) { $sql = $sql . " and (metaqtdpartidas = " . $MetaQtdPartidas . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_tarefaturma = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new TarefaTurmaModel();

                $obj_out->IdTarefaTurma = $obj_in->IdTarefaTurma;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdUnidade = $obj_in->IdUnidade;
                $obj_out->IdCurso = $obj_in->IdCurso;
                $obj_out->IdTurma = $obj_in->IdTurma;
                $obj_out->IdProfessor = $obj_in->IdProfessor;
                $obj_out->IdDisciplina = $obj_in->IdDisciplina;
                $obj_out->IdJogo = $obj_in->IdJogo;
                $obj_out->Localizador = $obj_in->Localizador;
                $obj_out->Titulo = $obj_in->Titulo;
                $obj_out->Descricao = $obj_in->Descricao;
                $obj_out->DataInicial = $obj_in->DataInicial;
                $obj_out->DataFinal = $obj_in->DataFinal;
                $obj_out->MetaPontuacao = $obj_in->MetaPontuacao;
                $obj_out->MetaQtdPartidas = $obj_in->MetaQtdPartidas;
                $obj_out->Status = $obj_in->Status;

                array_push($array_tarefaturma, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_tarefaturma;
    }

    // --------------------------------------------------------------------------------
    // listByIdInstituicaoLocalizador
    // Lista os registros com base em IdInstituicao, Localizador
    // --------------------------------------------------------------------------------
    public function listByIdInstituicaoLocalizador(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdInstituicao = null, 
        string $Localizador = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdInstituicao, null, null, null, null, null, null, $Localizador, null, null, null, null, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdTarefaTurma = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $IdCurso = null, 
        string $IdTurma = null, 
        string $IdProfessor = null, 
        string $IdDisciplina = null, 
        string $IdJogo = null, 
        string $Localizador = null, 
        string $Titulo = null, 
        string $Descricao = null, 
        string $DataInicial = null, 
        string $DataFinal = null, 
        int $MetaPontuacao = null, 
        int $MetaQtdPartidas = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdTarefaTurma) && is_null($IdInstituicao) && is_null($IdUnidade)
             && is_null($IdCurso) && is_null($IdTurma) && is_null($IdProfessor)
             && is_null($IdDisciplina) && is_null($IdJogo) && is_null($Localizador)
             && is_null($Titulo) && is_null($Descricao) && is_null($DataInicial)
             && is_null($DataFinal) && is_null($MetaPontuacao) && is_null($MetaQtdPartidas)
             && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idtarefaturma as IdTarefaTurma, 
                    idinstituicao as IdInstituicao, 
                    idunidade as IdUnidade, 
                    idcurso as IdCurso, 
                    idturma as IdTurma, 
                    idprofessor as IdProfessor, 
                    iddisciplina as IdDisciplina, 
                    idjogo as IdJogo, 
                    localizador as Localizador, 
                    titulo as Titulo, 
                    descricao as Descricao, 
                    datainicial as DataInicial, 
                    datafinal as DataFinal, 
                    metapontuacao as MetaPontuacao, 
                    metaqtdpartidas as MetaQtdPartidas, 
                    status as Status
                from
                    tarefaturma
                where 1 = 1";

        if (isset($IdTarefaTurma)) { $sql = $sql . " and (idtarefaturma = " . $this->o_db->quote($IdTarefaTurma) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($IdTurma)) { $sql = $sql . " and (idturma = " . $this->o_db->quote($IdTurma) . ")"; }
        if (isset($IdProfessor)) { $sql = $sql . " and (idprofessor = " . $this->o_db->quote($IdProfessor) . ")"; }
        if (isset($IdDisciplina)) { $sql = $sql . " and (iddisciplina = " . $this->o_db->quote($IdDisciplina) . ")"; }
        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($Localizador)) { $sql = $sql . " and (localizador = " . $this->o_db->quote($Localizador) . ")"; }
        if (isset($Titulo)) { $sql = $sql . " and (titulo = " . $this->o_db->quote($Titulo) . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao = " . $this->o_db->quote($Descricao) . ")"; }
        if (isset($DataInicial)) { $sql = $sql . " and (datainicial = " . $this->o_db->quote($DataInicial) . ")"; }
        if (isset($DataFinal)) { $sql = $sql . " and (datafinal = " . $this->o_db->quote($DataFinal) . ")"; }
        if (isset($MetaPontuacao)) { $sql = $sql . " and (metapontuacao = " . $MetaPontuacao . ")"; }
        if (isset($MetaQtdPartidas)) { $sql = $sql . " and (metaqtdpartidas = " . $MetaQtdPartidas . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new TarefaTurmaModel();

                $obj_out->IdTarefaTurma = $obj_in->IdTarefaTurma;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdUnidade = $obj_in->IdUnidade;
                $obj_out->IdCurso = $obj_in->IdCurso;
                $obj_out->IdTurma = $obj_in->IdTurma;
                $obj_out->IdProfessor = $obj_in->IdProfessor;
                $obj_out->IdDisciplina = $obj_in->IdDisciplina;
                $obj_out->IdJogo = $obj_in->IdJogo;
                $obj_out->Localizador = $obj_in->Localizador;
                $obj_out->Titulo = $obj_in->Titulo;
                $obj_out->Descricao = $obj_in->Descricao;
                $obj_out->DataInicial = $obj_in->DataInicial;
                $obj_out->DataFinal = $obj_in->DataFinal;
                $obj_out->MetaPontuacao = $obj_in->MetaPontuacao;
                $obj_out->MetaQtdPartidas = $obj_in->MetaQtdPartidas;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdTarefaTurma)
    {
        $obj = $this->objectByFields($IdTarefaTurma, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        if ($obj) {
            $this->IdTarefaTurma = $obj->IdTarefaTurma;
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->IdUnidade = $obj->IdUnidade;
            $this->IdCurso = $obj->IdCurso;
            $this->IdTurma = $obj->IdTurma;
            $this->IdProfessor = $obj->IdProfessor;
            $this->IdDisciplina = $obj->IdDisciplina;
            $this->IdJogo = $obj->IdJogo;
            $this->Localizador = $obj->Localizador;
            $this->Titulo = $obj->Titulo;
            $this->Descricao = $obj->Descricao;
            $this->DataInicial = $obj->DataInicial;
            $this->DataFinal = $obj->DataFinal;
            $this->MetaPontuacao = $obj->MetaPontuacao;
            $this->MetaQtdPartidas = $obj->MetaQtdPartidas;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdTarefaTurma, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdInstituicaoLocalizador
    // Verifica se existe um registro com IdInstituicao, Localizador
    // --------------------------------------------------------------------------------
    public function existsIdInstituicaoLocalizador()
    {
        $obj = $this->objectByFields(null, $this->IdInstituicao, null, null, null, null, null, null, $this->Localizador, null, null, null, null, null, null, null);
        return !($obj && ($obj->IdTarefaTurma === $this->IdTarefaTurma));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdTarefaTurma = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $IdCurso = null, 
        string $IdTurma = null, 
        string $IdProfessor = null, 
        string $IdDisciplina = null, 
        string $IdJogo = null, 
        string $Localizador = null, 
        string $Titulo = null, 
        string $Descricao = null, 
        string $DataInicial = null, 
        string $DataFinal = null, 
        int $MetaPontuacao = null, 
        int $MetaQtdPartidas = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    tarefaturma
                where 1 = 1";

        if (isset($IdTarefaTurma)) { $sql = $sql . " and (idtarefaturma = " . $this->o_db->quote($IdTarefaTurma) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($IdTurma)) { $sql = $sql . " and (idturma = " . $this->o_db->quote($IdTurma) . ")"; }
        if (isset($IdProfessor)) { $sql = $sql . " and (idprofessor = " . $this->o_db->quote($IdProfessor) . ")"; }
        if (isset($IdDisciplina)) { $sql = $sql . " and (iddisciplina = " . $this->o_db->quote($IdDisciplina) . ")"; }
        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($Localizador)) { $sql = $sql . " and (localizador like " . $this->o_db->quote("%" . $Localizador. "%") . ")"; }
        if (isset($Titulo)) { $sql = $sql . " and (titulo like " . $this->o_db->quote("%" . $Titulo. "%") . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao like " . $this->o_db->quote("%" . $Descricao. "%") . ")"; }
        if (isset($DataInicial)) { $sql = $sql . " and (datainicial like " . $this->o_db->quote("%" . $DataInicial. "%") . ")"; }
        if (isset($DataFinal)) { $sql = $sql . " and (datafinal like " . $this->o_db->quote("%" . $DataFinal. "%") . ")"; }
        if (isset($MetaPontuacao)) { $sql = $sql . " and (metapontuacao = " . MetaPontuacao . ")"; }
        if (isset($MetaQtdPartidas)) { $sql = $sql . " and (metaqtdpartidas = " . MetaQtdPartidas . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdInstituicaoLocalizador
    // Conta os registros com base em IdInstituicao, Localizador
    // --------------------------------------------------------------------------------
    public function countByIdInstituicaoLocalizador(
        string $IdInstituicao = null, 
        string $Localizador = null)
    {
        return $this->countBy(null, $IdInstituicao, null, null, null, null, null, null, $Localizador, null, null, null, null, null, null, null);
    }

}

?>
