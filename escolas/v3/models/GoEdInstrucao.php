<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// GoEdInstrucaoModel
//
// Instruções utilizadas no decorrer das partidas.
//
// Gerado em: 2018-03-26 05:03:31
// --------------------------------------------------------------------------------
class GoEdInstrucaoModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdGoEdInstrucao = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdGoEdInstrucao;      // char(32), PK, obrigatório - Identificação da Instrução GOEDUCA
    private $IdGoEdDisciplina;     // char(32), FK, obrigatório - Identificação da Disciplina GOEDUCA
    private $IdGoEdTema;           // char(32), FK, obrigatório - Identificação do Tema dentro uma Disciplina GOEDUCA
    private $IdGoEdTopico;         // char(32), FK, opcional - Identificação do Tópico de um Tema dentro uma Disciplina GOEDUCA
    private $IdGoEdCurso;          // char(32), FK, opcional - Identificação do Curso GOEDUCA
    private $IdGoEdEtapa;          // char(32), FK, opcional - Identificação da Etapa de um Curso GOEDUCA
    private $Nome;                 // varchar(256), obrigatório - Nome da instrução
    private $Descricao;            // text, opcional - Descrição da instrução
    private $TipoMidia = 'Texto';  // varchar(64), obrigatório - Indica o tipo de mídia da questão
    private $Dimensao;             // varchar(64), opcional - Dimensão em pixels (Larguara x Altura) para tipos de mídia imagem e vídeo
    private $Status = 'AT';        // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdGoEdInstrucao") { return $this->IdGoEdInstrucao; }
        if ($name === "IdGoEdDisciplina") { return $this->IdGoEdDisciplina; }
        if ($name === "IdGoEdTema") { return $this->IdGoEdTema; }
        if ($name === "IdGoEdTopico") { return $this->IdGoEdTopico; }
        if ($name === "IdGoEdCurso") { return $this->IdGoEdCurso; }
        if ($name === "IdGoEdEtapa") { return $this->IdGoEdEtapa; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "Descricao") { return $this->Descricao; }
        if ($name === "TipoMidia") { return $this->TipoMidia; }
        if ($name === "Dimensao") { return $this->Dimensao; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdGoEdInstrucao") {
            if (is_null($value)) {
                $this->IdGoEdInstrucao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdInstrucao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdInstrucao;
        }
        if ($name === "IdGoEdDisciplina") {
            if (is_null($value)) {
                $this->IdGoEdDisciplina = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdDisciplina = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdDisciplina;
        }
        if ($name === "IdGoEdTema") {
            if (is_null($value)) {
                $this->IdGoEdTema = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdTema = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdTema;
        }
        if ($name === "IdGoEdTopico") {
            if (is_null($value)) {
                $this->IdGoEdTopico = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdTopico = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdTopico;
        }
        if ($name === "IdGoEdCurso") {
            if (is_null($value)) {
                $this->IdGoEdCurso = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdCurso = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdCurso;
        }
        if ($name === "IdGoEdEtapa") {
            if (is_null($value)) {
                $this->IdGoEdEtapa = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdEtapa = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdEtapa;
        }
        if ($name === "Nome") {
            if (is_null($value)) {
                $this->Nome = null;
            }
            else {
                $this->Nome = substr((string) $value, 0, 256);
            }
            return $this->Nome;
        }
        if ($name === "Descricao") {
            if (is_null($value)) {
                $this->Descricao = null;
            }
            else {
                $this->Descricao = substr((string) $value, 0, 65535);
            }
            return $this->Descricao;
        }
        if ($name === "TipoMidia") {
            if (is_null($value)) {
                $this->TipoMidia = null;
            }
            else {
                $this->TipoMidia = substr((string) $value, 0, 64);
            }
            return $this->TipoMidia;
        }
        if ($name === "Dimensao") {
            if (is_null($value)) {
                $this->Dimensao = null;
            }
            else {
                $this->Dimensao = substr((string) $value, 0, 64);
            }
            return $this->Dimensao;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        goedinstrucao
                    set 
                        idgoedinstrucao = " . ( isset($this->IdGoEdInstrucao) ? $this->o_db->quote($IdGoEdInstrucao) : "null" ) . ", 
                        idgoeddisciplina = " . ( isset($this->IdGoEdDisciplina) ? $this->o_db->quote($IdGoEdDisciplina) : "null" ) . ", 
                        idgoedtema = " . ( isset($this->IdGoEdTema) ? $this->o_db->quote($IdGoEdTema) : "null" ) . ", 
                        idgoedtopico = " . ( isset($this->IdGoEdTopico) ? $this->o_db->quote($IdGoEdTopico) : "null" ) . ", 
                        idgoedcurso = " . ( isset($this->IdGoEdCurso) ? $this->o_db->quote($IdGoEdCurso) : "null" ) . ", 
                        idgoedetapa = " . ( isset($this->IdGoEdEtapa) ? $this->o_db->quote($IdGoEdEtapa) : "null" ) . ", 
                        nome = " . ( isset($this->Nome) ? $this->o_db->quote($Nome) : "null" ) . ", 
                        descricao = " . ( isset($this->Descricao) ? $this->o_db->quote($Descricao) : "null" ) . ", 
                        tipomidia = " . ( isset($this->TipoMidia) ? $this->o_db->quote($TipoMidia) : "null" ) . ", 
                        dimensao = " . ( isset($this->Dimensao) ? $this->o_db->quote($Dimensao) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idgoedinstrucao" . ( isset($this->IdGoEdInstrucao) ? " = " . $this->o_db->quote($this->IdGoEdInstrucao) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        goedinstrucao (
                            idgoedinstrucao, 
                            idgoeddisciplina, 
                            idgoedtema, 
                            idgoedtopico, 
                            idgoedcurso, 
                            idgoedetapa, 
                            nome, 
                            descricao, 
                            tipomidia, 
                            dimensao, 
                            status
                        )
                        values (
                            " . ( isset($this->IdGoEdInstrucao) ? $this->o_db->quote($this->IdGoEdInstrucao) : "null" ) . ", 
                            " . ( isset($this->IdGoEdDisciplina) ? $this->o_db->quote($this->IdGoEdDisciplina) : "null" ) . ", 
                            " . ( isset($this->IdGoEdTema) ? $this->o_db->quote($this->IdGoEdTema) : "null" ) . ", 
                            " . ( isset($this->IdGoEdTopico) ? $this->o_db->quote($this->IdGoEdTopico) : "null" ) . ", 
                            " . ( isset($this->IdGoEdCurso) ? $this->o_db->quote($this->IdGoEdCurso) : "null" ) . ", 
                            " . ( isset($this->IdGoEdEtapa) ? $this->o_db->quote($this->IdGoEdEtapa) : "null" ) . ", 
                            " . ( isset($this->Nome) ? $this->o_db->quote($this->Nome) : "null" ) . ", 
                            " . ( isset($this->Descricao) ? $this->o_db->quote($this->Descricao) : "null" ) . ", 
                            " . ( isset($this->TipoMidia) ? $this->o_db->quote($this->TipoMidia) : "null" ) . ", 
                            " . ( isset($this->Dimensao) ? $this->o_db->quote($this->Dimensao) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdGoEdInstrucao)) {
            $sql = "delete from 
                        goedinstrucao
                     where 
                        idgoedinstrucao" . ( isset($this->IdGoEdInstrucao) ? " = " . $this->o_db->quote($this->IdGoEdInstrucao) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdInstrucao = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $IdGoEdTopico = null, 
        string $IdGoEdCurso = null, 
        string $IdGoEdEtapa = null, 
        string $Nome = null, 
        string $Descricao = null, 
        string $TipoMidia = null, 
        string $Dimensao = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idgoedinstrucao as IdGoEdInstrucao, 
                    idgoeddisciplina as IdGoEdDisciplina, 
                    idgoedtema as IdGoEdTema, 
                    idgoedtopico as IdGoEdTopico, 
                    idgoedcurso as IdGoEdCurso, 
                    idgoedetapa as IdGoEdEtapa, 
                    nome as Nome, 
                    descricao as Descricao, 
                    tipomidia as TipoMidia, 
                    dimensao as Dimensao, 
                    status as Status
                from
                    goedinstrucao
                where 1 = 1";

        if (isset($IdGoEdInstrucao)) { $sql = $sql . " and (idgoedinstrucao = " . $this->o_db->quote($IdGoEdInstrucao) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($IdGoEdCurso)) { $sql = $sql . " and (idgoedcurso = " . $this->o_db->quote($IdGoEdCurso) . ")"; }
        if (isset($IdGoEdEtapa)) { $sql = $sql . " and (idgoedetapa = " . $this->o_db->quote($IdGoEdEtapa) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao like " . $this->o_db->quote("%" . $Descricao. "%") . ")"; }
        if (isset($TipoMidia)) { $sql = $sql . " and (tipomidia like " . $this->o_db->quote("%" . $TipoMidia. "%") . ")"; }
        if (isset($Dimensao)) { $sql = $sql . " and (dimensao like " . $this->o_db->quote("%" . $Dimensao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_goedinstrucao = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdInstrucaoModel();

                $obj_out->IdGoEdInstrucao = $obj_in->IdGoEdInstrucao;
                $obj_out->IdGoEdDisciplina = $obj_in->IdGoEdDisciplina;
                $obj_out->IdGoEdTema = $obj_in->IdGoEdTema;
                $obj_out->IdGoEdTopico = $obj_in->IdGoEdTopico;
                $obj_out->IdGoEdCurso = $obj_in->IdGoEdCurso;
                $obj_out->IdGoEdEtapa = $obj_in->IdGoEdEtapa;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Descricao = $obj_in->Descricao;
                $obj_out->TipoMidia = $obj_in->TipoMidia;
                $obj_out->Dimensao = $obj_in->Dimensao;
                $obj_out->Status = $obj_in->Status;

                array_push($array_goedinstrucao, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_goedinstrucao;
    }

    // --------------------------------------------------------------------------------
    // listByIdGoEdDisciplinaIdGoEdTemaNome
    // Lista os registros com base em IdGoEdDisciplina, IdGoEdTema, Nome
    // --------------------------------------------------------------------------------
    public function listByIdGoEdDisciplinaIdGoEdTemaNome(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $Nome = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdGoEdDisciplina, $IdGoEdTema, null, null, null, $Nome, null, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdGoEdInstrucao = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $IdGoEdTopico = null, 
        string $IdGoEdCurso = null, 
        string $IdGoEdEtapa = null, 
        string $Nome = null, 
        string $Descricao = null, 
        string $TipoMidia = null, 
        string $Dimensao = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdGoEdInstrucao) && is_null($IdGoEdDisciplina) && is_null($IdGoEdTema)
             && is_null($IdGoEdTopico) && is_null($IdGoEdCurso) && is_null($IdGoEdEtapa)
             && is_null($Nome) && is_null($Descricao) && is_null($TipoMidia)
             && is_null($Dimensao) && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idgoedinstrucao as IdGoEdInstrucao, 
                    idgoeddisciplina as IdGoEdDisciplina, 
                    idgoedtema as IdGoEdTema, 
                    idgoedtopico as IdGoEdTopico, 
                    idgoedcurso as IdGoEdCurso, 
                    idgoedetapa as IdGoEdEtapa, 
                    nome as Nome, 
                    descricao as Descricao, 
                    tipomidia as TipoMidia, 
                    dimensao as Dimensao, 
                    status as Status
                from
                    goedinstrucao
                where 1 = 1";

        if (isset($IdGoEdInstrucao)) { $sql = $sql . " and (idgoedinstrucao = " . $this->o_db->quote($IdGoEdInstrucao) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($IdGoEdCurso)) { $sql = $sql . " and (idgoedcurso = " . $this->o_db->quote($IdGoEdCurso) . ")"; }
        if (isset($IdGoEdEtapa)) { $sql = $sql . " and (idgoedetapa = " . $this->o_db->quote($IdGoEdEtapa) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome = " . $this->o_db->quote($Nome) . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao = " . $this->o_db->quote($Descricao) . ")"; }
        if (isset($TipoMidia)) { $sql = $sql . " and (tipomidia = " . $this->o_db->quote($TipoMidia) . ")"; }
        if (isset($Dimensao)) { $sql = $sql . " and (dimensao = " . $this->o_db->quote($Dimensao) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdInstrucaoModel();

                $obj_out->IdGoEdInstrucao = $obj_in->IdGoEdInstrucao;
                $obj_out->IdGoEdDisciplina = $obj_in->IdGoEdDisciplina;
                $obj_out->IdGoEdTema = $obj_in->IdGoEdTema;
                $obj_out->IdGoEdTopico = $obj_in->IdGoEdTopico;
                $obj_out->IdGoEdCurso = $obj_in->IdGoEdCurso;
                $obj_out->IdGoEdEtapa = $obj_in->IdGoEdEtapa;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Descricao = $obj_in->Descricao;
                $obj_out->TipoMidia = $obj_in->TipoMidia;
                $obj_out->Dimensao = $obj_in->Dimensao;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdGoEdInstrucao)
    {
        $obj = $this->objectByFields($IdGoEdInstrucao, null, null, null, null, null, null, null, null, null, null);
        if ($obj) {
            $this->IdGoEdInstrucao = $obj->IdGoEdInstrucao;
            $this->IdGoEdDisciplina = $obj->IdGoEdDisciplina;
            $this->IdGoEdTema = $obj->IdGoEdTema;
            $this->IdGoEdTopico = $obj->IdGoEdTopico;
            $this->IdGoEdCurso = $obj->IdGoEdCurso;
            $this->IdGoEdEtapa = $obj->IdGoEdEtapa;
            $this->Nome = $obj->Nome;
            $this->Descricao = $obj->Descricao;
            $this->TipoMidia = $obj->TipoMidia;
            $this->Dimensao = $obj->Dimensao;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdGoEdInstrucao, null, null, null, null, null, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdGoEdDisciplinaIdGoEdTemaNome
    // Verifica se existe um registro com IdGoEdDisciplina, IdGoEdTema, Nome
    // --------------------------------------------------------------------------------
    public function existsIdGoEdDisciplinaIdGoEdTemaNome()
    {
        $obj = $this->objectByFields(null, $this->IdGoEdDisciplina, $this->IdGoEdTema, null, null, null, $this->Nome, null, null, null, null);
        return !($obj && ($obj->IdGoEdInstrucao === $this->IdGoEdInstrucao));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdGoEdInstrucao = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $IdGoEdTopico = null, 
        string $IdGoEdCurso = null, 
        string $IdGoEdEtapa = null, 
        string $Nome = null, 
        string $Descricao = null, 
        string $TipoMidia = null, 
        string $Dimensao = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    goedinstrucao
                where 1 = 1";

        if (isset($IdGoEdInstrucao)) { $sql = $sql . " and (idgoedinstrucao = " . $this->o_db->quote($IdGoEdInstrucao) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($IdGoEdCurso)) { $sql = $sql . " and (idgoedcurso = " . $this->o_db->quote($IdGoEdCurso) . ")"; }
        if (isset($IdGoEdEtapa)) { $sql = $sql . " and (idgoedetapa = " . $this->o_db->quote($IdGoEdEtapa) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao like " . $this->o_db->quote("%" . $Descricao. "%") . ")"; }
        if (isset($TipoMidia)) { $sql = $sql . " and (tipomidia like " . $this->o_db->quote("%" . $TipoMidia. "%") . ")"; }
        if (isset($Dimensao)) { $sql = $sql . " and (dimensao like " . $this->o_db->quote("%" . $Dimensao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdGoEdDisciplinaIdGoEdTemaNome
    // Conta os registros com base em IdGoEdDisciplina, IdGoEdTema, Nome
    // --------------------------------------------------------------------------------
    public function countByIdGoEdDisciplinaIdGoEdTemaNome(
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $Nome = null)
    {
        return $this->countBy(null, $IdGoEdDisciplina, $IdGoEdTema, null, null, null, $Nome, null, null, null, null);
    }

}

?>
