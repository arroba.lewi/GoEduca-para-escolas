<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// JogoJogoPlataformaModel
//
// Estabelece as plataformas operacionais aos quais os jogos são compatíveis.
//
// Gerado em: 2018-03-26 05:03:54
// --------------------------------------------------------------------------------
class JogoJogoPlataformaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdJogo;             // char(32), PK, FK, obrigatório - Identificação do Jogo
    private $IdJogoPlataforma;   // char(32), PK, FK, obrigatório - Identificação da Plataforma do Jogo

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdJogo") { return $this->IdJogo; }
        if ($name === "IdJogoPlataforma") { return $this->IdJogoPlataforma; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdJogo") {
            if (is_null($value)) {
                $this->IdJogo = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdJogo = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdJogo;
        }
        if ($name === "IdJogoPlataforma") {
            if (is_null($value)) {
                $this->IdJogoPlataforma = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdJogoPlataforma = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdJogoPlataforma;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        jogo_jogoplataforma
                    set 
                        idjogo = " . ( isset($this->IdJogo) ? $this->o_db->quote($IdJogo) : "null" ) . ", 
                        idjogoplataforma = " . ( isset($this->IdJogoPlataforma) ? $this->o_db->quote($IdJogoPlataforma) : "null" ) . "
                    where 
                        idjogo" . ( isset($this->IdJogo) ? " = " . $this->o_db->quote($this->IdJogo) : " is null" ) . "
                        and
                        idjogoplataforma" . ( isset($this->IdJogoPlataforma) ? " = " . $this->o_db->quote($this->IdJogoPlataforma) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        jogo_jogoplataforma (
                            idjogo, 
                            idjogoplataforma
                        )
                        values (
                            " . ( isset($this->IdJogo) ? $this->o_db->quote($this->IdJogo) : "null" ) . ", 
                            " . ( isset($this->IdJogoPlataforma) ? $this->o_db->quote($this->IdJogoPlataforma) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdJogo) && isset($this->IdJogoPlataforma)) {
            $sql = "delete from 
                        jogo_jogoplataforma
                     where 
                        idjogo" . ( isset($this->IdJogo) ? " = " . $this->o_db->quote($this->IdJogo) : " is null" ) . "
                        and 
                        idjogoplataforma" . ( isset($this->IdJogoPlataforma) ? " = " . $this->o_db->quote($this->IdJogoPlataforma) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdJogo = null, 
        string $IdJogoPlataforma = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idjogo as IdJogo, 
                    idjogoplataforma as IdJogoPlataforma
                from
                    jogo_jogoplataforma
                where 1 = 1";

        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdJogoPlataforma)) { $sql = $sql . " and (idjogoplataforma = " . $this->o_db->quote($IdJogoPlataforma) . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_jogo_jogoplataforma = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new JogoJogoPlataformaModel();

                $obj_out->IdJogo = $obj_in->IdJogo;
                $obj_out->IdJogoPlataforma = $obj_in->IdJogoPlataforma;

                array_push($array_jogo_jogoplataforma, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_jogo_jogoplataforma;
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdJogo = null, 
        string $IdJogoPlataforma = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdJogo) && is_null($IdJogoPlataforma)) {
            return null;
        }

        $sql = "select
                    idjogo as IdJogo, 
                    idjogoplataforma as IdJogoPlataforma
                from
                    jogo_jogoplataforma
                where 1 = 1";

        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdJogoPlataforma)) { $sql = $sql . " and (idjogoplataforma = " . $this->o_db->quote($IdJogoPlataforma) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new JogoJogoPlataformaModel();

                $obj_out->IdJogo = $obj_in->IdJogo;
                $obj_out->IdJogoPlataforma = $obj_in->IdJogoPlataforma;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdJogo, string $IdJogoPlataforma)
    {
        $obj = $this->objectByFields($IdJogo, $IdJogoPlataforma);
        if ($obj) {
            $this->IdJogo = $obj->IdJogo;
            $this->IdJogoPlataforma = $obj->IdJogoPlataforma;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdJogo, $this->IdJogoPlataforma);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdJogo = null, 
        string $IdJogoPlataforma = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    jogo_jogoplataforma
                where 1 = 1";

        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdJogoPlataforma)) { $sql = $sql . " and (idjogoplataforma = " . $this->o_db->quote($IdJogoPlataforma) . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
}

?>
