<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// ContaPessoaModel
//
// Conta de acesso disponibilizados pelas instituição às pessoas/alunos.
//
// Gerado em: 2018-03-26 05:03:10
// --------------------------------------------------------------------------------
class ContaPessoaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdContaPessoa = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdContaPessoa;      // char(32), PK, obrigatório - Identificador da Conta da Pessoa
    private $IdInstituicao;      // char(32), FK, opcional - Identificador da Instituição
    private $IdPessoa;           // char(32), FK, obrigatório - Identificador da Pessoa
    private $Matricula;          // varchar(16), opcional - Matrícula junto a Instituição
    private $DataCadastro;       // date, opcional - Data de cancelamento da conta
    private $DataCancelamento;   // date, opcional - Data de cancelamento da conta
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdContaPessoa") { return $this->IdContaPessoa; }
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "IdPessoa") { return $this->IdPessoa; }
        if ($name === "Matricula") { return $this->Matricula; }
        if ($name === "DataCadastro") { return ($this->DataCadastro ? $this->DataCadastro->format("d-m-Y") : null); }
        if ($name === "DataCancelamento") { return ($this->DataCancelamento ? $this->DataCancelamento->format("d-m-Y") : null); }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdContaPessoa") {
            if (is_null($value)) {
                $this->IdContaPessoa = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdContaPessoa = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdContaPessoa;
        }
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdInstituicao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdInstituicao;
        }
        if ($name === "IdPessoa") {
            if (is_null($value)) {
                $this->IdPessoa = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdPessoa = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdPessoa;
        }
        if ($name === "Matricula") {
            if (is_null($value)) {
                $this->Matricula = null;
            }
            else {
                $this->Matricula = substr((string) $value, 0, 16);
            }
            return $this->Matricula;
        }
        if ($name === "DataCadastro") {
            if (is_null($value)) {
                $this->DataCadastro = null;
            }
            else {
                if (isset($value) && DateTime::createFromFormat("d-m-Y", $value)) {
                    $this->DataCadastro = DateTime::createFromFormat("d-m-Y", $value);
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo DATE(dd-mm-aaaa) inválido.");
                }
            }
            return $this->DataCadastro->format("d-m-Y");
        }
        if ($name === "DataCancelamento") {
            if (is_null($value)) {
                $this->DataCancelamento = null;
            }
            else {
                if (isset($value) && DateTime::createFromFormat("d-m-Y", $value)) {
                    $this->DataCancelamento = DateTime::createFromFormat("d-m-Y", $value);
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo DATE(dd-mm-aaaa) inválido.");
                }
            }
            return $this->DataCancelamento->format("d-m-Y");
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        contapessoa
                    set 
                        idcontapessoa = " . ( isset($this->IdContaPessoa) ? $this->o_db->quote($IdContaPessoa) : "null" ) . ", 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        idpessoa = " . ( isset($this->IdPessoa) ? $this->o_db->quote($IdPessoa) : "null" ) . ", 
                        matricula = " . ( isset($this->Matricula) ? $this->o_db->quote($Matricula) : "null" ) . ", 
                        datacadastro = " . ( isset($this->DataCadastro) ? $this->DataCadastro->format("Y-m-d") : "null" ) . ", 
                        datacancelamento = " . ( isset($this->DataCancelamento) ? $this->DataCancelamento->format("Y-m-d") : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idcontapessoa" . ( isset($this->IdContaPessoa) ? " = " . $this->o_db->quote($this->IdContaPessoa) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        contapessoa (
                            idcontapessoa, 
                            idinstituicao, 
                            idpessoa, 
                            matricula, 
                            datacadastro, 
                            datacancelamento, 
                            status
                        )
                        values (
                            " . ( isset($this->IdContaPessoa) ? $this->o_db->quote($this->IdContaPessoa) : "null" ) . ", 
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->IdPessoa) ? $this->o_db->quote($this->IdPessoa) : "null" ) . ", 
                            " . ( isset($this->Matricula) ? $this->o_db->quote($this->Matricula) : "null" ) . ", 
                            " . ( isset($this->DataCadastro) ? $this->o_db->quote($this->DataCadastro->format("Y-m-d")) : "null" ) . ", 
                            " . ( isset($this->DataCancelamento) ? $this->o_db->quote($this->DataCancelamento->format("Y-m-d")) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdContaPessoa)) {
            $sql = "delete from 
                        contapessoa
                     where 
                        idcontapessoa" . ( isset($this->IdContaPessoa) ? " = " . $this->o_db->quote($this->IdContaPessoa) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdContaPessoa = null, 
        string $IdInstituicao = null, 
        string $IdPessoa = null, 
        string $Matricula = null, 
        string $DataCadastro = null, 
        string $DataCancelamento = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idcontapessoa as IdContaPessoa, 
                    idinstituicao as IdInstituicao, 
                    idpessoa as IdPessoa, 
                    matricula as Matricula, 
                    datacadastro as DataCadastro, 
                    datacancelamento as DataCancelamento, 
                    status as Status
                from
                    contapessoa
                where 1 = 1";

        if (isset($IdContaPessoa)) { $sql = $sql . " and (idcontapessoa = " . $this->o_db->quote($IdContaPessoa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($Matricula)) { $sql = $sql . " and (matricula like " . $this->o_db->quote("%" . $Matricula. "%") . ")"; }
        if (isset($DataCadastro)) { $sql = $sql . " and (datacadastro = " . $this->o_db->quote(isset($DataCadastro) ? DateTime::createFromFormat("d-m-Y", $DataCadastro)->format("Y-m-d") : "") . ")"; }
        if (isset($DataCancelamento)) { $sql = $sql . " and (datacancelamento = " . $this->o_db->quote(isset($DataCancelamento) ? DateTime::createFromFormat("d-m-Y", $DataCancelamento)->format("Y-m-d") : "") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_contapessoa = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new ContaPessoaModel();

                $obj_out->IdContaPessoa = $obj_in->IdContaPessoa;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->Matricula = $obj_in->Matricula;
                $obj_out->DataCadastro = $obj_in->DataCadastro;
                $obj_out->DataCancelamento = $obj_in->DataCancelamento;
                $obj_out->Status = $obj_in->Status;

                array_push($array_contapessoa, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_contapessoa;
    }

    // --------------------------------------------------------------------------------
    // listByIdInstituicaoIdPessoa
    // Lista os registros com base em IdInstituicao, IdPessoa
    // --------------------------------------------------------------------------------
    public function listByIdInstituicaoIdPessoa(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdInstituicao = null, 
        string $IdPessoa = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdInstituicao, $IdPessoa, null, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdContaPessoa = null, 
        string $IdInstituicao = null, 
        string $IdPessoa = null, 
        string $Matricula = null, 
        string $DataCadastro = null, 
        string $DataCancelamento = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdContaPessoa) && is_null($IdInstituicao) && is_null($IdPessoa)
             && is_null($Matricula) && is_null($DataCadastro) && is_null($DataCancelamento)
             && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idcontapessoa as IdContaPessoa, 
                    idinstituicao as IdInstituicao, 
                    idpessoa as IdPessoa, 
                    matricula as Matricula, 
                    datacadastro as DataCadastro, 
                    datacancelamento as DataCancelamento, 
                    status as Status
                from
                    contapessoa
                where 1 = 1";

        if (isset($IdContaPessoa)) { $sql = $sql . " and (idcontapessoa = " . $this->o_db->quote($IdContaPessoa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($Matricula)) { $sql = $sql . " and (matricula = " . $this->o_db->quote($Matricula) . ")"; }
        if (isset($DataCadastro)) { $sql = $sql . " and (datacadastro = " . $this->o_db->quote(isset($DataCadastro) ? DateTime::createFromFormat("d-m-Y", $DataCadastro)->format("Y-m-d") : "") . ")"; }
        if (isset($DataCancelamento)) { $sql = $sql . " and (datacancelamento = " . $this->o_db->quote(isset($DataCancelamento) ? DateTime::createFromFormat("d-m-Y", $DataCancelamento)->format("Y-m-d") : "") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new ContaPessoaModel();

                $obj_out->IdContaPessoa = $obj_in->IdContaPessoa;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->Matricula = $obj_in->Matricula;
                $obj_out->DataCadastro = $obj_in->DataCadastro;
                $obj_out->DataCancelamento = $obj_in->DataCancelamento;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdContaPessoa)
    {
        $obj = $this->objectByFields($IdContaPessoa, null, null, null, null, null, null);
        if ($obj) {
            $this->IdContaPessoa = $obj->IdContaPessoa;
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->IdPessoa = $obj->IdPessoa;
            $this->Matricula = $obj->Matricula;
            $this->DataCadastro = $obj->DataCadastro;
            $this->DataCancelamento = $obj->DataCancelamento;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdContaPessoa, null, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdInstituicaoIdPessoa
    // Verifica se existe um registro com IdInstituicao, IdPessoa
    // --------------------------------------------------------------------------------
    public function existsIdInstituicaoIdPessoa()
    {
        $obj = $this->objectByFields(null, $this->IdInstituicao, $this->IdPessoa, null, null, null, null);
        return !($obj && ($obj->IdContaPessoa === $this->IdContaPessoa));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdContaPessoa = null, 
        string $IdInstituicao = null, 
        string $IdPessoa = null, 
        string $Matricula = null, 
        string $DataCadastro = null, 
        string $DataCancelamento = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    contapessoa
                where 1 = 1";

        if (isset($IdContaPessoa)) { $sql = $sql . " and (idcontapessoa = " . $this->o_db->quote($IdContaPessoa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($Matricula)) { $sql = $sql . " and (matricula like " . $this->o_db->quote("%" . $Matricula. "%") . ")"; }
        if (isset($DataCadastro)) { $sql = $sql . " and (datacadastro = " . $this->o_db->quote(DateTime::createFromFormat("d-m-Y", $DataCadastro) ? DateTime::createFromFormat("d-m-Y", $DataCadastro)->format("Y-m-d") : "") . ")"; }
        if (isset($DataCancelamento)) { $sql = $sql . " and (datacancelamento = " . $this->o_db->quote(DateTime::createFromFormat("d-m-Y", $DataCancelamento) ? DateTime::createFromFormat("d-m-Y", $DataCancelamento)->format("Y-m-d") : "") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdInstituicaoIdPessoa
    // Conta os registros com base em IdInstituicao, IdPessoa
    // --------------------------------------------------------------------------------
    public function countByIdInstituicaoIdPessoa(
        string $IdInstituicao = null, 
        string $IdPessoa = null)
    {
        return $this->countBy(null, $IdInstituicao, $IdPessoa, null, null, null, null);
    }

}

?>
