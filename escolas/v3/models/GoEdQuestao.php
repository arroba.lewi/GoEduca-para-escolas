<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// GoEdQuestaoModel
//
// Questões utilizadas no decorrer das partidas.
//
// Gerado em: 2018-03-26 05:03:33
// --------------------------------------------------------------------------------
class GoEdQuestaoModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdGoEdQuestao = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdGoEdQuestao;        // char(32), PK, obrigatório - Identificação da Questão GOEDUCA
    private $IdGoEdDisciplina;     // char(32), FK, obrigatório - Identificação da Disciplina GOEDUCA
    private $IdGoEdTema;           // char(32), FK, obrigatório - Identificação do Tema dentro uma Disciplina GOEDUCA
    private $IdGoEdTopico;         // char(32), FK, opcional - Identificação do Tópico de um Tema dentro uma Disciplina GOEDUCA
    private $Descricao;            // text, opcional - Descrição da Questão
    private $Detalhamento;         // text, opcional - Detalhamento da Questão
    private $TipoMidia = 'Texto';  // varchar(64), obrigatório - Indica o tipo de mídia da questão
    private $Dimensao;             // varchar(64), opcional - Dimensão em pixels (Larguara x Altura) para tipos de mídia imagem e vídeo
    private $Status = 'AT';        // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdGoEdQuestao") { return $this->IdGoEdQuestao; }
        if ($name === "IdGoEdDisciplina") { return $this->IdGoEdDisciplina; }
        if ($name === "IdGoEdTema") { return $this->IdGoEdTema; }
        if ($name === "IdGoEdTopico") { return $this->IdGoEdTopico; }
        if ($name === "Descricao") { return $this->Descricao; }
        if ($name === "Detalhamento") { return $this->Detalhamento; }
        if ($name === "TipoMidia") { return $this->TipoMidia; }
        if ($name === "Dimensao") { return $this->Dimensao; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdGoEdQuestao") {
            if (is_null($value)) {
                $this->IdGoEdQuestao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdQuestao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdQuestao;
        }
        if ($name === "IdGoEdDisciplina") {
            if (is_null($value)) {
                $this->IdGoEdDisciplina = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdDisciplina = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdDisciplina;
        }
        if ($name === "IdGoEdTema") {
            if (is_null($value)) {
                $this->IdGoEdTema = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdTema = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdTema;
        }
        if ($name === "IdGoEdTopico") {
            if (is_null($value)) {
                $this->IdGoEdTopico = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdTopico = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdTopico;
        }
        if ($name === "Descricao") {
            if (is_null($value)) {
                $this->Descricao = null;
            }
            else {
                $this->Descricao = substr((string) $value, 0, 65535);
            }
            return $this->Descricao;
        }
        if ($name === "Detalhamento") {
            if (is_null($value)) {
                $this->Detalhamento = null;
            }
            else {
                $this->Detalhamento = substr((string) $value, 0, 65535);
            }
            return $this->Detalhamento;
        }
        if ($name === "TipoMidia") {
            if (is_null($value)) {
                $this->TipoMidia = null;
            }
            else {
                $this->TipoMidia = substr((string) $value, 0, 64);
            }
            return $this->TipoMidia;
        }
        if ($name === "Dimensao") {
            if (is_null($value)) {
                $this->Dimensao = null;
            }
            else {
                $this->Dimensao = substr((string) $value, 0, 64);
            }
            return $this->Dimensao;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        goedquestao
                    set 
                        idgoedquestao = " . ( isset($this->IdGoEdQuestao) ? $this->o_db->quote($IdGoEdQuestao) : "null" ) . ", 
                        idgoeddisciplina = " . ( isset($this->IdGoEdDisciplina) ? $this->o_db->quote($IdGoEdDisciplina) : "null" ) . ", 
                        idgoedtema = " . ( isset($this->IdGoEdTema) ? $this->o_db->quote($IdGoEdTema) : "null" ) . ", 
                        idgoedtopico = " . ( isset($this->IdGoEdTopico) ? $this->o_db->quote($IdGoEdTopico) : "null" ) . ", 
                        descricao = " . ( isset($this->Descricao) ? $this->o_db->quote($Descricao) : "null" ) . ", 
                        detalhamento = " . ( isset($this->Detalhamento) ? $this->o_db->quote($Detalhamento) : "null" ) . ", 
                        tipomidia = " . ( isset($this->TipoMidia) ? $this->o_db->quote($TipoMidia) : "null" ) . ", 
                        dimensao = " . ( isset($this->Dimensao) ? $this->o_db->quote($Dimensao) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idgoedquestao" . ( isset($this->IdGoEdQuestao) ? " = " . $this->o_db->quote($this->IdGoEdQuestao) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        goedquestao (
                            idgoedquestao, 
                            idgoeddisciplina, 
                            idgoedtema, 
                            idgoedtopico, 
                            descricao, 
                            detalhamento, 
                            tipomidia, 
                            dimensao, 
                            status
                        )
                        values (
                            " . ( isset($this->IdGoEdQuestao) ? $this->o_db->quote($this->IdGoEdQuestao) : "null" ) . ", 
                            " . ( isset($this->IdGoEdDisciplina) ? $this->o_db->quote($this->IdGoEdDisciplina) : "null" ) . ", 
                            " . ( isset($this->IdGoEdTema) ? $this->o_db->quote($this->IdGoEdTema) : "null" ) . ", 
                            " . ( isset($this->IdGoEdTopico) ? $this->o_db->quote($this->IdGoEdTopico) : "null" ) . ", 
                            " . ( isset($this->Descricao) ? $this->o_db->quote($this->Descricao) : "null" ) . ", 
                            " . ( isset($this->Detalhamento) ? $this->o_db->quote($this->Detalhamento) : "null" ) . ", 
                            " . ( isset($this->TipoMidia) ? $this->o_db->quote($this->TipoMidia) : "null" ) . ", 
                            " . ( isset($this->Dimensao) ? $this->o_db->quote($this->Dimensao) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdGoEdQuestao)) {
            $sql = "delete from 
                        goedquestao
                     where 
                        idgoedquestao" . ( isset($this->IdGoEdQuestao) ? " = " . $this->o_db->quote($this->IdGoEdQuestao) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdQuestao = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $IdGoEdTopico = null, 
        string $Descricao = null, 
        string $Detalhamento = null, 
        string $TipoMidia = null, 
        string $Dimensao = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idgoedquestao as IdGoEdQuestao, 
                    idgoeddisciplina as IdGoEdDisciplina, 
                    idgoedtema as IdGoEdTema, 
                    idgoedtopico as IdGoEdTopico, 
                    descricao as Descricao, 
                    detalhamento as Detalhamento, 
                    tipomidia as TipoMidia, 
                    dimensao as Dimensao, 
                    status as Status
                from
                    goedquestao
                where 1 = 1";

        if (isset($IdGoEdQuestao)) { $sql = $sql . " and (idgoedquestao = " . $this->o_db->quote($IdGoEdQuestao) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao like " . $this->o_db->quote("%" . $Descricao. "%") . ")"; }
        if (isset($Detalhamento)) { $sql = $sql . " and (detalhamento like " . $this->o_db->quote("%" . $Detalhamento. "%") . ")"; }
        if (isset($TipoMidia)) { $sql = $sql . " and (tipomidia like " . $this->o_db->quote("%" . $TipoMidia. "%") . ")"; }
        if (isset($Dimensao)) { $sql = $sql . " and (dimensao like " . $this->o_db->quote("%" . $Dimensao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_goedquestao = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdQuestaoModel();

                $obj_out->IdGoEdQuestao = $obj_in->IdGoEdQuestao;
                $obj_out->IdGoEdDisciplina = $obj_in->IdGoEdDisciplina;
                $obj_out->IdGoEdTema = $obj_in->IdGoEdTema;
                $obj_out->IdGoEdTopico = $obj_in->IdGoEdTopico;
                $obj_out->Descricao = $obj_in->Descricao;
                $obj_out->Detalhamento = $obj_in->Detalhamento;
                $obj_out->TipoMidia = $obj_in->TipoMidia;
                $obj_out->Dimensao = $obj_in->Dimensao;
                $obj_out->Status = $obj_in->Status;

                array_push($array_goedquestao, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_goedquestao;
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdGoEdQuestao = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $IdGoEdTopico = null, 
        string $Descricao = null, 
        string $Detalhamento = null, 
        string $TipoMidia = null, 
        string $Dimensao = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdGoEdQuestao) && is_null($IdGoEdDisciplina) && is_null($IdGoEdTema)
             && is_null($IdGoEdTopico) && is_null($Descricao) && is_null($Detalhamento)
             && is_null($TipoMidia) && is_null($Dimensao) && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idgoedquestao as IdGoEdQuestao, 
                    idgoeddisciplina as IdGoEdDisciplina, 
                    idgoedtema as IdGoEdTema, 
                    idgoedtopico as IdGoEdTopico, 
                    descricao as Descricao, 
                    detalhamento as Detalhamento, 
                    tipomidia as TipoMidia, 
                    dimensao as Dimensao, 
                    status as Status
                from
                    goedquestao
                where 1 = 1";

        if (isset($IdGoEdQuestao)) { $sql = $sql . " and (idgoedquestao = " . $this->o_db->quote($IdGoEdQuestao) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao = " . $this->o_db->quote($Descricao) . ")"; }
        if (isset($Detalhamento)) { $sql = $sql . " and (detalhamento = " . $this->o_db->quote($Detalhamento) . ")"; }
        if (isset($TipoMidia)) { $sql = $sql . " and (tipomidia = " . $this->o_db->quote($TipoMidia) . ")"; }
        if (isset($Dimensao)) { $sql = $sql . " and (dimensao = " . $this->o_db->quote($Dimensao) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdQuestaoModel();

                $obj_out->IdGoEdQuestao = $obj_in->IdGoEdQuestao;
                $obj_out->IdGoEdDisciplina = $obj_in->IdGoEdDisciplina;
                $obj_out->IdGoEdTema = $obj_in->IdGoEdTema;
                $obj_out->IdGoEdTopico = $obj_in->IdGoEdTopico;
                $obj_out->Descricao = $obj_in->Descricao;
                $obj_out->Detalhamento = $obj_in->Detalhamento;
                $obj_out->TipoMidia = $obj_in->TipoMidia;
                $obj_out->Dimensao = $obj_in->Dimensao;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdGoEdQuestao)
    {
        $obj = $this->objectByFields($IdGoEdQuestao, null, null, null, null, null, null, null, null);
        if ($obj) {
            $this->IdGoEdQuestao = $obj->IdGoEdQuestao;
            $this->IdGoEdDisciplina = $obj->IdGoEdDisciplina;
            $this->IdGoEdTema = $obj->IdGoEdTema;
            $this->IdGoEdTopico = $obj->IdGoEdTopico;
            $this->Descricao = $obj->Descricao;
            $this->Detalhamento = $obj->Detalhamento;
            $this->TipoMidia = $obj->TipoMidia;
            $this->Dimensao = $obj->Dimensao;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdGoEdQuestao, null, null, null, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdGoEdQuestao = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $IdGoEdTopico = null, 
        string $Descricao = null, 
        string $Detalhamento = null, 
        string $TipoMidia = null, 
        string $Dimensao = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    goedquestao
                where 1 = 1";

        if (isset($IdGoEdQuestao)) { $sql = $sql . " and (idgoedquestao = " . $this->o_db->quote($IdGoEdQuestao) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($Descricao)) { $sql = $sql . " and (descricao like " . $this->o_db->quote("%" . $Descricao. "%") . ")"; }
        if (isset($Detalhamento)) { $sql = $sql . " and (detalhamento like " . $this->o_db->quote("%" . $Detalhamento. "%") . ")"; }
        if (isset($TipoMidia)) { $sql = $sql . " and (tipomidia like " . $this->o_db->quote("%" . $TipoMidia. "%") . ")"; }
        if (isset($Dimensao)) { $sql = $sql . " and (dimensao like " . $this->o_db->quote("%" . $Dimensao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
}

?>
