<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// EtapaModel
//
// Etapas dos Cursos disponibilizados na instituição.
//
// Gerado em: 2018-03-26 05:03:18
// --------------------------------------------------------------------------------
class EtapaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdEtapa = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdEtapa;            // char(32), PK, obrigatório - Identificador da Etapa de um Curso
    private $IdInstituicao;      // char(32), PK, FK, obrigatório - Identificador da Instituição
    private $IdCurso;            // char(32), PK, FK, obrigatório - Identificador do Curso
    private $Nome;               // varchar(256), obrigatório - Nome da Etapa de um Curso
    private $Sigla;              // varchar(32), obrigatório - Sigla da Etapa de um Curso
    private $Duracao;            // varchar(256), opcional - Duração pode ser: semanal, quinzenal, mensal, semestral, anual, etc
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdEtapa") { return $this->IdEtapa; }
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "IdCurso") { return $this->IdCurso; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "Sigla") { return $this->Sigla; }
        if ($name === "Duracao") { return $this->Duracao; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdEtapa") {
            if (is_null($value)) {
                $this->IdEtapa = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdEtapa = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdEtapa;
        }
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdInstituicao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdInstituicao;
        }
        if ($name === "IdCurso") {
            if (is_null($value)) {
                $this->IdCurso = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdCurso = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdCurso;
        }
        if ($name === "Nome") {
            if (is_null($value)) {
                $this->Nome = null;
            }
            else {
                $this->Nome = substr((string) $value, 0, 256);
            }
            return $this->Nome;
        }
        if ($name === "Sigla") {
            if (is_null($value)) {
                $this->Sigla = null;
            }
            else {
                $this->Sigla = substr((string) $value, 0, 32);
            }
            return $this->Sigla;
        }
        if ($name === "Duracao") {
            if (is_null($value)) {
                $this->Duracao = null;
            }
            else {
                $this->Duracao = substr((string) $value, 0, 256);
            }
            return $this->Duracao;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        etapa
                    set 
                        idetapa = " . ( isset($this->IdEtapa) ? $this->o_db->quote($IdEtapa) : "null" ) . ", 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        idcurso = " . ( isset($this->IdCurso) ? $this->o_db->quote($IdCurso) : "null" ) . ", 
                        nome = " . ( isset($this->Nome) ? $this->o_db->quote($Nome) : "null" ) . ", 
                        sigla = " . ( isset($this->Sigla) ? $this->o_db->quote($Sigla) : "null" ) . ", 
                        duracao = " . ( isset($this->Duracao) ? $this->o_db->quote($Duracao) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idetapa" . ( isset($this->IdEtapa) ? " = " . $this->o_db->quote($this->IdEtapa) : " is null" ) . "
                        and
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and
                        idcurso" . ( isset($this->IdCurso) ? " = " . $this->o_db->quote($this->IdCurso) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        etapa (
                            idetapa, 
                            idinstituicao, 
                            idcurso, 
                            nome, 
                            sigla, 
                            duracao, 
                            status
                        )
                        values (
                            " . ( isset($this->IdEtapa) ? $this->o_db->quote($this->IdEtapa) : "null" ) . ", 
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->IdCurso) ? $this->o_db->quote($this->IdCurso) : "null" ) . ", 
                            " . ( isset($this->Nome) ? $this->o_db->quote($this->Nome) : "null" ) . ", 
                            " . ( isset($this->Sigla) ? $this->o_db->quote($this->Sigla) : "null" ) . ", 
                            " . ( isset($this->Duracao) ? $this->o_db->quote($this->Duracao) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdEtapa) && isset($this->IdInstituicao) && isset($this->IdCurso)) {
            $sql = "delete from 
                        etapa
                     where 
                        idetapa" . ( isset($this->IdEtapa) ? " = " . $this->o_db->quote($this->IdEtapa) : " is null" ) . "
                        and 
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and 
                        idcurso" . ( isset($this->IdCurso) ? " = " . $this->o_db->quote($this->IdCurso) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdEtapa = null, 
        string $IdInstituicao = null, 
        string $IdCurso = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Duracao = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idetapa as IdEtapa, 
                    idinstituicao as IdInstituicao, 
                    idcurso as IdCurso, 
                    nome as Nome, 
                    sigla as Sigla, 
                    duracao as Duracao, 
                    status as Status
                from
                    etapa
                where 1 = 1";

        if (isset($IdEtapa)) { $sql = $sql . " and (idetapa = " . $this->o_db->quote($IdEtapa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Duracao)) { $sql = $sql . " and (duracao like " . $this->o_db->quote("%" . $Duracao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_etapa = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new EtapaModel();

                $obj_out->IdEtapa = $obj_in->IdEtapa;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdCurso = $obj_in->IdCurso;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Duracao = $obj_in->Duracao;
                $obj_out->Status = $obj_in->Status;

                array_push($array_etapa, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_etapa;
    }

    // --------------------------------------------------------------------------------
    // listByIdInstituicaoIdCursoNome
    // Lista os registros com base em IdInstituicao, IdCurso, Nome
    // --------------------------------------------------------------------------------
    public function listByIdInstituicaoIdCursoNome(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdInstituicao = null, 
        string $IdCurso = null, 
        string $Nome = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdInstituicao, $IdCurso, $Nome, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // listByIdInstituicaoIdCursoSigla
    // Lista os registros com base em IdInstituicao, IdCurso, Sigla
    // --------------------------------------------------------------------------------
    public function listByIdInstituicaoIdCursoSigla(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdInstituicao = null, 
        string $IdCurso = null, 
        string $Sigla = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdInstituicao, $IdCurso, null, $Sigla, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdEtapa = null, 
        string $IdInstituicao = null, 
        string $IdCurso = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Duracao = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdEtapa) && is_null($IdInstituicao) && is_null($IdCurso)
             && is_null($Nome) && is_null($Sigla) && is_null($Duracao)
             && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idetapa as IdEtapa, 
                    idinstituicao as IdInstituicao, 
                    idcurso as IdCurso, 
                    nome as Nome, 
                    sigla as Sigla, 
                    duracao as Duracao, 
                    status as Status
                from
                    etapa
                where 1 = 1";

        if (isset($IdEtapa)) { $sql = $sql . " and (idetapa = " . $this->o_db->quote($IdEtapa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome = " . $this->o_db->quote($Nome) . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla = " . $this->o_db->quote($Sigla) . ")"; }
        if (isset($Duracao)) { $sql = $sql . " and (duracao = " . $this->o_db->quote($Duracao) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new EtapaModel();

                $obj_out->IdEtapa = $obj_in->IdEtapa;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdCurso = $obj_in->IdCurso;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Duracao = $obj_in->Duracao;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdEtapa, string $IdInstituicao, string $IdCurso)
    {
        $obj = $this->objectByFields($IdEtapa, $IdInstituicao, $IdCurso, null, null, null, null);
        if ($obj) {
            $this->IdEtapa = $obj->IdEtapa;
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->IdCurso = $obj->IdCurso;
            $this->Nome = $obj->Nome;
            $this->Sigla = $obj->Sigla;
            $this->Duracao = $obj->Duracao;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdEtapa, $this->IdInstituicao, $this->IdCurso, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdInstituicaoIdCursoNome
    // Verifica se existe um registro com IdInstituicao, IdCurso, Nome
    // --------------------------------------------------------------------------------
    public function existsIdInstituicaoIdCursoNome()
    {
        $obj = $this->objectByFields(null, $this->IdInstituicao, $this->IdCurso, $this->Nome, null, null, null);
        return !($obj && ($obj->IdEtapa === $this->IdEtapa) && ($obj->IdInstituicao === $this->IdInstituicao) && ($obj->IdCurso === $this->IdCurso));
    }

    // --------------------------------------------------------------------------------
    // existsIdInstituicaoIdCursoSigla
    // Verifica se existe um registro com IdInstituicao, IdCurso, Sigla
    // --------------------------------------------------------------------------------
    public function existsIdInstituicaoIdCursoSigla()
    {
        $obj = $this->objectByFields(null, $this->IdInstituicao, $this->IdCurso, null, $this->Sigla, null, null);
        return !($obj && ($obj->IdEtapa === $this->IdEtapa) && ($obj->IdInstituicao === $this->IdInstituicao) && ($obj->IdCurso === $this->IdCurso));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdEtapa = null, 
        string $IdInstituicao = null, 
        string $IdCurso = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Duracao = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    etapa
                where 1 = 1";

        if (isset($IdEtapa)) { $sql = $sql . " and (idetapa = " . $this->o_db->quote($IdEtapa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Duracao)) { $sql = $sql . " and (duracao like " . $this->o_db->quote("%" . $Duracao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdInstituicaoIdCursoNome
    // Conta os registros com base em IdInstituicao, IdCurso, Nome
    // --------------------------------------------------------------------------------
    public function countByIdInstituicaoIdCursoNome(
        string $IdInstituicao = null, 
        string $IdCurso = null, 
        string $Nome = null)
    {
        return $this->countBy(null, $IdInstituicao, $IdCurso, $Nome, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // countByIdInstituicaoIdCursoSigla
    // Conta os registros com base em IdInstituicao, IdCurso, Sigla
    // --------------------------------------------------------------------------------
    public function countByIdInstituicaoIdCursoSigla(
        string $IdInstituicao = null, 
        string $IdCurso = null, 
        string $Sigla = null)
    {
        return $this->countBy(null, $IdInstituicao, $IdCurso, null, $Sigla, null, null);
    }

}

?>
