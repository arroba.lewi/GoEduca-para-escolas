<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// TurmaProfessorDisciplinaModel
//
// Vinculação das turmas aos professores e disciplinas.
//
// Gerado em: 2018-03-26 05:04:11
// --------------------------------------------------------------------------------
class TurmaProfessorDisciplinaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdTurma;            // char(32), PK, FK, obrigatório - Identificador da Turma
    private $IdInstituicao;      // char(32), PK, FK, obrigatório - Identificador da Instituição
    private $IdUnidade;          // char(32), PK, FK, obrigatório - Identificador da Unidade
    private $IdCurso;            // char(32), PK, FK, obrigatório - Identificador do Curso
    private $IdProfessor;        // char(32), PK, FK, obrigatório - Identificador do Professor
    private $IdDisciplina;       // char(32), PK, FK, obrigatório - Identificador da disciplina
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdTurma") { return $this->IdTurma; }
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "IdUnidade") { return $this->IdUnidade; }
        if ($name === "IdCurso") { return $this->IdCurso; }
        if ($name === "IdProfessor") { return $this->IdProfessor; }
        if ($name === "IdDisciplina") { return $this->IdDisciplina; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdTurma") {
            if (is_null($value)) {
                $this->IdTurma = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdTurma = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdTurma;
        }
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdInstituicao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdInstituicao;
        }
        if ($name === "IdUnidade") {
            if (is_null($value)) {
                $this->IdUnidade = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdUnidade = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdUnidade;
        }
        if ($name === "IdCurso") {
            if (is_null($value)) {
                $this->IdCurso = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdCurso = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdCurso;
        }
        if ($name === "IdProfessor") {
            if (is_null($value)) {
                $this->IdProfessor = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdProfessor = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdProfessor;
        }
        if ($name === "IdDisciplina") {
            if (is_null($value)) {
                $this->IdDisciplina = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdDisciplina = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdDisciplina;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        turma_professor_disciplina
                    set 
                        idturma = " . ( isset($this->IdTurma) ? $this->o_db->quote($IdTurma) : "null" ) . ", 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        idunidade = " . ( isset($this->IdUnidade) ? $this->o_db->quote($IdUnidade) : "null" ) . ", 
                        idcurso = " . ( isset($this->IdCurso) ? $this->o_db->quote($IdCurso) : "null" ) . ", 
                        idprofessor = " . ( isset($this->IdProfessor) ? $this->o_db->quote($IdProfessor) : "null" ) . ", 
                        iddisciplina = " . ( isset($this->IdDisciplina) ? $this->o_db->quote($IdDisciplina) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idturma" . ( isset($this->IdTurma) ? " = " . $this->o_db->quote($this->IdTurma) : " is null" ) . "
                        and
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and
                        idunidade" . ( isset($this->IdUnidade) ? " = " . $this->o_db->quote($this->IdUnidade) : " is null" ) . "
                        and
                        idcurso" . ( isset($this->IdCurso) ? " = " . $this->o_db->quote($this->IdCurso) : " is null" ) . "
                        and
                        idprofessor" . ( isset($this->IdProfessor) ? " = " . $this->o_db->quote($this->IdProfessor) : " is null" ) . "
                        and
                        iddisciplina" . ( isset($this->IdDisciplina) ? " = " . $this->o_db->quote($this->IdDisciplina) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        turma_professor_disciplina (
                            idturma, 
                            idinstituicao, 
                            idunidade, 
                            idcurso, 
                            idprofessor, 
                            iddisciplina, 
                            status
                        )
                        values (
                            " . ( isset($this->IdTurma) ? $this->o_db->quote($this->IdTurma) : "null" ) . ", 
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->IdUnidade) ? $this->o_db->quote($this->IdUnidade) : "null" ) . ", 
                            " . ( isset($this->IdCurso) ? $this->o_db->quote($this->IdCurso) : "null" ) . ", 
                            " . ( isset($this->IdProfessor) ? $this->o_db->quote($this->IdProfessor) : "null" ) . ", 
                            " . ( isset($this->IdDisciplina) ? $this->o_db->quote($this->IdDisciplina) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdTurma) && isset($this->IdInstituicao) && isset($this->IdUnidade) && isset($this->IdCurso) && isset($this->IdProfessor) && isset($this->IdDisciplina)) {
            $sql = "delete from 
                        turma_professor_disciplina
                     where 
                        idturma" . ( isset($this->IdTurma) ? " = " . $this->o_db->quote($this->IdTurma) : " is null" ) . "
                        and 
                        idinstituicao" . ( isset($this->IdInstituicao) ? " = " . $this->o_db->quote($this->IdInstituicao) : " is null" ) . "
                        and 
                        idunidade" . ( isset($this->IdUnidade) ? " = " . $this->o_db->quote($this->IdUnidade) : " is null" ) . "
                        and 
                        idcurso" . ( isset($this->IdCurso) ? " = " . $this->o_db->quote($this->IdCurso) : " is null" ) . "
                        and 
                        idprofessor" . ( isset($this->IdProfessor) ? " = " . $this->o_db->quote($this->IdProfessor) : " is null" ) . "
                        and 
                        iddisciplina" . ( isset($this->IdDisciplina) ? " = " . $this->o_db->quote($this->IdDisciplina) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdTurma = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $IdCurso = null, 
        string $IdProfessor = null, 
        string $IdDisciplina = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idturma as IdTurma, 
                    idinstituicao as IdInstituicao, 
                    idunidade as IdUnidade, 
                    idcurso as IdCurso, 
                    idprofessor as IdProfessor, 
                    iddisciplina as IdDisciplina, 
                    status as Status
                from
                    turma_professor_disciplina
                where 1 = 1";

        if (isset($IdTurma)) { $sql = $sql . " and (idturma = " . $this->o_db->quote($IdTurma) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($IdProfessor)) { $sql = $sql . " and (idprofessor = " . $this->o_db->quote($IdProfessor) . ")"; }
        if (isset($IdDisciplina)) { $sql = $sql . " and (iddisciplina = " . $this->o_db->quote($IdDisciplina) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_turma_professor_disciplina = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new TurmaProfessorDisciplinaModel();

                $obj_out->IdTurma = $obj_in->IdTurma;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdUnidade = $obj_in->IdUnidade;
                $obj_out->IdCurso = $obj_in->IdCurso;
                $obj_out->IdProfessor = $obj_in->IdProfessor;
                $obj_out->IdDisciplina = $obj_in->IdDisciplina;
                $obj_out->Status = $obj_in->Status;

                array_push($array_turma_professor_disciplina, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_turma_professor_disciplina;
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdTurma = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $IdCurso = null, 
        string $IdProfessor = null, 
        string $IdDisciplina = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdTurma) && is_null($IdInstituicao) && is_null($IdUnidade)
             && is_null($IdCurso) && is_null($IdProfessor) && is_null($IdDisciplina)
             && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idturma as IdTurma, 
                    idinstituicao as IdInstituicao, 
                    idunidade as IdUnidade, 
                    idcurso as IdCurso, 
                    idprofessor as IdProfessor, 
                    iddisciplina as IdDisciplina, 
                    status as Status
                from
                    turma_professor_disciplina
                where 1 = 1";

        if (isset($IdTurma)) { $sql = $sql . " and (idturma = " . $this->o_db->quote($IdTurma) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($IdProfessor)) { $sql = $sql . " and (idprofessor = " . $this->o_db->quote($IdProfessor) . ")"; }
        if (isset($IdDisciplina)) { $sql = $sql . " and (iddisciplina = " . $this->o_db->quote($IdDisciplina) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new TurmaProfessorDisciplinaModel();

                $obj_out->IdTurma = $obj_in->IdTurma;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->IdUnidade = $obj_in->IdUnidade;
                $obj_out->IdCurso = $obj_in->IdCurso;
                $obj_out->IdProfessor = $obj_in->IdProfessor;
                $obj_out->IdDisciplina = $obj_in->IdDisciplina;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdTurma, string $IdInstituicao, string $IdUnidade, string $IdCurso, string $IdProfessor, string $IdDisciplina)
    {
        $obj = $this->objectByFields($IdTurma, $IdInstituicao, $IdUnidade, $IdCurso, $IdProfessor, $IdDisciplina, null);
        if ($obj) {
            $this->IdTurma = $obj->IdTurma;
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->IdUnidade = $obj->IdUnidade;
            $this->IdCurso = $obj->IdCurso;
            $this->IdProfessor = $obj->IdProfessor;
            $this->IdDisciplina = $obj->IdDisciplina;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdTurma, $this->IdInstituicao, $this->IdUnidade, $this->IdCurso, $this->IdProfessor, $this->IdDisciplina, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdTurma = null, 
        string $IdInstituicao = null, 
        string $IdUnidade = null, 
        string $IdCurso = null, 
        string $IdProfessor = null, 
        string $IdDisciplina = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    turma_professor_disciplina
                where 1 = 1";

        if (isset($IdTurma)) { $sql = $sql . " and (idturma = " . $this->o_db->quote($IdTurma) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($IdUnidade)) { $sql = $sql . " and (idunidade = " . $this->o_db->quote($IdUnidade) . ")"; }
        if (isset($IdCurso)) { $sql = $sql . " and (idcurso = " . $this->o_db->quote($IdCurso) . ")"; }
        if (isset($IdProfessor)) { $sql = $sql . " and (idprofessor = " . $this->o_db->quote($IdProfessor) . ")"; }
        if (isset($IdDisciplina)) { $sql = $sql . " and (iddisciplina = " . $this->o_db->quote($IdDisciplina) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
}

?>
