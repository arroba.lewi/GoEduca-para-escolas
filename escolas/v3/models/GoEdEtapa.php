<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// GoEdEtapaModel
//
// Etapas dos Cursos disponibilizados na plataforma, serão usados como sugestões para as instituições.
//
// Gerado em: 2018-03-26 05:03:28
// --------------------------------------------------------------------------------
class GoEdEtapaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdGoEdEtapa = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdGoEdEtapa;        // char(32), PK, obrigatório - Identificação da Etapa de um Curso GOEDUCA
    private $IdGoEdCurso;        // char(32), PK, FK, obrigatório - Identificação do Curso GOEDUCA
    private $Nome;               // varchar(256), obrigatório - Nome da Etapa de um Curso
    private $Sigla;              // varchar(32), obrigatório - Sigla da Etapa de um Curso
    private $Duracao;            // varchar(256), opcional - Duração pode ser: semanal, quinzenal, mensal, semestral, anual, etc
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdGoEdEtapa") { return $this->IdGoEdEtapa; }
        if ($name === "IdGoEdCurso") { return $this->IdGoEdCurso; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "Sigla") { return $this->Sigla; }
        if ($name === "Duracao") { return $this->Duracao; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdGoEdEtapa") {
            if (is_null($value)) {
                $this->IdGoEdEtapa = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdEtapa = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdEtapa;
        }
        if ($name === "IdGoEdCurso") {
            if (is_null($value)) {
                $this->IdGoEdCurso = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdCurso = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdCurso;
        }
        if ($name === "Nome") {
            if (is_null($value)) {
                $this->Nome = null;
            }
            else {
                $this->Nome = substr((string) $value, 0, 256);
            }
            return $this->Nome;
        }
        if ($name === "Sigla") {
            if (is_null($value)) {
                $this->Sigla = null;
            }
            else {
                $this->Sigla = substr((string) $value, 0, 32);
            }
            return $this->Sigla;
        }
        if ($name === "Duracao") {
            if (is_null($value)) {
                $this->Duracao = null;
            }
            else {
                $this->Duracao = substr((string) $value, 0, 256);
            }
            return $this->Duracao;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        goedetapa
                    set 
                        idgoedetapa = " . ( isset($this->IdGoEdEtapa) ? $this->o_db->quote($IdGoEdEtapa) : "null" ) . ", 
                        idgoedcurso = " . ( isset($this->IdGoEdCurso) ? $this->o_db->quote($IdGoEdCurso) : "null" ) . ", 
                        nome = " . ( isset($this->Nome) ? $this->o_db->quote($Nome) : "null" ) . ", 
                        sigla = " . ( isset($this->Sigla) ? $this->o_db->quote($Sigla) : "null" ) . ", 
                        duracao = " . ( isset($this->Duracao) ? $this->o_db->quote($Duracao) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idgoedetapa" . ( isset($this->IdGoEdEtapa) ? " = " . $this->o_db->quote($this->IdGoEdEtapa) : " is null" ) . "
                        and
                        idgoedcurso" . ( isset($this->IdGoEdCurso) ? " = " . $this->o_db->quote($this->IdGoEdCurso) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        goedetapa (
                            idgoedetapa, 
                            idgoedcurso, 
                            nome, 
                            sigla, 
                            duracao, 
                            status
                        )
                        values (
                            " . ( isset($this->IdGoEdEtapa) ? $this->o_db->quote($this->IdGoEdEtapa) : "null" ) . ", 
                            " . ( isset($this->IdGoEdCurso) ? $this->o_db->quote($this->IdGoEdCurso) : "null" ) . ", 
                            " . ( isset($this->Nome) ? $this->o_db->quote($this->Nome) : "null" ) . ", 
                            " . ( isset($this->Sigla) ? $this->o_db->quote($this->Sigla) : "null" ) . ", 
                            " . ( isset($this->Duracao) ? $this->o_db->quote($this->Duracao) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdGoEdEtapa) && isset($this->IdGoEdCurso)) {
            $sql = "delete from 
                        goedetapa
                     where 
                        idgoedetapa" . ( isset($this->IdGoEdEtapa) ? " = " . $this->o_db->quote($this->IdGoEdEtapa) : " is null" ) . "
                        and 
                        idgoedcurso" . ( isset($this->IdGoEdCurso) ? " = " . $this->o_db->quote($this->IdGoEdCurso) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdEtapa = null, 
        string $IdGoEdCurso = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Duracao = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idgoedetapa as IdGoEdEtapa, 
                    idgoedcurso as IdGoEdCurso, 
                    nome as Nome, 
                    sigla as Sigla, 
                    duracao as Duracao, 
                    status as Status
                from
                    goedetapa
                where 1 = 1";

        if (isset($IdGoEdEtapa)) { $sql = $sql . " and (idgoedetapa = " . $this->o_db->quote($IdGoEdEtapa) . ")"; }
        if (isset($IdGoEdCurso)) { $sql = $sql . " and (idgoedcurso = " . $this->o_db->quote($IdGoEdCurso) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Duracao)) { $sql = $sql . " and (duracao like " . $this->o_db->quote("%" . $Duracao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_goedetapa = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdEtapaModel();

                $obj_out->IdGoEdEtapa = $obj_in->IdGoEdEtapa;
                $obj_out->IdGoEdCurso = $obj_in->IdGoEdCurso;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Duracao = $obj_in->Duracao;
                $obj_out->Status = $obj_in->Status;

                array_push($array_goedetapa, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_goedetapa;
    }

    // --------------------------------------------------------------------------------
    // listByIdGoEdCursoNome
    // Lista os registros com base em IdGoEdCurso, Nome
    // --------------------------------------------------------------------------------
    public function listByIdGoEdCursoNome(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdCurso = null, 
        string $Nome = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdGoEdCurso, $Nome, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // listByIdGoEdCursoSigla
    // Lista os registros com base em IdGoEdCurso, Sigla
    // --------------------------------------------------------------------------------
    public function listByIdGoEdCursoSigla(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdCurso = null, 
        string $Sigla = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdGoEdCurso, null, $Sigla, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdGoEdEtapa = null, 
        string $IdGoEdCurso = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Duracao = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdGoEdEtapa) && is_null($IdGoEdCurso) && is_null($Nome)
             && is_null($Sigla) && is_null($Duracao) && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idgoedetapa as IdGoEdEtapa, 
                    idgoedcurso as IdGoEdCurso, 
                    nome as Nome, 
                    sigla as Sigla, 
                    duracao as Duracao, 
                    status as Status
                from
                    goedetapa
                where 1 = 1";

        if (isset($IdGoEdEtapa)) { $sql = $sql . " and (idgoedetapa = " . $this->o_db->quote($IdGoEdEtapa) . ")"; }
        if (isset($IdGoEdCurso)) { $sql = $sql . " and (idgoedcurso = " . $this->o_db->quote($IdGoEdCurso) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome = " . $this->o_db->quote($Nome) . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla = " . $this->o_db->quote($Sigla) . ")"; }
        if (isset($Duracao)) { $sql = $sql . " and (duracao = " . $this->o_db->quote($Duracao) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdEtapaModel();

                $obj_out->IdGoEdEtapa = $obj_in->IdGoEdEtapa;
                $obj_out->IdGoEdCurso = $obj_in->IdGoEdCurso;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Sigla = $obj_in->Sigla;
                $obj_out->Duracao = $obj_in->Duracao;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdGoEdEtapa, string $IdGoEdCurso)
    {
        $obj = $this->objectByFields($IdGoEdEtapa, $IdGoEdCurso, null, null, null, null);
        if ($obj) {
            $this->IdGoEdEtapa = $obj->IdGoEdEtapa;
            $this->IdGoEdCurso = $obj->IdGoEdCurso;
            $this->Nome = $obj->Nome;
            $this->Sigla = $obj->Sigla;
            $this->Duracao = $obj->Duracao;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdGoEdEtapa, $this->IdGoEdCurso, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdGoEdCursoNome
    // Verifica se existe um registro com IdGoEdCurso, Nome
    // --------------------------------------------------------------------------------
    public function existsIdGoEdCursoNome()
    {
        $obj = $this->objectByFields(null, $this->IdGoEdCurso, $this->Nome, null, null, null);
        return !($obj && ($obj->IdGoEdEtapa === $this->IdGoEdEtapa) && ($obj->IdGoEdCurso === $this->IdGoEdCurso));
    }

    // --------------------------------------------------------------------------------
    // existsIdGoEdCursoSigla
    // Verifica se existe um registro com IdGoEdCurso, Sigla
    // --------------------------------------------------------------------------------
    public function existsIdGoEdCursoSigla()
    {
        $obj = $this->objectByFields(null, $this->IdGoEdCurso, null, $this->Sigla, null, null);
        return !($obj && ($obj->IdGoEdEtapa === $this->IdGoEdEtapa) && ($obj->IdGoEdCurso === $this->IdGoEdCurso));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdGoEdEtapa = null, 
        string $IdGoEdCurso = null, 
        string $Nome = null, 
        string $Sigla = null, 
        string $Duracao = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    goedetapa
                where 1 = 1";

        if (isset($IdGoEdEtapa)) { $sql = $sql . " and (idgoedetapa = " . $this->o_db->quote($IdGoEdEtapa) . ")"; }
        if (isset($IdGoEdCurso)) { $sql = $sql . " and (idgoedcurso = " . $this->o_db->quote($IdGoEdCurso) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Sigla)) { $sql = $sql . " and (sigla like " . $this->o_db->quote("%" . $Sigla. "%") . ")"; }
        if (isset($Duracao)) { $sql = $sql . " and (duracao like " . $this->o_db->quote("%" . $Duracao. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdGoEdCursoNome
    // Conta os registros com base em IdGoEdCurso, Nome
    // --------------------------------------------------------------------------------
    public function countByIdGoEdCursoNome(
        string $IdGoEdCurso = null, 
        string $Nome = null)
    {
        return $this->countBy(null, $IdGoEdCurso, $Nome, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // countByIdGoEdCursoSigla
    // Conta os registros com base em IdGoEdCurso, Sigla
    // --------------------------------------------------------------------------------
    public function countByIdGoEdCursoSigla(
        string $IdGoEdCurso = null, 
        string $Sigla = null)
    {
        return $this->countBy(null, $IdGoEdCurso, null, $Sigla, null, null);
    }

}

?>
