<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// GoEdTemaModel
//
// Segmentação de temas de uma disciplina, será usado para cadastramento dos jogos.
//
// Gerado em: 2018-03-26 05:03:36
// --------------------------------------------------------------------------------
class GoEdTemaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdGoEdTema = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdGoEdTema;         // char(32), PK, obrigatório - Identificação do Tema dentro uma Disciplina GOEDUCA
    private $IdGoEdDisciplina;   // char(32), PK, FK, obrigatório - Identificação da Disciplina GOEDUCA
    private $Nome;               // varchar(256), obrigatório - Nome da Disciplina
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdGoEdTema") { return $this->IdGoEdTema; }
        if ($name === "IdGoEdDisciplina") { return $this->IdGoEdDisciplina; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdGoEdTema") {
            if (is_null($value)) {
                $this->IdGoEdTema = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdTema = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdTema;
        }
        if ($name === "IdGoEdDisciplina") {
            if (is_null($value)) {
                $this->IdGoEdDisciplina = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdDisciplina = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdDisciplina;
        }
        if ($name === "Nome") {
            if (is_null($value)) {
                $this->Nome = null;
            }
            else {
                $this->Nome = substr((string) $value, 0, 256);
            }
            return $this->Nome;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        goedtema
                    set 
                        idgoedtema = " . ( isset($this->IdGoEdTema) ? $this->o_db->quote($IdGoEdTema) : "null" ) . ", 
                        idgoeddisciplina = " . ( isset($this->IdGoEdDisciplina) ? $this->o_db->quote($IdGoEdDisciplina) : "null" ) . ", 
                        nome = " . ( isset($this->Nome) ? $this->o_db->quote($Nome) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idgoedtema" . ( isset($this->IdGoEdTema) ? " = " . $this->o_db->quote($this->IdGoEdTema) : " is null" ) . "
                        and
                        idgoeddisciplina" . ( isset($this->IdGoEdDisciplina) ? " = " . $this->o_db->quote($this->IdGoEdDisciplina) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        goedtema (
                            idgoedtema, 
                            idgoeddisciplina, 
                            nome, 
                            status
                        )
                        values (
                            " . ( isset($this->IdGoEdTema) ? $this->o_db->quote($this->IdGoEdTema) : "null" ) . ", 
                            " . ( isset($this->IdGoEdDisciplina) ? $this->o_db->quote($this->IdGoEdDisciplina) : "null" ) . ", 
                            " . ( isset($this->Nome) ? $this->o_db->quote($this->Nome) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdGoEdTema) && isset($this->IdGoEdDisciplina)) {
            $sql = "delete from 
                        goedtema
                     where 
                        idgoedtema" . ( isset($this->IdGoEdTema) ? " = " . $this->o_db->quote($this->IdGoEdTema) : " is null" ) . "
                        and 
                        idgoeddisciplina" . ( isset($this->IdGoEdDisciplina) ? " = " . $this->o_db->quote($this->IdGoEdDisciplina) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdTema = null, 
        string $IdGoEdDisciplina = null, 
        string $Nome = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idgoedtema as IdGoEdTema, 
                    idgoeddisciplina as IdGoEdDisciplina, 
                    nome as Nome, 
                    status as Status
                from
                    goedtema
                where 1 = 1";

        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_goedtema = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdTemaModel();

                $obj_out->IdGoEdTema = $obj_in->IdGoEdTema;
                $obj_out->IdGoEdDisciplina = $obj_in->IdGoEdDisciplina;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Status = $obj_in->Status;

                array_push($array_goedtema, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_goedtema;
    }

    // --------------------------------------------------------------------------------
    // listByIdGoEdDisciplinaNome
    // Lista os registros com base em IdGoEdDisciplina, Nome
    // --------------------------------------------------------------------------------
    public function listByIdGoEdDisciplinaNome(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdDisciplina = null, 
        string $Nome = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdGoEdDisciplina, $Nome, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdGoEdTema = null, 
        string $IdGoEdDisciplina = null, 
        string $Nome = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdGoEdTema) && is_null($IdGoEdDisciplina) && is_null($Nome)
             && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idgoedtema as IdGoEdTema, 
                    idgoeddisciplina as IdGoEdDisciplina, 
                    nome as Nome, 
                    status as Status
                from
                    goedtema
                where 1 = 1";

        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome = " . $this->o_db->quote($Nome) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdTemaModel();

                $obj_out->IdGoEdTema = $obj_in->IdGoEdTema;
                $obj_out->IdGoEdDisciplina = $obj_in->IdGoEdDisciplina;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdGoEdTema, string $IdGoEdDisciplina)
    {
        $obj = $this->objectByFields($IdGoEdTema, $IdGoEdDisciplina, null, null);
        if ($obj) {
            $this->IdGoEdTema = $obj->IdGoEdTema;
            $this->IdGoEdDisciplina = $obj->IdGoEdDisciplina;
            $this->Nome = $obj->Nome;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdGoEdTema, $this->IdGoEdDisciplina, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdGoEdDisciplinaNome
    // Verifica se existe um registro com IdGoEdDisciplina, Nome
    // --------------------------------------------------------------------------------
    public function existsIdGoEdDisciplinaNome()
    {
        $obj = $this->objectByFields(null, $this->IdGoEdDisciplina, $this->Nome, null);
        return !($obj && ($obj->IdGoEdTema === $this->IdGoEdTema) && ($obj->IdGoEdDisciplina === $this->IdGoEdDisciplina));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdGoEdTema = null, 
        string $IdGoEdDisciplina = null, 
        string $Nome = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    goedtema
                where 1 = 1";

        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdGoEdDisciplinaNome
    // Conta os registros com base em IdGoEdDisciplina, Nome
    // --------------------------------------------------------------------------------
    public function countByIdGoEdDisciplinaNome(
        string $IdGoEdDisciplina = null, 
        string $Nome = null)
    {
        return $this->countBy(null, $IdGoEdDisciplina, $Nome, null);
    }

}

?>
