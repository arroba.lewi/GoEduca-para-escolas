<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// GoEdTopicoModel
//
// Segmentação de tópicos dentro dos temas de uma disciplina, será usado para cadastramento dos jogos.
//
// Gerado em: 2018-03-26 05:03:38
// --------------------------------------------------------------------------------
class GoEdTopicoModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdGoEdTopico = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdGoEdTopico;       // char(32), PK, obrigatório - Identificação do Tópico de um Tema dentro uma Disciplina GOEDUCA
    private $IdGoEdDisciplina;   // char(32), PK, FK, obrigatório - Identificação da Disciplina GOEDUCA
    private $IdGoEdTema;         // char(32), PK, FK, obrigatório - Identificação do Tema dentro uma Disciplina GOEDUCA
    private $Nome;               // varchar(256), obrigatório - Nome do Tópico
    private $Status = 'AT';      // varchar(8), obrigatório - Situação do registro no BD

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdGoEdTopico") { return $this->IdGoEdTopico; }
        if ($name === "IdGoEdDisciplina") { return $this->IdGoEdDisciplina; }
        if ($name === "IdGoEdTema") { return $this->IdGoEdTema; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "Status") { return $this->Status; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdGoEdTopico") {
            if (is_null($value)) {
                $this->IdGoEdTopico = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdTopico = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdTopico;
        }
        if ($name === "IdGoEdDisciplina") {
            if (is_null($value)) {
                $this->IdGoEdDisciplina = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdDisciplina = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdDisciplina;
        }
        if ($name === "IdGoEdTema") {
            if (is_null($value)) {
                $this->IdGoEdTema = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdTema = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdTema;
        }
        if ($name === "Nome") {
            if (is_null($value)) {
                $this->Nome = null;
            }
            else {
                $this->Nome = substr((string) $value, 0, 256);
            }
            return $this->Nome;
        }
        if ($name === "Status") {
            if (is_null($value)) {
                $this->Status = null;
            }
            else {
                $this->Status = substr((string) $value, 0, 8);
            }
            return $this->Status;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        goedtopico
                    set 
                        idgoedtopico = " . ( isset($this->IdGoEdTopico) ? $this->o_db->quote($IdGoEdTopico) : "null" ) . ", 
                        idgoeddisciplina = " . ( isset($this->IdGoEdDisciplina) ? $this->o_db->quote($IdGoEdDisciplina) : "null" ) . ", 
                        idgoedtema = " . ( isset($this->IdGoEdTema) ? $this->o_db->quote($IdGoEdTema) : "null" ) . ", 
                        nome = " . ( isset($this->Nome) ? $this->o_db->quote($Nome) : "null" ) . ", 
                        status = " . ( isset($this->Status) ? $this->o_db->quote($Status) : "null" ) . "
                    where 
                        idgoedtopico" . ( isset($this->IdGoEdTopico) ? " = " . $this->o_db->quote($this->IdGoEdTopico) : " is null" ) . "
                        and
                        idgoeddisciplina" . ( isset($this->IdGoEdDisciplina) ? " = " . $this->o_db->quote($this->IdGoEdDisciplina) : " is null" ) . "
                        and
                        idgoedtema" . ( isset($this->IdGoEdTema) ? " = " . $this->o_db->quote($this->IdGoEdTema) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        goedtopico (
                            idgoedtopico, 
                            idgoeddisciplina, 
                            idgoedtema, 
                            nome, 
                            status
                        )
                        values (
                            " . ( isset($this->IdGoEdTopico) ? $this->o_db->quote($this->IdGoEdTopico) : "null" ) . ", 
                            " . ( isset($this->IdGoEdDisciplina) ? $this->o_db->quote($this->IdGoEdDisciplina) : "null" ) . ", 
                            " . ( isset($this->IdGoEdTema) ? $this->o_db->quote($this->IdGoEdTema) : "null" ) . ", 
                            " . ( isset($this->Nome) ? $this->o_db->quote($this->Nome) : "null" ) . ", 
                            " . ( isset($this->Status) ? $this->o_db->quote($this->Status) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdGoEdTopico) && isset($this->IdGoEdDisciplina) && isset($this->IdGoEdTema)) {
            $sql = "delete from 
                        goedtopico
                     where 
                        idgoedtopico" . ( isset($this->IdGoEdTopico) ? " = " . $this->o_db->quote($this->IdGoEdTopico) : " is null" ) . "
                        and 
                        idgoeddisciplina" . ( isset($this->IdGoEdDisciplina) ? " = " . $this->o_db->quote($this->IdGoEdDisciplina) : " is null" ) . "
                        and 
                        idgoedtema" . ( isset($this->IdGoEdTema) ? " = " . $this->o_db->quote($this->IdGoEdTema) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdTopico = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $Nome = null, 
        string $Status = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idgoedtopico as IdGoEdTopico, 
                    idgoeddisciplina as IdGoEdDisciplina, 
                    idgoedtema as IdGoEdTema, 
                    nome as Nome, 
                    status as Status
                from
                    goedtopico
                where 1 = 1";

        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_goedtopico = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdTopicoModel();

                $obj_out->IdGoEdTopico = $obj_in->IdGoEdTopico;
                $obj_out->IdGoEdDisciplina = $obj_in->IdGoEdDisciplina;
                $obj_out->IdGoEdTema = $obj_in->IdGoEdTema;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Status = $obj_in->Status;

                array_push($array_goedtopico, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_goedtopico;
    }

    // --------------------------------------------------------------------------------
    // listByIdGoEdDisciplinaIdGoEdTemaNome
    // Lista os registros com base em IdGoEdDisciplina, IdGoEdTema, Nome
    // --------------------------------------------------------------------------------
    public function listByIdGoEdDisciplinaIdGoEdTemaNome(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $Nome = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdGoEdDisciplina, $IdGoEdTema, $Nome, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdGoEdTopico = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $Nome = null, 
        string $Status = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdGoEdTopico) && is_null($IdGoEdDisciplina) && is_null($IdGoEdTema)
             && is_null($Nome) && is_null($Status)) {
            return null;
        }

        $sql = "select
                    idgoedtopico as IdGoEdTopico, 
                    idgoeddisciplina as IdGoEdDisciplina, 
                    idgoedtema as IdGoEdTema, 
                    nome as Nome, 
                    status as Status
                from
                    goedtopico
                where 1 = 1";

        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome = " . $this->o_db->quote($Nome) . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status = " . $this->o_db->quote($Status) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new GoEdTopicoModel();

                $obj_out->IdGoEdTopico = $obj_in->IdGoEdTopico;
                $obj_out->IdGoEdDisciplina = $obj_in->IdGoEdDisciplina;
                $obj_out->IdGoEdTema = $obj_in->IdGoEdTema;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Status = $obj_in->Status;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdGoEdTopico, string $IdGoEdDisciplina, string $IdGoEdTema)
    {
        $obj = $this->objectByFields($IdGoEdTopico, $IdGoEdDisciplina, $IdGoEdTema, null, null);
        if ($obj) {
            $this->IdGoEdTopico = $obj->IdGoEdTopico;
            $this->IdGoEdDisciplina = $obj->IdGoEdDisciplina;
            $this->IdGoEdTema = $obj->IdGoEdTema;
            $this->Nome = $obj->Nome;
            $this->Status = $obj->Status;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdGoEdTopico, $this->IdGoEdDisciplina, $this->IdGoEdTema, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdGoEdDisciplinaIdGoEdTemaNome
    // Verifica se existe um registro com IdGoEdDisciplina, IdGoEdTema, Nome
    // --------------------------------------------------------------------------------
    public function existsIdGoEdDisciplinaIdGoEdTemaNome()
    {
        $obj = $this->objectByFields(null, $this->IdGoEdDisciplina, $this->IdGoEdTema, $this->Nome, null);
        return !($obj && ($obj->IdGoEdTopico === $this->IdGoEdTopico) && ($obj->IdGoEdDisciplina === $this->IdGoEdDisciplina) && ($obj->IdGoEdTema === $this->IdGoEdTema));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdGoEdTopico = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $Nome = null, 
        string $Status = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    goedtopico
                where 1 = 1";

        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Status)) { $sql = $sql . " and (status like " . $this->o_db->quote("%" . $Status. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdGoEdDisciplinaIdGoEdTemaNome
    // Conta os registros com base em IdGoEdDisciplina, IdGoEdTema, Nome
    // --------------------------------------------------------------------------------
    public function countByIdGoEdDisciplinaIdGoEdTemaNome(
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $Nome = null)
    {
        return $this->countBy(null, $IdGoEdDisciplina, $IdGoEdTema, $Nome, null);
    }

}

?>
