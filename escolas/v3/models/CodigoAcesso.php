<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// CodigoAcessoModel
//
// Gera o código de acesso único por pessoa.
// 
// Atenção:
// * Usuários GOEDUCA terã o IDINSTITUICAO com o valor.
//
// Gerado em: 2018-03-26 05:03:06
// --------------------------------------------------------------------------------
class CodigoAcessoModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdCodigoAcesso;     // bigint(20), PK, AUTO-INCREMENTO, obrigatório - Código de Acesso Sequencial
    private $IdPessoa;           // char(32), FK, obrigatório - Identificador da Pessoa
    private $IdInstituicao;      // char(32), FK, opcional - Identificador da Instituição
    private $Perfil = 'Pessoa';  // varchar(32), obrigatório - Perfil para a pessoa junto a instituição
    private $CodigoAcesso;       // varchar(32), opcional - Representação reduzida alfanumérica do Código de Acesso
    private $Senha;              // varchar(256), opcional - Senha criptografada

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdCodigoAcesso") { return $this->IdCodigoAcesso; }
        if ($name === "IdPessoa") { return $this->IdPessoa; }
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "Perfil") { return $this->Perfil; }
        if ($name === "CodigoAcesso") { return $this->CodigoAcesso; }
        if ($name === "Senha") { return $this->Senha; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdCodigoAcesso") {
            if (is_null($value)) {
                $this->IdCodigoAcesso = null;
            }
            else {
                if (is_int($value)) {
                    $this->IdCodigoAcesso = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->IdCodigoAcesso;
        }
        if ($name === "IdPessoa") {
            if (is_null($value)) {
                $this->IdPessoa = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdPessoa = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdPessoa;
        }
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdInstituicao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdInstituicao;
        }
        if ($name === "Perfil") {
            if (is_null($value)) {
                $this->Perfil = null;
            }
            else {
                $this->Perfil = substr((string) $value, 0, 32);
            }
            return $this->Perfil;
        }
        if ($name === "CodigoAcesso") {
            if (is_null($value)) {
                $this->CodigoAcesso = null;
            }
            else {
                $this->CodigoAcesso = substr((string) $value, 0, 32);
            }
            return $this->CodigoAcesso;
        }
        if ($name === "Senha") {
            if (is_null($value)) {
                $this->Senha = null;
            }
            else {
                $this->Senha = substr((string) $value, 0, 256);
            }
            return $this->Senha;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        codigoacesso
                    set 
                        idcodigoacesso = " . ( isset($this->IdCodigoAcesso) ? $this->IdCodigoAcesso : "null" ) . ", 
                        idpessoa = " . ( isset($this->IdPessoa) ? $this->o_db->quote($IdPessoa) : "null" ) . ", 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        perfil = " . ( isset($this->Perfil) ? $this->o_db->quote($Perfil) : "null" ) . ", 
                        codigoacesso = " . ( isset($this->CodigoAcesso) ? $this->o_db->quote($CodigoAcesso) : "null" ) . ", 
                        senha = " . ( isset($this->Senha) ? $this->o_db->quote($Senha) : "null" ) . "
                    where 
                        idcodigoacesso" . ( isset($this->IdCodigoAcesso) ? " = " . $this->IdCodigoAcesso : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        codigoacesso (
                            idcodigoacesso, 
                            idpessoa, 
                            idinstituicao, 
                            perfil, 
                            codigoacesso, 
                            senha
                        )
                        values (
                            null, 
                            " . ( isset($this->IdPessoa) ? $this->o_db->quote($this->IdPessoa) : "null" ) . ", 
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->Perfil) ? $this->o_db->quote($this->Perfil) : "null" ) . ", 
                            " . ( isset($this->CodigoAcesso) ? $this->o_db->quote($this->CodigoAcesso) : "null" ) . ", 
                            " . ( isset($this->Senha) ? $this->o_db->quote($this->Senha) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            if (!$regexists) {
                    $this->IdCodigoAcesso = $this->o_db->lastInsertId();
            }
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdCodigoAcesso)) {
            $sql = "delete from 
                        codigoacesso
                     where 
                        idcodigoacesso" . ( isset($this->IdCodigoAcesso) ? " = " . $this->o_db->quote($this->IdCodigoAcesso) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        int $IdCodigoAcesso = null, 
        string $IdPessoa = null, 
        string $IdInstituicao = null, 
        string $Perfil = null, 
        string $CodigoAcesso = null, 
        string $Senha = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idcodigoacesso as IdCodigoAcesso, 
                    idpessoa as IdPessoa, 
                    idinstituicao as IdInstituicao, 
                    perfil as Perfil, 
                    codigoacesso as CodigoAcesso, 
                    senha as Senha
                from
                    codigoacesso
                where 1 = 1";

        if (isset($IdCodigoAcesso)) { $sql = $sql . " and (idcodigoacesso = " . $IdCodigoAcesso . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($Perfil)) { $sql = $sql . " and (perfil like " . $this->o_db->quote("%" . $Perfil. "%") . ")"; }
        if (isset($CodigoAcesso)) { $sql = $sql . " and (codigoacesso like " . $this->o_db->quote("%" . $CodigoAcesso. "%") . ")"; }
        if (isset($Senha)) { $sql = $sql . " and (senha like " . $this->o_db->quote("%" . $Senha. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_codigoacesso = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new CodigoAcessoModel();

                $obj_out->IdCodigoAcesso = $obj_in->IdCodigoAcesso;
                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->Perfil = $obj_in->Perfil;
                $obj_out->CodigoAcesso = $obj_in->CodigoAcesso;
                $obj_out->Senha = $obj_in->Senha;

                array_push($array_codigoacesso, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_codigoacesso;
    }

    // --------------------------------------------------------------------------------
    // listByIdPessoaIdInstituicaoPerfil
    // Lista os registros com base em IdPessoa, IdInstituicao, Perfil
    // --------------------------------------------------------------------------------
    public function listByIdPessoaIdInstituicaoPerfil(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdPessoa = null, 
        string $IdInstituicao = null, 
        string $Perfil = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdPessoa, $IdInstituicao, $Perfil, null, null);
    }

    // --------------------------------------------------------------------------------
    // listByCodigoAcesso
    // Lista os registros com base em CodigoAcesso
    // --------------------------------------------------------------------------------
    public function listByCodigoAcesso(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $CodigoAcesso = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, null, null, null, $CodigoAcesso, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        int $IdCodigoAcesso = null, 
        string $IdPessoa = null, 
        string $IdInstituicao = null, 
        string $Perfil = null, 
        string $CodigoAcesso = null, 
        string $Senha = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdCodigoAcesso) && is_null($IdPessoa) && is_null($IdInstituicao)
             && is_null($Perfil) && is_null($CodigoAcesso) && is_null($Senha)) {
            return null;
        }

        $sql = "select
                    idcodigoacesso as IdCodigoAcesso, 
                    idpessoa as IdPessoa, 
                    idinstituicao as IdInstituicao, 
                    perfil as Perfil, 
                    codigoacesso as CodigoAcesso, 
                    senha as Senha
                from
                    codigoacesso
                where 1 = 1";

        if (isset($IdCodigoAcesso)) { $sql = $sql . " and (idcodigoacesso = " . $IdCodigoAcesso . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($Perfil)) { $sql = $sql . " and (perfil = " . $this->o_db->quote($Perfil) . ")"; }
        if (isset($CodigoAcesso)) { $sql = $sql . " and (codigoacesso = " . $this->o_db->quote($CodigoAcesso) . ")"; }
        if (isset($Senha)) { $sql = $sql . " and (senha = " . $this->o_db->quote($Senha) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new CodigoAcessoModel();

                $obj_out->IdCodigoAcesso = $obj_in->IdCodigoAcesso;
                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->Perfil = $obj_in->Perfil;
                $obj_out->CodigoAcesso = $obj_in->CodigoAcesso;
                $obj_out->Senha = $obj_in->Senha;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(int $IdCodigoAcesso)
    {
        $obj = $this->objectByFields($IdCodigoAcesso, null, null, null, null, null);
        if ($obj) {
            $this->IdCodigoAcesso = $obj->IdCodigoAcesso;
            $this->IdPessoa = $obj->IdPessoa;
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->Perfil = $obj->Perfil;
            $this->CodigoAcesso = $obj->CodigoAcesso;
            $this->Senha = $obj->Senha;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdCodigoAcesso, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdPessoaIdInstituicaoPerfil
    // Verifica se existe um registro com IdPessoa, IdInstituicao, Perfil
    // --------------------------------------------------------------------------------
    public function existsIdPessoaIdInstituicaoPerfil()
    {
        $obj = $this->objectByFields(null, $this->IdPessoa, $this->IdInstituicao, $this->Perfil, null, null);
        return !($obj && ($obj->IdCodigoAcesso === $this->IdCodigoAcesso));
    }

    // --------------------------------------------------------------------------------
    // existsCodigoAcesso
    // Verifica se existe um registro com CodigoAcesso
    // --------------------------------------------------------------------------------
    public function existsCodigoAcesso()
    {
        $obj = $this->objectByFields(null, null, null, null, $this->CodigoAcesso, null);
        return !($obj && ($obj->IdCodigoAcesso === $this->IdCodigoAcesso));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        int $IdCodigoAcesso = null, 
        string $IdPessoa = null, 
        string $IdInstituicao = null, 
        string $Perfil = null, 
        string $CodigoAcesso = null, 
        string $Senha = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    codigoacesso
                where 1 = 1";

        if (isset($IdCodigoAcesso)) { $sql = $sql . " and (idcodigoacesso = " . IdCodigoAcesso . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($Perfil)) { $sql = $sql . " and (perfil like " . $this->o_db->quote("%" . $Perfil. "%") . ")"; }
        if (isset($CodigoAcesso)) { $sql = $sql . " and (codigoacesso like " . $this->o_db->quote("%" . $CodigoAcesso. "%") . ")"; }
        if (isset($Senha)) { $sql = $sql . " and (senha like " . $this->o_db->quote("%" . $Senha. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdPessoaIdInstituicaoPerfil
    // Conta os registros com base em IdPessoa, IdInstituicao, Perfil
    // --------------------------------------------------------------------------------
    public function countByIdPessoaIdInstituicaoPerfil(
        string $IdPessoa = null, 
        string $IdInstituicao = null, 
        string $Perfil = null)
    {
        return $this->countBy(null, $IdPessoa, $IdInstituicao, $Perfil, null, null);
    }

    // --------------------------------------------------------------------------------
    // countByCodigoAcesso
    // Conta os registros com base em CodigoAcesso
    // --------------------------------------------------------------------------------
    public function countByCodigoAcesso(
        string $CodigoAcesso = null)
    {
        return $this->countBy(null, null, null, null, $CodigoAcesso, null);
    }

}

?>
