<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// PartidaModel
//
// Registra os dados de cada partida para os usuários da plataforma.
//
// Gerado em: 2018-03-26 05:03:56
// --------------------------------------------------------------------------------
class PartidaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdPartida = md5(uniqid(rand(), true));
        $this->DataHoraPartida = DateTime::createFromFormat("d-m-Y H:i:s", date("d-m-Y H:i:s"));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdPartida;          // char(32), PK, obrigatório - Identificação da Partida
    private $IdJogo;             // char(32), FK, obrigatório - Identificação do Jogo
    private $IdPessoa;           // char(32), FK, obrigatório - Identificador da Pessoa
    private $IdInstituicao;      // char(32), FK, obrigatório - Identificador da Instituição
    private $CodigoAcesso;       // varchar(32), obrigatório - Representação reduzida alfanumérica do Código de Acesso
    private $IdGoEdDisciplina;   // char(32), FK, opcional - Identificação da Disciplina GOEDUCA
    private $IdGoEdTema;         // char(32), FK, opcional - Identificação do Tema dentro uma Disciplina GOEDUCA
    private $IdGoEdTopico;       // char(32), FK, opcional - Identificação do Tópico de um Tema dentro uma Disciplina GOEDUCA
    private $DataHoraPartida;    // timestamp, obrigatório - Data e hora que ocorreu a partida
    private $DuracaoSegundos;    // bigint(20), opcional - Duração em minutos da partida
    private $Pontos;             // bigint(20), opcional - Quantidade de pontos da partida
    private $QtdQuestoes;        // int(11), opcional - Quantidade de questões
    private $QtdAcertos;         // int(11), opcional - Quantidade de acertos
    private $Desempenho;         // decimal(18,2), opcional - Desempenho percentual, quantidade de acertos em relação a quantidade de questões
    private $DadosPartida;       // json, opcional - Dados gerais da partida em formato JSON

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdPartida") { return $this->IdPartida; }
        if ($name === "IdJogo") { return $this->IdJogo; }
        if ($name === "IdPessoa") { return $this->IdPessoa; }
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "CodigoAcesso") { return $this->CodigoAcesso; }
        if ($name === "IdGoEdDisciplina") { return $this->IdGoEdDisciplina; }
        if ($name === "IdGoEdTema") { return $this->IdGoEdTema; }
        if ($name === "IdGoEdTopico") { return $this->IdGoEdTopico; }
        if ($name === "DataHoraPartida") { return ($this->DataHoraPartida ? $this->DataHoraPartida->format("d-m-Y H:i:s") : null); }
        if ($name === "DuracaoSegundos") { return $this->DuracaoSegundos; }
        if ($name === "Pontos") { return $this->Pontos; }
        if ($name === "QtdQuestoes") { return $this->QtdQuestoes; }
        if ($name === "QtdAcertos") { return $this->QtdAcertos; }
        if ($name === "Desempenho") { return $this->Desempenho; }
        if ($name === "DadosPartida") { return $this->DadosPartida; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdPartida") {
            if (is_null($value)) {
                $this->IdPartida = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdPartida = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdPartida;
        }
        if ($name === "IdJogo") {
            if (is_null($value)) {
                $this->IdJogo = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdJogo = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdJogo;
        }
        if ($name === "IdPessoa") {
            if (is_null($value)) {
                $this->IdPessoa = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdPessoa = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdPessoa;
        }
        if ($name === "IdInstituicao") {
            if (is_null($value)) {
                $this->IdInstituicao = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdInstituicao = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdInstituicao;
        }
        if ($name === "CodigoAcesso") {
            if (is_null($value)) {
                $this->CodigoAcesso = null;
            }
            else {
                $this->CodigoAcesso = substr((string) $value, 0, 32);
            }
            return $this->CodigoAcesso;
        }
        if ($name === "IdGoEdDisciplina") {
            if (is_null($value)) {
                $this->IdGoEdDisciplina = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdDisciplina = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdDisciplina;
        }
        if ($name === "IdGoEdTema") {
            if (is_null($value)) {
                $this->IdGoEdTema = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdTema = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdTema;
        }
        if ($name === "IdGoEdTopico") {
            if (is_null($value)) {
                $this->IdGoEdTopico = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdGoEdTopico = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdGoEdTopico;
        }
        if ($name === "DataHoraPartida") {
            if (is_null($value)) {
                $this->DataHoraPartida = null;
            }
            else {
                if (isset($value) && DateTime::createFromFormat("d-m-Y H:i:s", $value)) {
                    $this->DataHoraPartida = DateTime::createFromFormat("d-m-Y H:i:s", $value);
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo TIMESTAMP(dd-mm-aaaa hh:mi:ss) inválido.");
                }
            }
            return $this->DataHoraPartida->format("d-m-Y H:i:s");
        }
        if ($name === "DuracaoSegundos") {
            if (is_null($value)) {
                $this->DuracaoSegundos = null;
            }
            else {
                if (is_int($value)) {
                    $this->DuracaoSegundos = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->DuracaoSegundos;
        }
        if ($name === "Pontos") {
            if (is_null($value)) {
                $this->Pontos = null;
            }
            else {
                if (is_int($value)) {
                    $this->Pontos = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->Pontos;
        }
        if ($name === "QtdQuestoes") {
            if (is_null($value)) {
                $this->QtdQuestoes = null;
            }
            else {
                if (is_int($value)) {
                    $this->QtdQuestoes = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->QtdQuestoes;
        }
        if ($name === "QtdAcertos") {
            if (is_null($value)) {
                $this->QtdAcertos = null;
            }
            else {
                if (is_int($value)) {
                    $this->QtdAcertos = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo INT inválido.");
                }
            }
            return $this->QtdAcertos;
        }
        if ($name === "Desempenho") {
            if (is_null($value)) {
                $this->Desempenho = null;
            }
            else {
                if (is_float($value)) {
                    $this->Desempenho = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo FLOAT inválido.");
                }
            }
            return $this->Desempenho;
        }
        if ($name === "DadosPartida") {
            if (is_null($value)) {
                $this->DadosPartida = null;
            }
            else {
                $this->DadosPartida = $value;
            }
            return $this->DadosPartida;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        partida
                    set 
                        idpartida = " . ( isset($this->IdPartida) ? $this->o_db->quote($IdPartida) : "null" ) . ", 
                        idjogo = " . ( isset($this->IdJogo) ? $this->o_db->quote($IdJogo) : "null" ) . ", 
                        idpessoa = " . ( isset($this->IdPessoa) ? $this->o_db->quote($IdPessoa) : "null" ) . ", 
                        idinstituicao = " . ( isset($this->IdInstituicao) ? $this->o_db->quote($IdInstituicao) : "null" ) . ", 
                        codigoacesso = " . ( isset($this->CodigoAcesso) ? $this->o_db->quote($CodigoAcesso) : "null" ) . ", 
                        idgoeddisciplina = " . ( isset($this->IdGoEdDisciplina) ? $this->o_db->quote($IdGoEdDisciplina) : "null" ) . ", 
                        idgoedtema = " . ( isset($this->IdGoEdTema) ? $this->o_db->quote($IdGoEdTema) : "null" ) . ", 
                        idgoedtopico = " . ( isset($this->IdGoEdTopico) ? $this->o_db->quote($IdGoEdTopico) : "null" ) . ", 
                        datahorapartida = " . ( isset($this->DataHoraPartida) ? $this->DataHoraPartida->format("Y-m-d H:i:s") : "null" ) . ", 
                        duracaosegundos = " . ( isset($this->DuracaoSegundos) ? $this->DuracaoSegundos : "null" ) . ", 
                        pontos = " . ( isset($this->Pontos) ? $this->Pontos : "null" ) . ", 
                        qtdquestoes = " . ( isset($this->QtdQuestoes) ? $this->QtdQuestoes : "null" ) . ", 
                        qtdacertos = " . ( isset($this->QtdAcertos) ? $this->QtdAcertos : "null" ) . ", 
                        desempenho = " . ( isset($this->Desempenho) ? $this->Desempenho : "null" ) . ", 
                        dadospartida = " . ( isset($this->DadosPartida) ? $this->o_db->quote($DadosPartida) : "null" ) . "
                    where 
                        idpartida" . ( isset($this->IdPartida) ? " = " . $this->o_db->quote($this->IdPartida) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        partida (
                            idpartida, 
                            idjogo, 
                            idpessoa, 
                            idinstituicao, 
                            codigoacesso, 
                            idgoeddisciplina, 
                            idgoedtema, 
                            idgoedtopico, 
                            datahorapartida, 
                            duracaosegundos, 
                            pontos, 
                            qtdquestoes, 
                            qtdacertos, 
                            desempenho, 
                            dadospartida
                        )
                        values (
                            " . ( isset($this->IdPartida) ? $this->o_db->quote($this->IdPartida) : "null" ) . ", 
                            " . ( isset($this->IdJogo) ? $this->o_db->quote($this->IdJogo) : "null" ) . ", 
                            " . ( isset($this->IdPessoa) ? $this->o_db->quote($this->IdPessoa) : "null" ) . ", 
                            " . ( isset($this->IdInstituicao) ? $this->o_db->quote($this->IdInstituicao) : "null" ) . ", 
                            " . ( isset($this->CodigoAcesso) ? $this->o_db->quote($this->CodigoAcesso) : "null" ) . ", 
                            " . ( isset($this->IdGoEdDisciplina) ? $this->o_db->quote($this->IdGoEdDisciplina) : "null" ) . ", 
                            " . ( isset($this->IdGoEdTema) ? $this->o_db->quote($this->IdGoEdTema) : "null" ) . ", 
                            " . ( isset($this->IdGoEdTopico) ? $this->o_db->quote($this->IdGoEdTopico) : "null" ) . ", 
                            " . ( isset($this->DataHoraPartida) ? $this->o_db->quote($this->DataHoraPartida->format("Y-m-d H:i:s")) : "null" ) . ", 
                            " . ( isset($this->DuracaoSegundos) ? $this->DuracaoSegundos : "null" ) . ", 
                            " . ( isset($this->Pontos) ? $this->Pontos : "null" ) . ", 
                            " . ( isset($this->QtdQuestoes) ? $this->QtdQuestoes : "null" ) . ", 
                            " . ( isset($this->QtdAcertos) ? $this->QtdAcertos : "null" ) . ", 
                            " . ( isset($this->Desempenho) ? $this->Desempenho : "null" ) . ", 
                            " . ( isset($this->DadosPartida) ? $this->o_db->quote($this->DadosPartida) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdPartida)) {
            $sql = "delete from 
                        partida
                     where 
                        idpartida" . ( isset($this->IdPartida) ? " = " . $this->o_db->quote($this->IdPartida) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdPartida = null, 
        string $IdJogo = null, 
        string $IdPessoa = null, 
        string $IdInstituicao = null, 
        string $CodigoAcesso = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $IdGoEdTopico = null, 
        string $DataHoraPartida = null, 
        int $DuracaoSegundos = null, 
        int $Pontos = null, 
        int $QtdQuestoes = null, 
        int $QtdAcertos = null, 
        float $Desempenho = null, 
        string $DadosPartida = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idpartida as IdPartida, 
                    idjogo as IdJogo, 
                    idpessoa as IdPessoa, 
                    idinstituicao as IdInstituicao, 
                    codigoacesso as CodigoAcesso, 
                    idgoeddisciplina as IdGoEdDisciplina, 
                    idgoedtema as IdGoEdTema, 
                    idgoedtopico as IdGoEdTopico, 
                    datahorapartida as DataHoraPartida, 
                    duracaosegundos as DuracaoSegundos, 
                    pontos as Pontos, 
                    qtdquestoes as QtdQuestoes, 
                    qtdacertos as QtdAcertos, 
                    desempenho as Desempenho, 
                    dadospartida as DadosPartida
                from
                    partida
                where 1 = 1";

        if (isset($IdPartida)) { $sql = $sql . " and (idpartida = " . $this->o_db->quote($IdPartida) . ")"; }
        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($CodigoAcesso)) { $sql = $sql . " and (codigoacesso like " . $this->o_db->quote("%" . $CodigoAcesso. "%") . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($DataHoraPartida)) { $sql = $sql . " and (datahorapartida = " . $this->o_db->quote(isset($DataHoraPartida) ? DateTime::createFromFormat("Y-m-d H:i:s", $DataHoraPartida)->format("Y-m-d H:i:s") : "") . ")"; }
        if (isset($DuracaoSegundos)) { $sql = $sql . " and (duracaosegundos = " . $DuracaoSegundos . ")"; }
        if (isset($Pontos)) { $sql = $sql . " and (pontos = " . $Pontos . ")"; }
        if (isset($QtdQuestoes)) { $sql = $sql . " and (qtdquestoes = " . $QtdQuestoes . ")"; }
        if (isset($QtdAcertos)) { $sql = $sql . " and (qtdacertos = " . $QtdAcertos . ")"; }
        if (isset($Desempenho)) { $sql = $sql . " and (desempenho = " . $Desempenho . ")"; }
        if (isset($DadosPartida)) { $sql = $sql . " and (dadospartida = " . $this->o_db->quote($DadosPartida) . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_partida = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new PartidaModel();

                $obj_out->IdPartida = $obj_in->IdPartida;
                $obj_out->IdJogo = $obj_in->IdJogo;
                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->CodigoAcesso = $obj_in->CodigoAcesso;
                $obj_out->IdGoEdDisciplina = $obj_in->IdGoEdDisciplina;
                $obj_out->IdGoEdTema = $obj_in->IdGoEdTema;
                $obj_out->IdGoEdTopico = $obj_in->IdGoEdTopico;
                $obj_out->DataHoraPartida = $obj_in->DataHoraPartida;
                $obj_out->DuracaoSegundos = $obj_in->DuracaoSegundos;
                $obj_out->Pontos = $obj_in->Pontos;
                $obj_out->QtdQuestoes = $obj_in->QtdQuestoes;
                $obj_out->QtdAcertos = $obj_in->QtdAcertos;
                $obj_out->Desempenho = $obj_in->Desempenho;
                $obj_out->DadosPartida = $obj_in->DadosPartida;

                array_push($array_partida, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_partida;
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdPartida = null, 
        string $IdJogo = null, 
        string $IdPessoa = null, 
        string $IdInstituicao = null, 
        string $CodigoAcesso = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $IdGoEdTopico = null, 
        string $DataHoraPartida = null, 
        int $DuracaoSegundos = null, 
        int $Pontos = null, 
        int $QtdQuestoes = null, 
        int $QtdAcertos = null, 
        float $Desempenho = null, 
        string $DadosPartida = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdPartida) && is_null($IdJogo) && is_null($IdPessoa)
             && is_null($IdInstituicao) && is_null($CodigoAcesso) && is_null($IdGoEdDisciplina)
             && is_null($IdGoEdTema) && is_null($IdGoEdTopico) && is_null($DataHoraPartida)
             && is_null($DuracaoSegundos) && is_null($Pontos) && is_null($QtdQuestoes)
             && is_null($QtdAcertos) && is_null($Desempenho) && is_null($DadosPartida)) {
            return null;
        }

        $sql = "select
                    idpartida as IdPartida, 
                    idjogo as IdJogo, 
                    idpessoa as IdPessoa, 
                    idinstituicao as IdInstituicao, 
                    codigoacesso as CodigoAcesso, 
                    idgoeddisciplina as IdGoEdDisciplina, 
                    idgoedtema as IdGoEdTema, 
                    idgoedtopico as IdGoEdTopico, 
                    datahorapartida as DataHoraPartida, 
                    duracaosegundos as DuracaoSegundos, 
                    pontos as Pontos, 
                    qtdquestoes as QtdQuestoes, 
                    qtdacertos as QtdAcertos, 
                    desempenho as Desempenho, 
                    dadospartida as DadosPartida
                from
                    partida
                where 1 = 1";

        if (isset($IdPartida)) { $sql = $sql . " and (idpartida = " . $this->o_db->quote($IdPartida) . ")"; }
        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($CodigoAcesso)) { $sql = $sql . " and (codigoacesso = " . $this->o_db->quote($CodigoAcesso) . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($DataHoraPartida)) { $sql = $sql . " and (datahorapartida = " . $this->o_db->quote(isset($DataHoraPartida) ? DateTime::createFromFormat("Y-m-d H:i:s", $DataHoraPartida)->format("Y-m-d H:i:s") : "") . ")"; }
        if (isset($DuracaoSegundos)) { $sql = $sql . " and (duracaosegundos = " . $DuracaoSegundos . ")"; }
        if (isset($Pontos)) { $sql = $sql . " and (pontos = " . $Pontos . ")"; }
        if (isset($QtdQuestoes)) { $sql = $sql . " and (qtdquestoes = " . $QtdQuestoes . ")"; }
        if (isset($QtdAcertos)) { $sql = $sql . " and (qtdacertos = " . $QtdAcertos . ")"; }
        if (isset($Desempenho)) { $sql = $sql . " and (desempenho = " . $Desempenho . ")"; }
        if (isset($DadosPartida)) { $sql = $sql . " and (dadospartida = " . $this->o_db->quote($DadosPartida) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new PartidaModel();

                $obj_out->IdPartida = $obj_in->IdPartida;
                $obj_out->IdJogo = $obj_in->IdJogo;
                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->IdInstituicao = $obj_in->IdInstituicao;
                $obj_out->CodigoAcesso = $obj_in->CodigoAcesso;
                $obj_out->IdGoEdDisciplina = $obj_in->IdGoEdDisciplina;
                $obj_out->IdGoEdTema = $obj_in->IdGoEdTema;
                $obj_out->IdGoEdTopico = $obj_in->IdGoEdTopico;
                $obj_out->DataHoraPartida = $obj_in->DataHoraPartida;
                $obj_out->DuracaoSegundos = $obj_in->DuracaoSegundos;
                $obj_out->Pontos = $obj_in->Pontos;
                $obj_out->QtdQuestoes = $obj_in->QtdQuestoes;
                $obj_out->QtdAcertos = $obj_in->QtdAcertos;
                $obj_out->Desempenho = $obj_in->Desempenho;
                $obj_out->DadosPartida = $obj_in->DadosPartida;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdPartida)
    {
        $obj = $this->objectByFields($IdPartida, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        if ($obj) {
            $this->IdPartida = $obj->IdPartida;
            $this->IdJogo = $obj->IdJogo;
            $this->IdPessoa = $obj->IdPessoa;
            $this->IdInstituicao = $obj->IdInstituicao;
            $this->CodigoAcesso = $obj->CodigoAcesso;
            $this->IdGoEdDisciplina = $obj->IdGoEdDisciplina;
            $this->IdGoEdTema = $obj->IdGoEdTema;
            $this->IdGoEdTopico = $obj->IdGoEdTopico;
            $this->DataHoraPartida = $obj->DataHoraPartida;
            $this->DuracaoSegundos = $obj->DuracaoSegundos;
            $this->Pontos = $obj->Pontos;
            $this->QtdQuestoes = $obj->QtdQuestoes;
            $this->QtdAcertos = $obj->QtdAcertos;
            $this->Desempenho = $obj->Desempenho;
            $this->DadosPartida = $obj->DadosPartida;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdPartida, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdPartida = null, 
        string $IdJogo = null, 
        string $IdPessoa = null, 
        string $IdInstituicao = null, 
        string $CodigoAcesso = null, 
        string $IdGoEdDisciplina = null, 
        string $IdGoEdTema = null, 
        string $IdGoEdTopico = null, 
        string $DataHoraPartida = null, 
        int $DuracaoSegundos = null, 
        int $Pontos = null, 
        int $QtdQuestoes = null, 
        int $QtdAcertos = null, 
        float $Desempenho = null, 
        string $DadosPartida = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    partida
                where 1 = 1";

        if (isset($IdPartida)) { $sql = $sql . " and (idpartida = " . $this->o_db->quote($IdPartida) . ")"; }
        if (isset($IdJogo)) { $sql = $sql . " and (idjogo = " . $this->o_db->quote($IdJogo) . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($IdInstituicao)) { $sql = $sql . " and (idinstituicao = " . $this->o_db->quote($IdInstituicao) . ")"; }
        if (isset($CodigoAcesso)) { $sql = $sql . " and (codigoacesso like " . $this->o_db->quote("%" . $CodigoAcesso. "%") . ")"; }
        if (isset($IdGoEdDisciplina)) { $sql = $sql . " and (idgoeddisciplina = " . $this->o_db->quote($IdGoEdDisciplina) . ")"; }
        if (isset($IdGoEdTema)) { $sql = $sql . " and (idgoedtema = " . $this->o_db->quote($IdGoEdTema) . ")"; }
        if (isset($IdGoEdTopico)) { $sql = $sql . " and (idgoedtopico = " . $this->o_db->quote($IdGoEdTopico) . ")"; }
        if (isset($DataHoraPartida)) { $sql = $sql . " and (datahorapartida = " . $this->o_db->quote(DateTime::createFromFormat("Y-m-d H:i:s", $DataHoraPartida) ? DateTime::createFromFormat("Y-m-d H:i:s", $DataHoraPartida)->format("Y-m-d H:i:s") : "") . ")"; }
        if (isset($DuracaoSegundos)) { $sql = $sql . " and (duracaosegundos = " . DuracaoSegundos . ")"; }
        if (isset($Pontos)) { $sql = $sql . " and (pontos = " . Pontos . ")"; }
        if (isset($QtdQuestoes)) { $sql = $sql . " and (qtdquestoes = " . QtdQuestoes . ")"; }
        if (isset($QtdAcertos)) { $sql = $sql . " and (qtdacertos = " . QtdAcertos . ")"; }
        if (isset($Desempenho)) { $sql = $sql . " and (desempenho = " . Desempenho . ")"; }
        if (isset($DadosPartida)) { $sql = $sql . " and (dadospartida = " . $this->o_db->quote($DadosPartida) . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
}

?>
