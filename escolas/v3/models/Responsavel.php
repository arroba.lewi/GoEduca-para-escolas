<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// ResponsavelModel
//
// Responsáveis legais das pessoas (alunos) cadastrados na plataforma.
//
// Gerado em: 2018-03-26 05:04:03
// --------------------------------------------------------------------------------
class ResponsavelModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe é criada
    function __construct() {
        parent::__construct();
        $this->IdResponsavel = md5(uniqid(rand(), true));
    }

    // --------------------------------------------------------------------------------
    // Propriedades privadas do objeto
    // --------------------------------------------------------------------------------
    private $IdResponsavel;      // char(32), PK, obrigatório - Identificador do Responsável
    private $IdPessoa;           // char(32), FK, obrigatório - Identificador da Pessoa
    private $Nome;               // varchar(256), obrigatório - Nome do Responsável
    private $Telefone;           // varchar(256), opcional - Telefone
    private $Email;              // varchar(256), opcional - Endereço e-mail
    private $GrauParentesco;     // varchar(64), opcional - Grau de parentesco com a Pessoa
    private $Observacao;         // text, opcional - Observações sobre o responsável

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdResponsavel") { return $this->IdResponsavel; }
        if ($name === "IdPessoa") { return $this->IdPessoa; }
        if ($name === "Nome") { return $this->Nome; }
        if ($name === "Telefone") { return $this->Telefone; }
        if ($name === "Email") { return $this->Email; }
        if ($name === "GrauParentesco") { return $this->GrauParentesco; }
        if ($name === "Observacao") { return $this->Observacao; }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdResponsavel") {
            if (is_null($value)) {
                $this->IdResponsavel = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdResponsavel = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdResponsavel;
        }
        if ($name === "IdPessoa") {
            if (is_null($value)) {
                $this->IdPessoa = null;
            }
            else {
                if (isset($value) && preg_match("/^[0-9a-f]{32}$/i", $value)) {
                    $this->IdPessoa = $value;
                }
                else {
                    throw new Exception("<< " . $name . " >> << " . $value . " >> => Tipo UUID inválido.");
                }
            }
            return $this->IdPessoa;
        }
        if ($name === "Nome") {
            if (is_null($value)) {
                $this->Nome = null;
            }
            else {
                $this->Nome = substr((string) $value, 0, 256);
            }
            return $this->Nome;
        }
        if ($name === "Telefone") {
            if (is_null($value)) {
                $this->Telefone = null;
            }
            else {
                $this->Telefone = substr((string) $value, 0, 256);
            }
            return $this->Telefone;
        }
        if ($name === "Email") {
            if (is_null($value)) {
                $this->Email = null;
            }
            else {
                $this->Email = substr((string) $value, 0, 256);
            }
            return $this->Email;
        }
        if ($name === "GrauParentesco") {
            if (is_null($value)) {
                $this->GrauParentesco = null;
            }
            else {
                $this->GrauParentesco = substr((string) $value, 0, 64);
            }
            return $this->GrauParentesco;
        }
        if ($name === "Observacao") {
            if (is_null($value)) {
                $this->Observacao = null;
            }
            else {
                $this->Observacao = substr((string) $value, 0, 65535);
            }
            return $this->Observacao;
        }
        throw new Exception($name . " => Propriedade inválida.");
    }

    // --------------------------------------------------------------------------------
    // save
    // Salva o objeto
    // --------------------------------------------------------------------------------
    public function save()
    {
        // verifica se o registro já existe ou não
        $regexists = $this->existsPk();

        // se o registro existir atualiza, senão insere um novo
        if ($regexists) {
            $sql = "update 
                        responsavel
                    set 
                        idresponsavel = " . ( isset($this->IdResponsavel) ? $this->o_db->quote($IdResponsavel) : "null" ) . ", 
                        idpessoa = " . ( isset($this->IdPessoa) ? $this->o_db->quote($IdPessoa) : "null" ) . ", 
                        nome = " . ( isset($this->Nome) ? $this->o_db->quote($Nome) : "null" ) . ", 
                        telefone = " . ( isset($this->Telefone) ? $this->o_db->quote($Telefone) : "null" ) . ", 
                        email = " . ( isset($this->Email) ? $this->o_db->quote($Email) : "null" ) . ", 
                        grauparentesco = " . ( isset($this->GrauParentesco) ? $this->o_db->quote($GrauParentesco) : "null" ) . ", 
                        observacao = " . ( isset($this->Observacao) ? $this->o_db->quote($Observacao) : "null" ) . "
                    where 
                        idresponsavel" . ( isset($this->IdResponsavel) ? " = " . $this->o_db->quote($this->IdResponsavel) : " is null" ) . "";
        }
        else {
            $sql = "insert into 
                        responsavel (
                            idresponsavel, 
                            idpessoa, 
                            nome, 
                            telefone, 
                            email, 
                            grauparentesco, 
                            observacao
                        )
                        values (
                            " . ( isset($this->IdResponsavel) ? $this->o_db->quote($this->IdResponsavel) : "null" ) . ", 
                            " . ( isset($this->IdPessoa) ? $this->o_db->quote($this->IdPessoa) : "null" ) . ", 
                            " . ( isset($this->Nome) ? $this->o_db->quote($this->Nome) : "null" ) . ", 
                            " . ( isset($this->Telefone) ? $this->o_db->quote($this->Telefone) : "null" ) . ", 
                            " . ( isset($this->Email) ? $this->o_db->quote($this->Email) : "null" ) . ", 
                            " . ( isset($this->GrauParentesco) ? $this->o_db->quote($this->GrauParentesco) : "null" ) . ", 
                            " . ( isset($this->Observacao) ? $this->o_db->quote($this->Observacao) : "null" ) . "
                        );";
        }

        if ($this->o_db->exec($sql) > 0) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------
    // remove
    // Remove o objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function remove()
    {
        // se as PK estiverem definidas faz a exclusão
        if (isset($this->IdResponsavel)) {
            $sql = "delete from 
                        responsavel
                     where 
                        idresponsavel" . ( isset($this->IdResponsavel) ? " = " . $this->o_db->quote($this->IdResponsavel) : " is null" ) . ""; 
            if ($this->o_db->exec($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------------
    // listBy
    // Lista os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function listBy(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdResponsavel = null, 
        string $IdPessoa = null, 
        string $Nome = null, 
        string $Telefone = null, 
        string $Email = null, 
        string $GrauParentesco = null, 
        string $Observacao = null)
    {
        // garante que a primeira página não seja menor que 1 e o tamanho não seja maior que 100
        if (is_null($pagenumber) || ($pagenumber < 1)) { $pagenumber = 1; }
        if (is_null($pagesize) || ($pagesize < 1) || ($pagesize > 100)) { $pagesize = 100; }

        $sql = "select
                    idresponsavel as IdResponsavel, 
                    idpessoa as IdPessoa, 
                    nome as Nome, 
                    telefone as Telefone, 
                    email as Email, 
                    grauparentesco as GrauParentesco, 
                    observacao as Observacao
                from
                    responsavel
                where 1 = 1";

        if (isset($IdResponsavel)) { $sql = $sql . " and (idresponsavel = " . $this->o_db->quote($IdResponsavel) . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Telefone)) { $sql = $sql . " and (telefone like " . $this->o_db->quote("%" . $Telefone. "%") . ")"; }
        if (isset($Email)) { $sql = $sql . " and (email like " . $this->o_db->quote("%" . $Email. "%") . ")"; }
        if (isset($GrauParentesco)) { $sql = $sql . " and (grauparentesco like " . $this->o_db->quote("%" . $GrauParentesco. "%") . ")"; }
        if (isset($Observacao)) { $sql = $sql . " and (observacao like " . $this->o_db->quote("%" . $Observacao. "%") . ")"; }

        $skipvalue = ($pagesize * ($pagenumber - 1));
        $sql = $sql . " limit $pagesize offset $skipvalue";

        $array_responsavel = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                $obj_out = new ResponsavelModel();

                $obj_out->IdResponsavel = $obj_in->IdResponsavel;
                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Telefone = $obj_in->Telefone;
                $obj_out->Email = $obj_in->Email;
                $obj_out->GrauParentesco = $obj_in->GrauParentesco;
                $obj_out->Observacao = $obj_in->Observacao;

                array_push($array_responsavel, $obj_out);
            }
        }

        // retorna a lista de objetos como array
        return $array_responsavel;
    }

    // --------------------------------------------------------------------------------
    // listByIdPessoaNome
    // Lista os registros com base em IdPessoa, Nome
    // --------------------------------------------------------------------------------
    public function listByIdPessoaNome(
        int $pagenumber = 1, 
        int $pagesize   = 25, 
        string $IdPessoa = null, 
        string $Nome = null)
    {
        return $this->listBy($pagenumber, $pagesize, null, $IdPessoa, $Nome, null, null, null, null);
    }

    // --------------------------------------------------------------------------------
    // objectByFields
    // Carrega a primeira ocorrência do objeto que coincida com os campos informados
    // --------------------------------------------------------------------------------
    public function objectByFields(
        string $IdResponsavel = null, 
        string $IdPessoa = null, 
        string $Nome = null, 
        string $Telefone = null, 
        string $Email = null, 
        string $GrauParentesco = null, 
        string $Observacao = null)
    {
        // verifica se foi passa pelo menos um campo
        if (is_null($IdResponsavel) && is_null($IdPessoa) && is_null($Nome)
             && is_null($Telefone) && is_null($Email) && is_null($GrauParentesco)
             && is_null($Observacao)) {
            return null;
        }

        $sql = "select
                    idresponsavel as IdResponsavel, 
                    idpessoa as IdPessoa, 
                    nome as Nome, 
                    telefone as Telefone, 
                    email as Email, 
                    grauparentesco as GrauParentesco, 
                    observacao as Observacao
                from
                    responsavel
                where 1 = 1";

        if (isset($IdResponsavel)) { $sql = $sql . " and (idresponsavel = " . $this->o_db->quote($IdResponsavel) . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome = " . $this->o_db->quote($Nome) . ")"; }
        if (isset($Telefone)) { $sql = $sql . " and (telefone = " . $this->o_db->quote($Telefone) . ")"; }
        if (isset($Email)) { $sql = $sql . " and (email = " . $this->o_db->quote($Email) . ")"; }
        if (isset($GrauParentesco)) { $sql = $sql . " and (grauparentesco = " . $this->o_db->quote($GrauParentesco) . ")"; }
        if (isset($Observacao)) { $sql = $sql . " and (observacao = " . $this->o_db->quote($Observacao) . ")"; }

        $sql = $sql . " limit 1";

        // lê o registro no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma o registro em um objeto
            if ($obj_in = $resultset->fetchObject()) {
                $obj_out = new ResponsavelModel();

                $obj_out->IdResponsavel = $obj_in->IdResponsavel;
                $obj_out->IdPessoa = $obj_in->IdPessoa;
                $obj_out->Nome = $obj_in->Nome;
                $obj_out->Telefone = $obj_in->Telefone;
                $obj_out->Email = $obj_in->Email;
                $obj_out->GrauParentesco = $obj_in->GrauParentesco;
                $obj_out->Observacao = $obj_in->Observacao;

                return $obj_out;
            }
        }

        // retorna null se não for possível recuperar o objeto
        return null;
    }

    // --------------------------------------------------------------------------------
    // loadById
    // Recupera um objeto com base na chave primária
    // --------------------------------------------------------------------------------
    public function loadById(string $IdResponsavel)
    {
        $obj = $this->objectByFields($IdResponsavel, null, null, null, null, null, null);
        if ($obj) {
            $this->IdResponsavel = $obj->IdResponsavel;
            $this->IdPessoa = $obj->IdPessoa;
            $this->Nome = $obj->Nome;
            $this->Telefone = $obj->Telefone;
            $this->Email = $obj->Email;
            $this->GrauParentesco = $obj->GrauParentesco;
            $this->Observacao = $obj->Observacao;

            return $this;
        }
        return null;
    }

    // --------------------------------------------------------------------------------
    // existsPk
    // Verifica se existe um registro com essa Pk
    // --------------------------------------------------------------------------------
    public function existsPk()
    {
        $obj = $this->objectByFields($this->IdResponsavel, null, null, null, null, null, null);
        return isset($obj);
    }

    // --------------------------------------------------------------------------------
    // existsIdPessoaNome
    // Verifica se existe um registro com IdPessoa, Nome
    // --------------------------------------------------------------------------------
    public function existsIdPessoaNome()
    {
        $obj = $this->objectByFields(null, $this->IdPessoa, $this->Nome, null, null, null, null);
        return !($obj && ($obj->IdResponsavel === $this->IdResponsavel));
    }

    // --------------------------------------------------------------------------------
    // countBy
    // Conta os registros com base em filtros
    // --------------------------------------------------------------------------------
    public function countBy(
        string $IdResponsavel = null, 
        string $IdPessoa = null, 
        string $Nome = null, 
        string $Telefone = null, 
        string $Email = null, 
        string $GrauParentesco = null, 
        string $Observacao = null) : int
    {
        $sql = "select
                    count(*) as Quantity
                from
                    responsavel
                where 1 = 1";

        if (isset($IdResponsavel)) { $sql = $sql . " and (idresponsavel = " . $this->o_db->quote($IdResponsavel) . ")"; }
        if (isset($IdPessoa)) { $sql = $sql . " and (idpessoa = " . $this->o_db->quote($IdPessoa) . ")"; }
        if (isset($Nome)) { $sql = $sql . " and (nome like " . $this->o_db->quote("%" . $Nome. "%") . ")"; }
        if (isset($Telefone)) { $sql = $sql . " and (telefone like " . $this->o_db->quote("%" . $Telefone. "%") . ")"; }
        if (isset($Email)) { $sql = $sql . " and (email like " . $this->o_db->quote("%" . $Email. "%") . ")"; }
        if (isset($GrauParentesco)) { $sql = $sql . " and (grauparentesco like " . $this->o_db->quote("%" . $GrauParentesco. "%") . ")"; }
        if (isset($Observacao)) { $sql = $sql . " and (observacao like " . $this->o_db->quote("%" . $Observacao. "%") . ")"; }

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            if ($obj_in = $resultset->fetchObject()) {
                return $obj_in->Quantity;
            }
        }

        // retorna a lista de objetos como array
        return 0;
    }
    // --------------------------------------------------------------------------------
    // countByIdPessoaNome
    // Conta os registros com base em IdPessoa, Nome
    // --------------------------------------------------------------------------------
    public function countByIdPessoaNome(
        string $IdPessoa = null, 
        string $Nome = null)
    {
        return $this->countBy(null, $IdPessoa, $Nome, null, null, null, null);
    }

}

?>
