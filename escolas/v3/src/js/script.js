(function($){
  $(function(){

  $('.timepicker').pickatime({
      default: 'now', // Set default time: 'now', '1:30AM', '16:30'
      fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
      twelvehour: false, // Use AM/PM or 24-hour format
      donetext: 'OK', // text for done-button
      cleartext: 'Clear', // text for clear-button
      canceltext: 'Cancel', // Text for cancel-button
      autoclose: false, // automatic close timepicker
      ampmclickable: true, // make AM PM clickable
      aftershow: function(){} //Function for after opening timepicker
    });

	$('.modal').modal();

	$('.chips-placeholder').material_chip({
		placeholder: 'Digite uma Tag',
		secondaryPlaceholder: '+Tag',
	});


  $('.parallax').parallax();

  $('.carousel').carousel({
        dist:0,
        shift:0,
        padding:20,
        indicators:true,
        duration:200
  });

  autoplay()   
  function autoplay() {
    $('.carousel').carousel('next');
    setTimeout(autoplay, 4500);
  }

  	//NavLeft
  	$('.button-collapse').sideNav();

    /*
    *	Materialize inicianizations
    */
	//materialize_dropdown
  $('.dropdown-button').dropdown();

	$('.dropdown-button').dropdown({
		inDuration: 300,
		outDuration: 225,
    constrainWidth: false,
    hover: true,
    gutter: 0,
    belowOrigin: true,
    alignment: 'left',
    stopPropagation: false
	 });



    $('.dropdown-button').dropdown('open',function(){
    	//when dropdown open
    });
    $('.dropdown-button').dropdown('close',function(){
    	//when dropdown close
    });

	  // Initialize collapse button
	  $(".button-collapse").sideNav();
	  // Initialize collapsible (uncomment the line below if you use the dropdown variation)
	  //$('.collapsible').collapsible();

	  //datepicker
    var data = new Date();


	  $('.datepicker').pickadate({
      selectMonths: true, 
      selectYears: 60, 
      today: false,
      clear: 'Resetar',
      close: 'Ok',
      labelMonthNext: 'Próximo mês',
      labelMonthPrev: 'Mês anterior',
      labelMonthSelect: 'Selecione o mês',
      labelYearSelect: 'Selecione o ano',
      monthsFull: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
      monthsShort: [ 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ],
      weekdaysFull: [ 'Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado' ],
      weekdaysShort: [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab' ],
      weekdaysLetter: [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S' ],
      format: 'yyyy-mm-dd',
      closeOnSelect: false,
      min: new Date(data.getFullYear() - 100, 12, 30),
      max: new Date(data.getFullYear() + -5, 12, 30)
  });

  //select
  $('select').material_select();

/*
*   Funcionalidades da plataforma
*/

	// Preloader controll
	$('.preloader').addClass("hide");
	$(".loaded").removeClass("hide");


  //Preloader Bruno
  $('.preloader-background').delay(1700).fadeOut('slow');
  $('.preloader-wrapper').delay(1700).fadeOut();

  }); // end of document ready
})(jQuery); // end of jQuery name space



document.addEventListener("DOMContentLoaded", function(){
  $('.preloader-background').delay(1700).fadeOut('slow');
  
  $('.preloader-wrapper')
    .delay(1700)
    .fadeOut();
});