<?php

require_once $APP_PATH_ROOT."/lib/BDConBaseModel.php";

// --------------------------------------------------------------------------------
// GraficoPessoaModel
// Classe para criação de gráficos a partir do desempenhos de pessoas/alunos.
//
// Gerado em: 2018-03-21 09:09:00
// --------------------------------------------------------------------------------
class GraficoPessoaModel extends BDConBaseModel
{
    // Construtor da classe, executado quando a classe e criada
    function __construct() {
        parent::__construct();
    }

	private $IdInstituicao;
    private $IdPessoa;
    private $CodigoAcesso;
    private $IdDisciplina;
    private $PeriodoLetivo;

    // --------------------------------------------------------------------------------
    // Getter das propriedades
    // --------------------------------------------------------------------------------
    public function __get($name) {
        if ($name === "IdInstituicao") { return $this->IdInstituicao; }
        if ($name === "IdPessoa") { return $this->IdPessoa; }
        if ($name === "CodigoAcesso") { return $this->CodigoAcesso; }
        if ($name === "IdDisciplina") { return $this->IdDisciplina; }
        if ($name === "PeriodoLetivo") { return $this->PeriodoLetivo; }
        throw new Exception( $name . ' => Propriedade inválida.');
    }

    // --------------------------------------------------------------------------------
    // Setters das propriedades
    // --------------------------------------------------------------------------------
    public function __set($name, $value) {
        if ($name === "IdInstituicao") { $this->IdInstituicao = $value; return $value; }
        if ($name === "IdPessoa") { $this->IdPessoa = $value; return $value; }
        if ($name === "CodigoAcesso") { $this->CodigoAcesso = $value; return $value; }
        if ($name === "IdDisciplina") { $this->IdDisciplina = $value; return $value; }
        if ($name === "PeriodoLetivo") { $this->PeriodoLetivo = $value; return $value; }
        throw new Exception( $name . ' => Propriedade inválida.');
    }

    // --------------------------------------------------------------------------------
    // DesempenhoPorDisciplina
    //
    // Retorno
    //     Unidade            Unidade vinculada à pessoa
    //     Professor          Nome do professor
    //     Disciplina         Nome da disciplina
    //     PeriodoLetivo      Período letivo (Ano)
    //     Curso              Nome do curso
    //     Etapa              Nome da etapa, normalmente a série
    //     Turma              Sigla da turma
    //     Turno              Nome do turno
    //     QtdePartidas       Quantidade de partidas
    //     MenorDesempenho    Menor desempenho
    //     MediaDesempenho    Média do desempenho
    //     MaiorDesempenho    Maior desempenho
    // --------------------------------------------------------------------------------
    public function DesempenhoPorDisciplina()
    {
		
		// valida o id do jogo e da instituicao
        if (is_null($this->IdInstituicao) || is_null($this->IdPessoa) || is_null($this->CodigoAcesso)) {
            return array();
        }
		
        // Dados do desempenho das disciplinas
        $sql = "select	
                    unidade.nome as Unidade,
                    pessoaprofessor.nome as Professor,
                    disciplina.nome as Disciplina,
                    turma.periodoletivo as PeriodoLetivo,
                    curso.nome as Curso,
                    etapa.nome as Etapa,
                    turma.sigla as Turma,
                    turno.nome as Turno,
                    count(*) as QtdePartidas,
                    min(partida.desempenho) as MenorDesempenho,
                    format(avg(partida.desempenho), 2) as MediaDesempenho,
                    max(partida.desempenho) as MaiorDesempenho
                from
                    partida
                    join contapessoa
                        on contapessoa.idpessoa = partida.idpessoa
                        and contapessoa.idinstituicao = partida.idinstituicao		
                    join turma_contapessoa
                        on turma_contapessoa.idcontapessoa = contapessoa.idcontapessoa
                        and turma_contapessoa.idinstituicao = contapessoa.idinstituicao
                    join turma
                        on turma.idinstituicao = turma_contapessoa.idinstituicao
                        and turma.idunidade = turma_contapessoa.idunidade
                        and turma.idcurso = turma_contapessoa.idcurso
                        and turma.idturma = turma_contapessoa.idturma
                        " . ((isset($this->PeriodoLetivo) && ($this->PeriodoLetivo !== "")) ? "and turma.periodoletivo = " . $this->o_db->quote($this->PeriodoLetivo) : "")  . "
                    join unidade
                        on unidade.idinstituicao = turma.idinstituicao
                        and unidade.idunidade = turma.idunidade
                    join curso
                        on curso.idinstituicao = turma.idinstituicao
                        and curso.idcurso = turma.idcurso
                    join etapa
                        on etapa.idinstituicao = turma.idinstituicao
                        and etapa.idcurso = turma.idcurso
                        and etapa.idetapa = turma.idetapa		
                    join turno
                        on turno.idinstituicao = turma.idinstituicao
                        and turno.idturno = turma.idturno
                    join turma_professor_disciplina
                        on turma_professor_disciplina.idinstituicao = turma.idinstituicao
                        and turma_professor_disciplina.idunidade = turma.idunidade
                        and turma_professor_disciplina.idcurso = turma.idcurso
                        and turma_professor_disciplina.idturma = turma.idturma
                    join disciplina
                        on disciplina.idinstituicao = turma_professor_disciplina.idinstituicao
                        and disciplina.iddisciplina = turma_professor_disciplina.iddisciplina		
                    join professor
                        on professor.idinstituicao = turma_professor_disciplina.idinstituicao
                        and professor.idprofessor = turma_professor_disciplina.idprofessor
                    join pessoa pessoaprofessor
                        on pessoaprofessor.idpessoa = professor.idprofessor
                where 1 = 1
                    and partida.idinstituicao = " . $this->o_db->quote($this->IdInstituicao) . "
                    and	partida.idpessoa = " . $this->o_db->quote($this->IdPessoa) . "
                    and partida.codigoacesso = " . $this->o_db->quote($this->CodigoAcesso) . "
                group by
                    1, 2, 3, 4, 5, 6, 7, 8
                order by
                    1, 2, 3";
			 
        $array_result = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                array_push($array_result, $obj_in);
            }
        }

        // retorna o ranking como array
        return $array_result;
    }

    // --------------------------------------------------------------------------------
    // DesempenhoDisciplinaDiario
    //
    // Retorno
    //     EscalaTempo        Escala de tempo do resultado    
    //     Unidade            Unidade vinculada à pessoa
    //     Professor          Nome do professor
    //     Disciplina         Nome da disciplina
    //     PeriodoLetivo      Período letivo (Ano)
    //     Curso              Nome do curso
    //     Etapa              Nome da etapa, normalmente a série
    //     Turma              Sigla da turma
    //     Turno              Nome do turno
    //     QtdePartidas       Quantidade de partidas
    //     MenorDesempenho    Menor desempenho
    //     MediaDesempenho    Média do desempenho
    //     MaiorDesempenho    Maior desempenho
    //
    // Parâmetro de entrada:
    //     D => DiaMesAno     Dia, Mês e Ano das partidas
    //     S => AnoSemana     Ano e Semana das partidas
    //     M => AnoMes        Ano e Mês das partidas (valor padrão)
    // --------------------------------------------------------------------------------
    public function DesempenhoDisciplinaPeriodo($EscalaTempo)
    {		
		// valida o id do jogo e da instituicao
        if (is_null($this->IdInstituicao) || is_null($this->IdPessoa) || is_null($this->IdDisciplina) || is_null($this->CodigoAcesso)) {
            return array();
        }

        switch ($EscalaTempo) {
            case 'D':
                $EscalaSel = "date_format(partida.datahorapartida, '%Y-%m-%d') as EscalaTempo,";
                break;
            case 'S':
                $EscalaSel = "concat(date_format(partida.datahorapartida, '%Y'), '-', right(concat('00', week(partida.datahorapartida, 2)), 2)) as EscalaTempo,";
                break;
            default:
                $EscalaSel = "date_format(partida.datahorapartida, '%Y-%m') as EscalaTempo,";
        }        

        // Dados do desempenho das disciplinas
        $sql = "select
                    " . $EscalaSel . "
                    unidade.nome as Unidade,
                    pessoaprofessor.nome as Professor,
                    disciplina.nome as Disciplina,
                    turma.periodoletivo as PeriodoLetivo,
                    curso.nome as Curso,
                    etapa.nome as Etapa,
                    turma.sigla as Turma,
                    turno.nome as Turno,
                    count(*) as QtdePartidas,
                    min(partida.desempenho) as MenorDesempenho,
                    format(avg(partida.desempenho), 2) as MediaDesempenho,
                    max(partida.desempenho) as MaiorDesempenho
                from
                    partida
                    join contapessoa
                        on contapessoa.idpessoa = partida.idpessoa
                        and contapessoa.idinstituicao = partida.idinstituicao		
                    join turma_contapessoa
                        on turma_contapessoa.idcontapessoa = contapessoa.idcontapessoa
                        and turma_contapessoa.idinstituicao = contapessoa.idinstituicao
                    join turma
                        on turma.idinstituicao = turma_contapessoa.idinstituicao
                        and turma.idunidade = turma_contapessoa.idunidade
                        and turma.idcurso = turma_contapessoa.idcurso
                        and turma.idturma = turma_contapessoa.idturma
                        " . ((isset($this->PeriodoLetivo) && ($this->PeriodoLetivo !== "")) ? "and turma.periodoletivo = " . $this->o_db->quote($this->PeriodoLetivo) : "")  . "
                    join unidade
                        on unidade.idinstituicao = turma.idinstituicao
                        and unidade.idunidade = turma.idunidade
                    join curso
                        on curso.idinstituicao = turma.idinstituicao
                        and curso.idcurso = turma.idcurso
                    join etapa
                        on etapa.idinstituicao = turma.idinstituicao
                        and etapa.idcurso = turma.idcurso
                        and etapa.idetapa = turma.idetapa		
                    join turno
                        on turno.idinstituicao = turma.idinstituicao
                        and turno.idturno = turma.idturno
                    join turma_professor_disciplina
                        on turma_professor_disciplina.idinstituicao = turma.idinstituicao
                        and turma_professor_disciplina.idunidade = turma.idunidade
                        and turma_professor_disciplina.idcurso = turma.idcurso
                        and turma_professor_disciplina.idturma = turma.idturma
                    join disciplina
                        on disciplina.idinstituicao = turma_professor_disciplina.idinstituicao
                        and disciplina.iddisciplina = turma_professor_disciplina.iddisciplina		
                    join professor
                        on professor.idinstituicao = turma_professor_disciplina.idinstituicao
                        and professor.idprofessor = turma_professor_disciplina.idprofessor
                    join pessoa pessoaprofessor
                        on pessoaprofessor.idpessoa = professor.idprofessor
                where 1 = 1
                    and partida.idinstituicao = " . $this->o_db->quote($this->IdInstituicao) . "
                    and	partida.idpessoa = " . $this->o_db->quote($this->IdPessoa) . "
                    and partida.codigoacesso = " . $this->o_db->quote($this->CodigoAcesso) . "
                    and disciplina.iddisciplina = " . $this->o_db->quote($this->IdDisciplina) . "
                group by
                    1, 2, 3, 4, 5, 6, 7, 8, 9
                order by
                    1, 2, 3";
        
        $array_result = array();

        // lê os registros no bd
        if ($resultset = $this->o_db->query($sql)) {
            // transforma os registros em objetos e adiciona ao array de retorno
            while ($obj_in = $resultset->fetchObject()) {
                array_push($array_result, $obj_in);
            }
        }

        // retorna o ranking como array
        return $array_result;
    }


}

?>
