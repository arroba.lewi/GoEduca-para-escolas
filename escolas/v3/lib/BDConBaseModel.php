<?php
// --------------------------------------------------------------------------------
// BDConBaseModel
//
// Classe BASE para acesso ao banco de dados.
//
// Gerado em: 2018-03-13
// --------------------------------------------------------------------------------
abstract class BDConBaseModel
{
	protected $o_db;
	
	function __construct()
	{
		$st_host = '35.198.44.92';
		$st_banco = 'goeduca_dev';
		$st_usuario = 'dev001';
		$st_senha = 'dev001'; 
		 
		$st_dsn = "mysql:host=$st_host;dbname=$st_banco"; 
		$this->o_db = new PDO
		(
			$st_dsn,
			$st_usuario,
			$st_senha
		);
	
		$this->o_db->setAttribute ( PDO::ATTR_ERRMODE , PDO::ERRMODE_EXCEPTION );
	}
}
?>