<section class="bloco2">
    <div class="row">
        <div class="col s12">
           <h3>
               <b>GoEduca - ADM</b>
           </h3>
        </div>
    </div>
</section>
<section class="bloco2">
    <div class="row">
        <div class="col s12" style="margin-bottom: 15px;">
            <p class="left">
                <b>Nome da Tabela</b>
            </p>
            <a class="btn  transparent grey-text right">
                <i class="material-icons left">add_circle_outline</i>
                Novo Registro
            </a>
        </div>
        <table class="col s12 striped">
            <thead>
                <tr>
                    <th>Column 1</th>
                    <th>Column 2</th>
                    <th>Column 3</th>
                    <th>Column 4</th>
                    <th>Column 5</th>
                    <th>Column 6</th>
                    <th>Column 7</th>
                    <th class="center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td class="center">
                        <a href="#!" class="btn btn-floating cyan tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="#!" class="btn btn-floating red tooltipped" data-position="bottom" data-delay="50" data-tooltip="Excluir">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td class="center">
                        <a href="#!" class="btn btn-floating cyan tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="#!" class="btn btn-floating red tooltipped" data-position="bottom" data-delay="50" data-tooltip="Excluir">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td class="center">
                        <a href="#!" class="btn btn-floating cyan tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="#!" class="btn btn-floating red tooltipped" data-position="bottom" data-delay="50" data-tooltip="Excluir">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td class="center">
                        <a href="#!" class="btn btn-floating cyan tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="#!" class="btn btn-floating red tooltipped" data-position="bottom" data-delay="50" data-tooltip="Excluir">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td>x</td>
                    <td class="center">
                        <a href="#!" class="btn btn-floating cyan tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="#!" class="btn btn-floating red tooltipped" data-position="bottom" data-delay="50" data-tooltip="Excluir">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>