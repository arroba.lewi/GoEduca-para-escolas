
<style type="text/css">
body, html{
  min-height: 100%;
}
@keyframes animatedBackground {
	from { background-position: 0 0; }
	to { background-position: 100% 0; }
  }

body{
  background-image: url('https://goeduca.com/escolas/v3/components/config/imgResize2.php?img=/var/www/html/escolas/v3/src/img/background-games.png&w=500');
  background-size: auto 100%;
  background-attachment: fixed;
  background-repeat: repeat-x;
  animation: animatedBackground 25s alternate infinite;
}
.cont-login{
  position: fixed !important;
  height:  100%;
  padding: 100px 0px !important;
  margin-top: 0px;
  margin-bottom: 0px;
  right:  0 !important;
  background: linear-gradient(to right, rgb(255,255,255), rgb(240,240,240));
  border-left: 1px solid #bbb;
}
#login-page{
  position:  absolute; 
  height:  100%; 
  width:  66.666666666%;
  align-items: center;
  display: flex;
  background: rgba(0,161,176,0.7);
  background: -moz-linear-gradient(to bottom right, #48a1afbb, #232423);
  background: -webkit-linear-gradient(bottom right, #48a1afbb, #232423);
  background: linear-gradient(to bottom right, #48a1afbb, #232423);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#48a1afbb', endColorstr='#232423',GradientType=0 );
  color: white;
}

/*botao de login mobile*/
.botao-login {
  background: none;
  color: white;
  border: 1px solid white;
  border-radius: 35px;
  cursor: pointer;
  font-size: 14px;
  outline: none;
  padding: 15px 30px;
  top: 15px;
  right: 15px;
  position: absolute;

}
/* OS ESTILOS ABAIXO SERÃO APLICADOS SE O DISPOSITIVO TIVER NO MÁXIMO 600PX */
@media only screen and (max-width : 600px) {
  .cont-login{
    position: fixed !important;
  }
  
}
#login-page{
    position: absolute;
    width: 100% !important;
  }

  #botao-close {
    position: absolute;
    top: 15px;
    right: 15px; 
  }
</style>



<section id="login-page" class="row no-margin-b">

<div class="col offset-l2 offset-m1 offset-s1" style="padding: 100px 0;">
  <div id="login" class="show-on-small botao-login" onclick="exibeLogin ()"> Fazer Login</div>
    <h2>
      <b>
        Estude jogando!
      </b>
    </h2>
    <p class="flow-text" style="margin-top: -12px;">O modo mais divertido de aprender!</p>
    <p class="flow-text" style="margin-top: 50px; font-size: 14px;">
      Quer levar o GoEduca para sua escola? 
      <a href="/goeduca-para-escolas" class="teal-text text-accent-2">Saiba Mais</a>
    </p>
</div>



 <!--
    //   LADO DIREITO
    //      FORMULARIO DE LOGIN
-->


<div id="telaMob" class="col s12 m5 l4 z-depth-0 card-panel cont-login hide-on-small-only">
  <form class="grey-text" method="POST" action=/escolas/v3/components/config/action-pessoa.php>
    <input type="hidden" name="func" value="FazerLoginSchool">

    <!-- BOTAO PARA FECHAR TELA MOBILE-->

    <div><a id="botao-close" class="hide-on-med-and-up" onclick="closeTela()"><i class="material-icons">close</i></a></div>

    <br>
    <div class="row">
      <div class="col s12 center">  
        <a class="center grey-text text-darken-2">
          <img src='https://goeduca.com/escolas/v3/src/img/marca-azul-escuro.png' width="175">
          <div style="line-height:1; color: grey; font-size: 12px;">Para Escolas</div>
        </a>
        <span class="red-text">
                  </span>
      </div>
    </div>
    <div class="row margin">
      <div class="input-field col s10 offset-s1">
        <i class="mdi-social-person-outline prefix material-icons">person</i>
        <input id="acesso" name="acesso" type="text" placeholder="Informação de acesso">
        <label for="acesso" class="center-align active">Acesso</label>
      </div>
    </div>
    <div class="row margin up-small">
      <div class="input-field col s10 offset-s1">
        <i class="mdi-action-lock-outline prefix material-icons">lock</i>
        <input id="senha" name="senha" type="password" placeholder="Sua Senha">
        <label for="senha" class="active">Senha</label>
      </div>
    </div>
    <div class="row">
      <div class="col s10 offset-s1 center">
        <button type="submit" name="login" class="btn waves-effect waves-light">Login</button>
      </div>
    </div>
    <div class="row">
      <div class="col s12 center">
        <a href="#!" class="teal-text"><b>Esqueci minha senha</b></a>
        
      </div>
    </div>
    <br>
  </form>
</div>



</section>



  <script type="text/javascript">
    function exibeLogin (){
            var retiraClasse = document.getElementById('telaMob').classList.remove('hide-on-small-only');
        }

    function closeTela (){
            var addClasse = document.getElementById('telaMob').classList.add('hide-on-small-only');
    }
  </script>