<section class="bloco2">
	<section class="row">
		<div class="col s12">
			<nav class="transparent z-depth-0">
				<div class="nav-wrapper">
					<div class="col">
					<a href='#!' class='breadcrumb'><?php echo $pessoaNome; ?></a>
					</div>
				</div>
			</nav>
		</div>
	</section>

<?php
	// 
	// PEGANDO ID DA DISCIPLINA ATUAL
	// 

	$IdDisciplinaAtual = isset($_GET['m']) ? $_GET['m'] : NULL;

	// 
	// PEGANDO ESCALA DE TEMPO ATUAL
	// 

	$EscalaDeTempoAtual = isset($_GET['t']) ? $_GET['t'] : "M";
?>

			<!--
			//
			//	Gráficos
			//
		-->
		<section id="chart-dashboard">
					<!--
						//
						//	GRÁFICO LINEAR ( MÉDIA GERAL DOS ALUNOS )
						//
					-->

			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-content waves-effect waves-block waves-light">
						<?php
							if(is_null($IdDisciplinaAtual))
								echo "<small class='red-text'>Selecione uma matéria</small>";
						?>
						<form action="" method="GET" onchange="$(this).submit();">
							<div class="input-field left" style="width:300px; margin-left: 20px;">
								<select name="m" id="materia">
									<?php
										// 
										// LISTANDO TODAS AS DISCILINAS DESTA INSTITUIÇÃO
										//
										$o_disciplina = new DisciplinaModel();
										$result = $o_disciplina->listByIdInstituicaoNome(1,100,$pessoaIdInstituicao,NULL);

										for($i=0; $i<count($result); $i++){
											if($result[$i]->IdDisciplina == $IdDisciplinaAtual){
												$option_selected = "selected='selected'";
												$NomeDisciplinaAtual = $result[$i]->Nome;
											}else{
												$option_selected = NULL;
												$NomeDisciplinaAtual = NULL;
											}
											echo "<option $option_selected value='".$result[$i]->IdDisciplina."'>".$result[$i]->Nome."</option>";
										}
									?>
								</select>
								<label for="disciplina">Disciplina</label>
							</div>
							<div class="input-field left" style="width:300px; margin-left: 20px;">
								<select name="t" id="tempo">
										<?php
											$dia_selected = ($EscalaDeTempoAtual == "D") ? "selected='selected'" : NULL;
											$semana_selected = ($EscalaDeTempoAtual == "S") ? "selected='selected'" : NULL;
											$mes_selected = ($EscalaDeTempoAtual == "M") ? "selected='selected'" : NULL;
										?>
									<option <?php echo $dia_selected; ?>  value="D">Por dia</option>
									<option <?php echo $semana_selected; ?>  value="S">Por semana</option>
									<option <?php echo $mes_selected; ?>  value="M">Por mês</option>
								</select>
								<label for="tempo">Tempo</label>
							</div>
						</form>


							<a class="btn-floating btn-move-up2 waves-effect waves-light transparent z-depth-0 right">
								<i class="material-icons activator grey-text">filter_list</i>
							</a>
							<div class="move-up transparent">
								<div class="trending-line-chart-wrapper">
									<canvas id="trending-line-chart" class="line-chartd" height="350" width="824" style="width: 824px; height: 350px;"></canvas>
								</div>
							</div>
						</div>
						<div class="card-reveal" style="display: none; transform: translateY(0px);">
							<div class="card-title grey-text text-darken-4" style="height: 68px;">
								<div class="left">Detalhado</div>
								<i class="material-icons right">close</i>

							</div>
							<table class="responsive-table striped">
								<thead>
									<tr>
										<th>Periodo</th>
										<th>Menor nota</th>
										<th>Média</th>
										<th>Maior nota</th>
									</tr>
								</thead>
								<tbody>
									
									<?php
										$pessoaCodigoAcesso =  $_SESSION['codigoacesso'];
										$pessoaID = $_SESSION['pessoaID'];
										$pessoaIdInstituicao = $_SESSION['IdInstituicao'];
				
										$o_graficoPessoa = new GraficoPessoaModel();
				
										$o_graficoPessoa->IdInstituicao = $pessoaIdInstituicao;
										$o_graficoPessoa->IdPessoa = $pessoaID;
										$o_graficoPessoa->CodigoAcesso = $pessoaCodigoAcesso;
										$o_graficoPessoa->IdDisciplina = $IdDisciplinaAtual;
										$o_graficoPessoa->PeriodoLetivo = "2018";
										
										$result = $o_graficoPessoa->DesempenhoDisciplinaPeriodo($EscalaDeTempoAtual);
				
										$eixoX = $menor = $maior = $media = "";
				
										for($i=0; $i < count($result); $i++){
											$eixoX .= "'".$result[$i]->EscalaTempo."'".($i < count($result) - 1 ? ",": "");
											$menor .= $result[$i]->MenorDesempenho.($i < count($result) - 1 ? ",": "");
											$media .= $result[$i]->MediaDesempenho.($i < count($result) - 1 ? ",": "");
											$maior .= $result[$i]->MaiorDesempenho.($i < count($result) - 1 ? ",": "");

											echo "<tr>
													<td>".$result[$i]->EscalaTempo."</td>
													<td>".$result[$i]->MenorDesempenho."</td>
													<td>".$result[$i]->MediaDesempenho."</td>
													<td>".$result[$i]->MaiorDesempenho."</td>
												</tr>";

										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<?php
						echo "<script type='text/javascript'>
								var contextod = document.getElementsByClassName('line-chartd');
								var chartGraph = new Chart(contextod, {
									type: 'line',
									data: {
										labels: [".$eixoX."],
										datasets: [
		
										{
											label: 'Média',
											data: [".$media."],
											borderWidth: 2,
											borderColor: 'green',
											backgroundColor: 'transparent',
										},
										{
											label: 'Menor nota',
											data: [".$menor."],
											borderWidth: 2,
											borderColor: 'red',
											backgroundColor: 'transparent',
										},
										{
											label: 'Maior nota',
											data: [".$maior."],
											borderWidth: 2,
											borderColor: 'blue',
											backgroundColor: 'transparent',
										},
		
										]
									},
									options: {
										title: {
											display: true,
											fontSize: 15,
											fontColor: '#222',
											text: 'Média de desempenho em $NomeDisciplinaAtual'
										},
										legend: {
											display: true,
											position: 'bottom'
										},
										labels: {
											fontStyle: 'bold'
										},
										maintainAspectRatio: true,
									}
								});
							</script>";
					?>
				</div>
			</div>




					<!--
						//
						//	GRÁFICO RADAR (COMPARATIVO DA MÉDIA DE CADA ESCOLA POR DICIPLINA)
						//
					-->

					<div class="card" style="overflow: hidden;">	
						<div class="card-content">
							<a class="btn-floating btn-move-up2 waves-effect waves-light transparent z-depth-0 right">
								<i class="material-icons activator grey-text">filter_list</i>
							</a>
							<div class="col s12">
								<canvas class="line-charte" height="200" width="300" style="width: 100%; height: 200px;"></canvas>
							</div>
						</div>
						<div class="card-reveal" style="display: none; transform: translateY(0px);">
							<div class="card-title grey-text text-darken-4" style="height: 68px;">
								<div class="left">Detalhado</div>
								<i class="material-icons right">close</i>

							</div>
							<table class="responsive-table striped">
								<thead>
									<tr>
										<th>Disciplina</th>
										<th>Menor nota</th>
										<th>Media</th>
										<th>Maior nota</th>
									</tr>
								</thead>
								<tbody>
									<?php
				
										$o_graficoPessoa = new GraficoPessoaModel();
				
										$o_graficoPessoa->IdInstituicao = $pessoaIdInstituicao;
										$o_graficoPessoa->IdPessoa = $pessoaID;
										$o_graficoPessoa->CodigoAcesso = $pessoaCodigoAcesso;
										// $o_graficoPessoa->IdDisciplina = "442dc54d2b8e11e88d7f42010a9e0013";
										$o_graficoPessoa->PeriodoLetivo = "2018";
										
										$result = $o_graficoPessoa->DesempenhoPorDisciplina();
				
										$eixoX = $menor = $maior = $media = "";
				
										for($i=0; $i < count($result); $i++){
											$eixoX .= "'".$result[$i]->Disciplina."'".($i < count($result) - 1 ? ",": "");
											$menor .= $result[$i]->MenorDesempenho.($i < count($result) - 1 ? ",": "");
											$media .= $result[$i]->MediaDesempenho.($i < count($result) - 1 ? ",": "");
											$maior .= $result[$i]->MaiorDesempenho.($i < count($result) - 1 ? ",": "");

											echo "<tr>
													<td>".$result[$i]->Disciplina."</td>
													<td>".$result[$i]->MenorDesempenho."</td>
													<td>".$result[$i]->MediaDesempenho."</td>
													<td>".$result[$i]->MaiorDesempenho."</td>
												</tr>";

										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<?php
					echo "<script type='text/javascript'>
						var contextoe = document.getElementsByClassName('line-charte');
						var chartGraph = new Chart(contextoe, {
							type: 'radar',
							data: {
								labels: [".$eixoX."],
								datasets: [

								{
									label: 'Media',
									data: [".$media."],
									borderWidth: 2,
									backgroundColor: 'transparent',
									borderColor: 'green',
								},

								{
									label: 'Menor nota',
									data: [".$menor."],
									borderWidth: 2,
									backgroundColor: 'transparent',
									borderColor: 'red',
								},

								{
									label: 'Maior nota',
									data: [".$maior."],
									borderWidth: 2,
									backgroundColor: 'transparent',
									borderColor: 'blue',
								},


								]
							},
							options: {
								scale: {
									pointLabels: {
										fontSize: 15
									}
								},
								legend: {
									position: 'bottom',
								},
								title: {
									display: true,
									fontSize: 15,
									fontColor: '#222',
									text: 'Matérias'
								},
								labels: {
									fontStyle: 'bold'
								},
								maintainAspectRatio: true,
							}
						});
					</script>";
					?>


				</div>
		</section>