<section class="bloco2">
    <section class="row">
		<div class="col s12">
			<nav class="transparent z-depth-0">
				<div class="nav-wrapper">
					<div class="col">
						<a href="https://goeduca.com/school/?escola" class="breadcrumb">Nome da Escola</a>
						<a href="https://goeduca.com/school/?turmas" class="breadcrumb">Unidade A</a>
					</div>
				</div>
			</nav>
		</div>
    </section>

    <section class="row">
        <div class="col input-field" style="width: 150px">
            <select name="etapa" id="etapa">
                <option value="">9º</option>
                <option value="">8º</option>
                <option value="">7º</option>
                <option value="">6º</option>
                <option value="">5º</option>
                <option value="">4º</option>
                <option value="">3º</option>
                <option value="">2º</option>
                <option value="">1º</option>
            </select>
            <label for="etapa">Etapa</label>
        </div>
        <div class="col input-field" style="width: 150px">
            <select name="turma" id="turma">
                <option value="">A</option>
                <option value="">B</option>
                <option value="">C</option>
                <option value="">D</option>
                <option value="">E</option>
                <option value="">F</option>
                <option value="">G</option>
                <option value="">H</option>
                <option value="">I</option>
            </select>
            <label for="turma">Turma</label>
        </div>
        <div class="col input-field" style="width: 150px">
            <select name="turno" id="turno">
                <option value="">Matutino</option>
                <option value="">Vespertino</option>
                <option value="">Noturno</option>
            </select>
            <label for="turno">Turno</label>
        </div>
        <div class="col input-field" style="width: 150px">
            <select name="disciplina" id="disciplina">
                <option value="">Geral</option>
                <option value="">Matemática</option>
                <option value="">Geografia</option>
                <option value="">História</option>
                <option value="">Artes</option>
            </select>
            <label for="disciplina">Disciplina</label>
        </div>
    </section>
    

<div class="row">
		<div class="col s12">
			<ul id="issues-collection" class="collection z-depth-1">
				<li class="collection-item blue accent-2 white-text center-align">
					<h6 class="collection-header m-0">Suas turmas</h6>
				</li>
				<li class="collection-item">
					<a href="#!">Matutino</a>
				</li>
				<li class="collection-item">
					<div class="row">
						<div class="col s2 center-align">
							<p class="collections-title">
								<strong>9º</strong>
							</p>
						</div>
						<div class="col s10">
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">A</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">B</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">C</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">D</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">E</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">F</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">G</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">H</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">I</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">J</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">K</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">L</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">M</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">N</span>
							</a>
						</div>
					</div>
				</li>
				<li class="collection-item">
					<div class="row">
						<div class="col s2 center-align">
							<p class="collections-title">
								<strong>8º</strong>
							</p>
						</div>
						<div class="col s10">
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">A</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">B</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">C</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">D</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">E</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">F</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">G</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">H</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">I</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">J</span>
							</a>
						</div>
					</div>
				</li>
				<li class="collection-item">
					<div class="row">
						<div class="col s2 center-align">
							<p class="collections-title">
								<strong>7º</strong>
							</p>
						</div>
						<div class="col s10">
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">A</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">B</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma teal darken-2">C</span>
							</a>
						</div>
					</div>
				</li>



				<li class="collection-item">
					<a href="#!">Vespertino</a>
				</li>
				<li class="collection-item">
					<div class="row">
						<div class="col s2 center-align">
							<p class="collections-title">
								<strong>8º</strong>
							</p>
						</div>
						<div class="col s10">
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">A</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">B</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">C</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">D</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">E</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">F</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">G</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">H</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">I</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">J</span>
							</a>
						</div>
					</div>
				</li>
				<li class="collection-item">
					<div class="row">
						<div class="col s2 center-align">
							<p class="collections-title">
								<strong>7º</strong>
							</p>
						</div>
						<div class="col s10">
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">A</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">B</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma orange darken-4">C</span>
							</a>
						</div>
					</div>
				</li>



				<li class="collection-item">
					<a href="#!">Noturno</a>
				</li>
				<li class="collection-item">
					<div class="row">
						<div class="col s2 center-align">
							<p class="collections-title">
								<strong>8º</strong>
							</p>
						</div>
						<div class="col s10">
							<a href="/escolas/v1/turma">
								<span class="turma indigo darken-4">A</span>
							</a>
							<a href="/escolas/v1/turma">
								<span class="turma indigo darken-4">B</span>
							</a>
						
					</div>
				</div>
			</li>
			<li class="collection-item">
				<div class="row">
					<div class="col s2 center-align">
						<p class="collections-title">
							<strong>7º</strong>
						</p>
					</div>
					<div class="col s10">
						<a href="/escolas/v1/turma">
							<span class="turma indigo darken-4">A</span>
						</a>
						<a href="/escolas/v1/turma">
							<span class="turma indigo darken-4">B</span>
						</a>
					</div>
				</div>
			</li>

		</ul>
	</div>


</div>



<section id="chart-dashboard">
					<!--
						//
						//	GRÁFICO LINEAR ( MÉDIA GERAL DOS ALUNOS )
						//
					-->

			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-content waves-effect waves-block waves-light">
												<form action="" method="GET" onchange="$(this).submit();">
							<div class="input-field left" style="width:300px; margin-left: 20px;">
								<div class="select-wrapper"><span class="caret">▼</span><input type="text" class="select-dropdown" readonly="true" data-activates="select-options-94be86e8-deae-67c0-4a5a-4d0dcb24632b" value="Redação"><ul id="select-options-94be86e8-deae-67c0-4a5a-4d0dcb24632b" class="dropdown-content select-dropdown "><li class=""><span>Artes</span></li><li class=""><span>Biologia</span></li><li class=""><span>Ciências</span></li><li class=""><span>Educação Física</span></li><li class=""><span>Educação Infantil</span></li><li class=""><span>Ensino Fundamental</span></li><li class=""><span>Espanhol</span></li><li class=""><span>Filosofia</span></li><li class=""><span>Física</span></li><li class=""><span>Geografia</span></li><li class=""><span>Gramática</span></li><li class=""><span>História</span></li><li class=""><span>Inglês</span></li><li class=""><span>Laboratório</span></li><li class=""><span>Literatura</span></li><li class=""><span>Matemática</span></li><li class=""><span>Matemática II</span></li><li class=""><span>Música</span></li><li class=""><span>Português</span></li><li class=""><span>Química</span></li><li class=""><span>Redação</span></li><li class=""><span>Sociologia</span></li></ul><select name="m" id="materia" data-select-id="94be86e8-deae-67c0-4a5a-4d0dcb24632b" class="initialized">
									<option value="0479569d308011e88d7f42010a9e0013">Artes</option><option value="04d8a280308011e88d7f42010a9e0013">Biologia</option><option value="053d8a53308011e88d7f42010a9e0013">Ciências</option><option value="059d2723308011e88d7f42010a9e0013">Educação Física</option><option value="05fb2eb6308011e88d7f42010a9e0013">Educação Infantil</option><option value="065a951b308011e88d7f42010a9e0013">Ensino Fundamental</option><option value="06b9fcd0308011e88d7f42010a9e0013">Espanhol</option><option value="0718153d308011e88d7f42010a9e0013">Filosofia</option><option value="07749f1c308011e88d7f42010a9e0013">Física</option><option value="07d576ad308011e88d7f42010a9e0013">Geografia</option><option value="08374b00308011e88d7f42010a9e0013">Gramática</option><option value="0898db98308011e88d7f42010a9e0013">História</option><option value="08f986f9308011e88d7f42010a9e0013">Inglês</option><option value="09580f35308011e88d7f42010a9e0013">Laboratório</option><option value="09b711f1308011e88d7f42010a9e0013">Literatura</option><option value="0a172f58308011e88d7f42010a9e0013">Matemática</option><option value="0a788e49308011e88d7f42010a9e0013">Matemática II</option><option value="0add97c5308011e88d7f42010a9e0013">Música</option><option value="0b3d7433308011e88d7f42010a9e0013">Português</option><option value="0b9bf86e308011e88d7f42010a9e0013">Química</option><option selected="selected" value="0bf8d908308011e88d7f42010a9e0013">Redação</option><option value="0c53a810308011e88d7f42010a9e0013">Sociologia</option>								</select></div>
								<label for="disciplina">Disciplina</label>
							</div>
							<div class="input-field left" style="width:300px; margin-left: 20px;">
								<div class="select-wrapper"><span class="caret">▼</span><input type="text" class="select-dropdown" readonly="true" data-activates="select-options-fbe57916-d374-3160-a44f-1855ffb32b47" value="Por semana"><ul id="select-options-fbe57916-d374-3160-a44f-1855ffb32b47" class="dropdown-content select-dropdown "><li class=""><span>Por dia</span></li><li class=""><span>Por semana</span></li><li class=""><span>Por mês</span></li></ul><select name="t" id="tempo" data-select-id="fbe57916-d374-3160-a44f-1855ffb32b47" class="initialized">
																			<option value="D">Por dia</option>
									<option selected="selected" value="S">Por semana</option>
									<option value="M">Por mês</option>
								</select></div>
								<label for="tempo">Tempo</label>
							</div>
						</form>


							<a class="btn-floating btn-move-up2 waves-effect waves-light transparent z-depth-0 right">
								<i class="material-icons activator grey-text">filter_list</i>
							</a>
							<div class="move-up transparent">
								<div class="trending-line-chart-wrapper"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
									<canvas id="trending-line-chart" class="line-chartd chartjs-render-monitor" height="292" width="688" style="width: 688px; height: 292px; display: block;"></canvas>
								</div>
							</div>
						</div>
						<div class="card-reveal" style="display: none; transform: translateY(0px);">
							<div class="card-title grey-text text-darken-4" style="height: 68px;">
								<div class="left">Detalhado</div>
								<i class="material-icons right">close</i>

							</div>
							<table class="responsive-table striped">
								<thead>
									<tr>
										<th>Periodo</th>
										<th>Menor nota</th>
										<th>Média</th>
										<th>Maior nota</th>
									</tr>
								</thead>
								<tbody>
									
									<tr>
													<td>2017-52</td>
													<td>4.00</td>
													<td>32.68</td>
													<td>67.00</td>
												</tr><tr>
													<td>2018-01</td>
													<td>1.00</td>
													<td>32.09</td>
													<td>74.00</td>
												</tr><tr>
													<td>2018-02</td>
													<td>1.00</td>
													<td>33.20</td>
													<td>75.00</td>
												</tr><tr>
													<td>2018-03</td>
													<td>1.00</td>
													<td>32.23</td>
													<td>75.00</td>
												</tr><tr>
													<td>2018-04</td>
													<td>1.00</td>
													<td>33.07</td>
													<td>75.00</td>
												</tr><tr>
													<td>2018-05</td>
													<td>1.00</td>
													<td>31.53</td>
													<td>73.00</td>
												</tr><tr>
													<td>2018-06</td>
													<td>1.00</td>
													<td>32.07</td>
													<td>74.00</td>
												</tr><tr>
													<td>2018-07</td>
													<td>1.00</td>
													<td>32.68</td>
													<td>75.00</td>
												</tr><tr>
													<td>2018-08</td>
													<td>1.00</td>
													<td>33.89</td>
													<td>75.00</td>
												</tr><tr>
													<td>2018-09</td>
													<td>1.00</td>
													<td>33.36</td>
													<td>75.00</td>
												</tr><tr>
													<td>2018-10</td>
													<td>1.00</td>
													<td>32.09</td>
													<td>75.00</td>
												</tr><tr>
													<td>2018-11</td>
													<td>1.00</td>
													<td>32.89</td>
													<td>75.00</td>
												</tr><tr>
													<td>2018-53</td>
													<td>1.00</td>
													<td>32.54</td>
													<td>75.00</td>
												</tr>								</tbody>
							</table>
						</div>
					</div>
					<script type="text/javascript">
								var contextod = document.getElementsByClassName('line-chartd');
								var chartGraph = new Chart(contextod, {
									type: 'line',
									data: {
										labels: ['2017-52','2018-01','2018-02','2018-03','2018-04','2018-05','2018-06','2018-07','2018-08','2018-09','2018-10','2018-11','2018-53'],
										datasets: [
		
										{
											label: 'Média',
											data: [32.68,32.09,33.20,32.23,33.07,31.53,32.07,32.68,33.89,33.36,32.09,32.89,32.54],
											borderWidth: 2,
											borderColor: 'green',
											backgroundColor: 'transparent',
										},
										{
											label: 'Menor nota',
											data: [4.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00],
											borderWidth: 2,
											borderColor: 'red',
											backgroundColor: 'transparent',
										},
										{
											label: 'Maior nota',
											data: [67.00,74.00,75.00,75.00,75.00,73.00,74.00,75.00,75.00,75.00,75.00,75.00,75.00],
											borderWidth: 2,
											borderColor: 'blue',
											backgroundColor: 'transparent',
										},
		
										]
									},
									options: {
										title: {
											display: true,
											fontSize: 15,
											fontColor: '#222',
											text: 'Média de desempenho em '
										},
										legend: {
											display: true,
											position: 'bottom'
										},
										labels: {
											fontStyle: 'bold'
										},
										maintainAspectRatio: true,
									}
								});
							</script>				</div>
			</div>




					<!--
						//
						//	GRÁFICO RADAR (COMPARATIVO DA MÉDIA DE CADA ESCOLA POR DICIPLINA)
						//
					-->

					<div class="card" style="overflow: hidden;">	
						<div class="card-content">
							<a class="btn-floating btn-move-up2 waves-effect waves-light transparent z-depth-0 right">
								<i class="material-icons activator grey-text">filter_list</i>
							</a>
							<div class="col s12"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
								<canvas class="line-charte chartjs-render-monitor" height="472" width="709" style="width: 709px; height: 472px; display: block;"></canvas>
							</div>
						</div>
						<div class="card-reveal" style="display: none; transform: translateY(0px);">
							<div class="card-title grey-text text-darken-4" style="height: 68px;">
								<div class="left">Detalhado</div>
								<i class="material-icons right">close</i>

							</div>
							<table class="responsive-table striped">
								<thead>
									<tr>
										<th>Disciplina</th>
										<th>Menor nota</th>
										<th>Media</th>
										<th>Maior nota</th>
									</tr>
								</thead>
								<tbody>
									<tr>
													<td>Música</td>
													<td>1.00</td>
													<td>32.64</td>
													<td>75.00</td>
												</tr><tr>
													<td>Espanhol</td>
													<td>1.00</td>
													<td>32.64</td>
													<td>75.00</td>
												</tr><tr>
													<td>Redação</td>
													<td>1.00</td>
													<td>32.64</td>
													<td>75.00</td>
												</tr><tr>
													<td>Educação Física</td>
													<td>1.00</td>
													<td>32.64</td>
													<td>75.00</td>
												</tr><tr>
													<td>Ciências</td>
													<td>1.00</td>
													<td>32.64</td>
													<td>75.00</td>
												</tr><tr>
													<td>Filosofia</td>
													<td>1.00</td>
													<td>32.64</td>
													<td>75.00</td>
												</tr><tr>
													<td>Português</td>
													<td>1.00</td>
													<td>32.64</td>
													<td>75.00</td>
												</tr><tr>
													<td>Geografia</td>
													<td>1.00</td>
													<td>32.64</td>
													<td>75.00</td>
												</tr><tr>
													<td>Matemática</td>
													<td>1.00</td>
													<td>32.64</td>
													<td>75.00</td>
												</tr><tr>
													<td>Inglês</td>
													<td>1.00</td>
													<td>32.64</td>
													<td>75.00</td>
												</tr><tr>
													<td>História</td>
													<td>1.00</td>
													<td>32.64</td>
													<td>75.00</td>
												</tr>								</tbody>
							</table>
						</div>
					</div>
					<script type="text/javascript">
						var contextoe = document.getElementsByClassName('line-charte');
						var chartGraph = new Chart(contextoe, {
							type: 'radar',
							data: {
								labels: ['Música','Espanhol','Redação','Educação Física','Ciências','Filosofia','Português','Geografia','Matemática','Inglês','História'],
								datasets: [

								{
									label: 'Media',
									data: [32.64,32.64,32.64,32.64,32.64,32.64,32.64,32.64,32.64,32.64,32.64],
									borderWidth: 2,
									backgroundColor: 'transparent',
									borderColor: 'green',
								},

								{
									label: 'Menor nota',
									data: [1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00],
									borderWidth: 2,
									backgroundColor: 'transparent',
									borderColor: 'red',
								},

								{
									label: 'Maior nota',
									data: [75.00,75.00,75.00,75.00,75.00,75.00,75.00,75.00,75.00,75.00,75.00],
									borderWidth: 2,
									backgroundColor: 'transparent',
									borderColor: 'blue',
								},


								]
							},
							options: {
								scale: {
									pointLabels: {
										fontSize: 15
									}
								},
								legend: {
									position: 'bottom',
								},
								title: {
									display: true,
									fontSize: 15,
									fontColor: '#222',
									text: 'Matérias'
								},
								labels: {
									fontStyle: 'bold'
								},
								maintainAspectRatio: true,
							}
						});
					</script>

				</section>




</section>