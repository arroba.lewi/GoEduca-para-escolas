<?php

    function imprimirGraficoLine($contexto, $eixoX, $maior, $menor, $media){
        echo "<script type='text/javascript'>
						var $contexto = document.getElementsByClassName('$contexto');
						var chartGraph = new Chart($contexto, {
							type: 'line',
							data: {
								labels: [".$eixoX."],
								datasets: [

								{
									label: 'Media',
									data: [".$media."],
									borderWidth: 2,
									backgroundColor: 'transparent',
									borderColor: 'green',
								},

								{
									label: 'Menor nota',
									data: [".$menor."],
									borderWidth: 2,
									backgroundColor: 'transparent',
									borderColor: 'red',
								},

								{
									label: 'Maior nota',
									data: [".$maior."],
									borderWidth: 2,
									backgroundColor: 'transparent',
									borderColor: 'blue',
								},


								]
							},
							options: {
								scale: {
									pointLabels: {
										fontSize: 15
									}
								},
								legend: {
									position: 'bottom',
								},
								title: {
									display: true,
									fontSize: 15,
									fontColor: '#222',
									text: 'Matérias'
								},
								labels: {
									fontStyle: 'bold'
								},
								maintainAspectRatio: true,
							}
						});
					</script>";
    }