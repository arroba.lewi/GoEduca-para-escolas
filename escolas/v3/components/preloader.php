 <div class="preloader-background" style="
 background: rgba(0,161,176);
  background: -moz-linear-gradient(to bottom right, #48a1af, #232423);
  background: -webkit-linear-gradient(bottom right, #48a1af, #232423);
  background: linear-gradient(to bottom right, #48a1af, #232423);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#48a1af', endColorstr='#232423',GradientType=0 );
 ">
    <div class="preloader-wrapper big active">
      <div class="spinner-layer spinner-blue-only" style="border-color: white;">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
        <div class="circle"></div>
      </div><div class="circle-clipper right">
      <div class="circle"></div>
    </div>
  </div>
</div>
</div>