<?php

	// Incluindo arquivo de configuração
	require_once (substr($_SERVER["DOCUMENT_ROOT"], -1) === "/" ? substr($_SERVER["DOCUMENT_ROOT"], 0, strlen($_SERVER["DOCUMENT_ROOT"]) - 1) : $_SERVER["DOCUMENT_ROOT"])."/escolas/config.php";
	
	// Conferir se está logado
	if(!Logado()){
		header("location: /$APP_PATH_VERSION");
	}else{
		$meta_title = "GoEduca ADM";

		$localCss 	= "dashboard";
		$hideNav = true;
		$hideFooter = true;

		include $APP_PATH_ROOT."/components/header.php";
		include $APP_PATH_ROOT."/components/nav.php";
		include $APP_PATH_ROOT."/view/body-adm.php";
		include $APP_PATH_ROOT."/components/config/end.php";
	}
?>